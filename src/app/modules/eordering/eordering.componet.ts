import { PermissionProvider } from './../../../providers/eordering/permission';
import { EoMenuPage } from './../../../pages/eordering/eo-menu/eo-menu';
import { Component, ViewChild } from '@angular/core';
import { ConfigProvider } from '../../../providers/eordering/config';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { EoTabsPage } from '../../../pages/eordering/eo-tabs/eo-tabs';
import { NavParams, Nav } from 'ionic-angular';

@Component({
	templateUrl: 'eordering.html'
})
export class Eordering {

	rootPage: any;
	userInfo: any;
	customerInfo: any;
	permissions: any;

	@ViewChild(Nav) nav : Nav;

	constructor(private configProvider: ConfigProvider,
							private storageProvider: StorageProvider,
							private permissionProvider: PermissionProvider,
							private navParams: NavParams) {

		this.userInfo = this.storageProvider.getUserInfo();
		this.customerInfo = this.storageProvider.getCustomerInfo();
		/*
		if (this.storageProvider.getEoMenuPermission() == undefined) {
			this.permissionProvider.getMenuPermission(this.userInfo.roleId)
				.then(res => {
					if (res.result == 'SUCCESS') {
						localStorage.setItem('eo_permission', JSON.stringify(res.data.permissionList));
					}
				});
		}
		*/

		if (this.userInfo.userTypeDesc == 'Multi') {
			this.rootPage = EoStorePage;
		} else {
			this.rootPage = EoTabsPage;
		}
	}

  ionViewDidLoad() {

	}
}
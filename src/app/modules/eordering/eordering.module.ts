import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Eordering } from './eordering.componet';
import { NgPipesModule } from 'ngx-pipes';
import { ConfigProvider } from '../../../providers/eordering/config';
import { ProductProvider } from '../../../providers/eordering/product';
import { FavoriteProvider } from '../../../providers/eordering/favorite';
import { CartProvider } from '../../../providers/eordering/cart';
import { OrderProvider } from '../../../providers/eordering/order';
import { CustomerProvider } from '../../../providers/eordering/customer';
import { MarketingProvider } from '../../../providers/eordering/marketing';
import { BrandFilterPipe } from '../../../pipes/eordering/brand-filter';
import { TypeFilterPipe } from '../../../pipes/eordering/type-filter';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { EoMenuPage } from '../../../pages/eordering/eo-menu/eo-menu';
import { EoTabsPage } from '../../../pages/eordering/eo-tabs/eo-tabs';
import { EoMainPage } from '../../../pages/eordering/eo-main/eo-main';
import { EoProductFilterPage } from '../../../pages/eordering/eo-product-filter/eo-product-filter';
import { EoProductSearchPage } from '../../../pages/eordering/eo-product-search/eo-product-search';
import { EoProductListPage } from '../../../pages/eordering/eo-product-list/eo-product-list';
import { EoProductDetailPage } from '../../../pages/eordering/eo-product-detail/eo-product-detail';
import { EoColorPage } from '../../../pages/eordering/eo-color/eo-color';
import { EoCartPage } from '../../../pages/eordering/eo-cart/eo-cart';
import { EoCheckoutPage } from '../../../pages/eordering/eo-checkout/eo-checkout';
import { ComponentsModule } from '../../../components/components.module';
import { EoOrderListPage } from '../../../pages/eordering/eo-order-list/eo-order-list';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { OrderFilterPipe } from '../../../pipes/eordering/order-filter';
import { EoSummaryPage } from '../../../pages/eordering/eo-summary/eo-summary';
import { EorderetcProvider } from '../../../providers/eordering/eorderetc';
import { EoProfilePage } from '../../../pages/eordering/eo-profile/eo-profile';
import { EoProfileUpdatePage } from '../../../pages/eordering/eo-profile-update/eo-profile-update';
import { EoContactPage } from '../../../pages/eordering/eo-contact/eo-contact';
import { EoProblemPage } from '../../../pages/eordering/eo-problem/eo-problem';
import { EoNewsListPage } from '../../../pages/eordering/eo-news-list/eo-news-list';
import { EoNewsDetailPage } from '../../../pages/eordering/eo-news-detail/eo-news-detail';
import { NewsProvider } from '../../../providers/eordering/news';
import { EoReportMonthlyPage } from '../../../pages/eordering/eo-report-monthly/eo-report-monthly';
import { EoReportQuarterPage } from '../../../pages/eordering/eo-report-quarter/eo-report-quarter';
import { DocumentProvider } from '../../../providers/eordering/document';
import { ReportProvider } from '../../../providers/eordering/report';
import { AreaFilterPipe } from '../../../pipes/eordering/area-filter';
import { SalesFilterPipe } from '../../../pipes/eordering/sales-filter';
import { EoOrderPdfPage } from '../../../pages/eordering/eo-order-pdf/eo-order-pdf';
import { EoSizePage } from '../../../pages/eordering/eo-size/eo-size';
import { EoProductPromotionInfoPage } from '../../../pages/eordering/eo-product-promotion-info/eo-product-promotion-info';
import { SafePipeModule } from 'safe-pipe';
import { EoPromotionPage } from '../../../pages/eordering/eo-promotion/eo-promotion';
import { PromotionProvider } from '../../../providers/eordering/promotion';
import { DirectivesModule } from '../../../directives/directives.module';
import { EoPromotionBtfPage } from '../../../pages/eordering/eo-promotion-btf/eo-promotion-btf';
import { EoPromotionSizePage } from '../../../pages/eordering/eo-promotion-size/eo-promotion-size';
import { EoPromotionColorPage } from '../../../pages/eordering/eo-promotion-color/eo-promotion-color';
import { EoProductDetailColorPage } from '../../../pages/eordering/eo-product-detail-color/eo-product-detail-color';
import { EoProductDetailSizePage } from '../../../pages/eordering/eo-product-detail-size/eo-product-detail-size';
import { EoPromotionFreegoodPage } from '../../../pages/eordering/eo-promotion-freegood/eo-promotion-freegood';
import { EoPromotionFreegoodColorPage } from '../../../pages/eordering/eo-promotion-freegood-color/eo-promotion-freegood-color';
import { EoPromotionFreegoodSizePage } from '../../../pages/eordering/eo-promotion-freegood-size/eo-promotion-freegood-size';

@NgModule({
	declarations: [
		Eordering,
    EoStorePage,
		EoMenuPage,
		EoTabsPage,
		EoMainPage,
		EoProductFilterPage,
		EoProductSearchPage,
		EoProductListPage,
		EoProductDetailPage,
		EoColorPage,
		EoCartPage,
		EoCheckoutPage,
		EoOrderListPage,
		EoSummaryPage,
		EoProfilePage,
		EoProfileUpdatePage,
		EoContactPage,
		EoProblemPage,
		EoNewsListPage,
		EoNewsDetailPage,
		EoReportMonthlyPage,
		EoReportQuarterPage,
		EoOrderPdfPage,
		EoSizePage,
		EoProductPromotionInfoPage,
		EoPromotionPage,
		EoPromotionBtfPage,
		EoPromotionSizePage,
		EoPromotionColorPage,
		EoProductDetailSizePage,
		EoProductDetailColorPage,
		EoPromotionFreegoodPage,
		EoPromotionFreegoodSizePage,
		EoPromotionFreegoodColorPage,
		BrandFilterPipe,
		TypeFilterPipe,
		OrderFilterPipe,
		AreaFilterPipe,
		SalesFilterPipe
	],
	imports: [
		IonicModule.forRoot(Eordering),
    PdfViewerModule,
		ComponentsModule,
		DirectivesModule,
		NgPipesModule,
		SafePipeModule
	],
	entryComponents: [
		Eordering,
    EoStorePage,
		EoMenuPage,
		EoTabsPage,
		EoMainPage,
		EoProductFilterPage,
		EoProductSearchPage,
		EoProductListPage,
		EoProductDetailPage,
		EoColorPage,
		EoCartPage,
    EoCheckoutPage,
		EoOrderListPage,
		EoSummaryPage,
		EoProfilePage,
		EoProfileUpdatePage,
		EoContactPage,
		EoProblemPage,
		EoNewsListPage,
		EoNewsDetailPage,
		EoReportMonthlyPage,
		EoReportQuarterPage,
		EoOrderPdfPage,
		EoSizePage,
		EoProductPromotionInfoPage,
		EoPromotionPage,
		EoPromotionBtfPage,
		EoPromotionSizePage,
		EoPromotionColorPage,
		EoProductDetailSizePage,
		EoProductDetailColorPage,
		EoPromotionFreegoodPage,
		EoPromotionFreegoodSizePage,
		EoPromotionFreegoodColorPage
	],
	providers: [
		ConfigProvider,
		ProductProvider,
		FavoriteProvider,
		CartProvider,
		OrderProvider,
		CustomerProvider,
		MarketingProvider,
		EorderetcProvider,
    NewsProvider,
		DocumentProvider,
		ReportProvider,
		PromotionProvider
	]
})
export class EorderingModule {

}
import { EpBankBranchPage } from './../../../pages/epocket/ep-bank-branch/ep-bank-branch';
import { EpBankPage } from './../../../pages/epocket/ep-bank/ep-bank';
import { EpSaleredeemHistoryInfoPage } from './../../../pages/epocket/ep-saleredeem-history-info/ep-saleredeem-history-info';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { NgPipesModule } from 'ngx-pipes';
import { EPocket } from './epocket.componet';
import { EpHomePage } from '../../../pages/epocket/ep-home/ep-home';
import { EpProductBrandListPage } from '../../../pages/epocket/ep-product-brand-list/ep-product-brand-list';
import { EpProductBrandSizeListPage } from '../../../pages/epocket/ep-product-brand-size-list/ep-product-brand-size-list';
import { EpProductInfoPage } from '../../../pages/epocket/ep-product-info/ep-product-info';
import { EpCartPage } from '../../../pages/epocket/ep-cart/ep-cart';
import { EpConfirmPhoneNoPage } from '../../../pages/epocket/ep-confirm-phone-no/ep-confirm-phone-no';
import { EPProductProvider } from '../../../providers/epocket/product';
import { EpDealerHistoryPage } from '../../../pages/epocket/ep-dealer-history/ep-dealer-history';
import { EpDealerHistoryInfoPage } from '../../../pages/epocket/ep-dealer-history-info/ep-dealer-history-info';
import { EpDealerRedeemConfirmPage } from '../../../pages/epocket/ep-dealer-redeem-confirm/ep-dealer-redeem-confirm';
import { EpDealerNewsPage } from '../../../pages/epocket/ep-dealer-news/ep-dealer-news';
import { EpDealerNewsInfoPage } from '../../../pages/epocket/ep-dealer-news-info/ep-dealer-news-info';
import { EpDealerPaymentHistoryPage } from '../../../pages/epocket/ep-dealer-payment-history/ep-dealer-payment-history';
import { EpDealerRequestHistoryPage } from '../../../pages/epocket/ep-dealer-request-history/ep-dealer-request-history';
import { EPDealerProvider } from '../../../providers/epocket/dealer';
import { CalendarModule } from "ion2-calendar";
import { EpHomeCoinstockPage } from '../../../pages/epocket/ep-home-coinstock/ep-home-coinstock';
import { EpCoinstockListPage } from '../../../pages/epocket/ep-coinstock-list/ep-coinstock-list';
import { EPSaleProvider } from '../../../providers/epocket/sale';
import { EpCoinstockInfoPage } from './../../../pages/epocket/ep-coinstock-info/ep-coinstock-info';
import { EpCoinstockProductbrandListPage } from './../../../pages/epocket/ep-coinstock-productbrand-list/ep-coinstock-productbrand-list';
import { EpCoinstockProductInfoPage } from './../../../pages/epocket/ep-coinstock-product-info/ep-coinstock-product-info';
import { EpSaleredeemHistoryPage } from '../../../pages/epocket/ep-saleredeem-history/ep-saleredeem-history';
import { EpCoinstockReturncoinPage } from './../../../pages/epocket/ep-coinstock-returncoin/ep-coinstock-returncoin';
import { EpChangeprofilePage } from './../../../pages/epocket/ep-changeprofile/ep-changeprofile';
import { EpChangeprofileStep2Page } from './../../../pages/epocket/ep-changeprofile-step2/ep-changeprofile-step2';
import { EpSaleConfirmArPage } from './../../../pages/epocket/ep-sale-confirm-ar/ep-sale-confirm-ar';
import { EpChangeprofileHistoryMainPage } from './../../../pages/epocket/ep-changeprofile-history-main/ep-changeprofile-history-main';
import { EpChangeprofileHistoryProcessPage } from './../../../pages/epocket/ep-changeprofile-history-process/ep-changeprofile-history-process';
import { EpChangeprofileHistoryRequestPage } from './../../../pages/epocket/ep-changeprofile-history-request/ep-changeprofile-history-request';
import { EpDealerProfilePage } from './../../../pages/epocket/ep-dealer-profile/ep-dealer-profile';
import { EpDealerProfileStep2Page } from '../../../pages/epocket/ep-dealer-profile-step2/ep-dealer-profile-step2';
import { EpDealerPaymentHistoryInfoPage } from '../../../pages/epocket/ep-dealer-payment-history-info/ep-dealer-payment-history-info';
import { EpHomeDealerReportPage } from './../../../pages/epocket/ep-home-dealer-report/ep-home-dealer-report';
import { EpRpt01DealerexchangecoverInfoPage } from '../../../pages/epocket/ep-report/ep-rpt01-dealerexchangecover-info/ep-rpt01-dealerexchangecover-info';
import { EpRpt01DealerexchangecoverPage } from '../../../pages/epocket/ep-report/ep-rpt01-dealerexchangecover/ep-rpt01-dealerexchangecover';
import { EpRpt02DealerredeempainterPage } from '../../../pages/epocket/ep-report/ep-rpt02-dealerredeempainter/ep-rpt02-dealerredeempainter';
import { EpRpt02DealerredeempainterInfoPage } from '../../../pages/epocket/ep-report/ep-rpt02-dealerredeempainter-info/ep-rpt02-dealerredeempainter-info';
import { EpRpt03DealerredeembrandtypePage } from '../../../pages/epocket/ep-report/ep-rpt03-dealerredeembrandtype/ep-rpt03-dealerredeembrandtype';
import { EpRpt04DealerredeemhistoryPage } from '../../../pages/epocket/ep-report/ep-rpt04-dealerredeemhistory/ep-rpt04-dealerredeemhistory';
import { EpRpt04DealerredeemhistoryInfoPage } from '../../../pages/epocket/ep-report/ep-rpt04-dealerredeemhistory-info/ep-rpt04-dealerredeemhistory-info';
import { EpRpt05CoinstockPage } from '../../../pages/epocket/ep-report/ep-rpt05-coinstock/ep-rpt05-coinstock';
import { EpRpt05ConstockInfoPage } from '../../../pages/epocket/ep-report/ep-rpt05-constock-info/ep-rpt05-constock-info';
import { EpRpt05CoinstockdealerPage } from '../../../pages/epocket/ep-report/ep-rpt05-coinstockdealer/ep-rpt05-coinstockdealer';
import { EpHomeSaleReportPage } from './../../../pages/epocket/ep-home-sale-report/ep-home-sale-report';
import { EpRpt06SalereceiveInfoPage } from '../../../pages/epocket/ep-report/ep-rpt06-salereceive-info/ep-rpt06-salereceive-info';
import { EpRpt06SalereceivePage } from '../../../pages/epocket/ep-report/ep-rpt06-salereceive/ep-rpt06-salereceive';
import { EpRpt07SaleredeemPage } from './../../../pages/epocket/ep-report/ep-rpt07-saleredeem/ep-rpt07-saleredeem';
import { EpRpt07SaleredeemInfoPage } from '../../../pages/epocket/ep-report/ep-rpt07-saleredeem-info/ep-rpt07-saleredeem-info';
import { EpRpt08SalecoinstockInfoPage } from './../../../pages/epocket/ep-report/ep-rpt08-salecoinstock-info/ep-rpt08-salecoinstock-info';
import { EpRpt08SalecoinstockPage } from './../../../pages/epocket/ep-report/ep-rpt08-salecoinstock/ep-rpt08-salecoinstock';
import { SafePipeModule } from 'safe-pipe';
import { EpRpt09DealerwaitconfirmPage } from '../../../pages/epocket/ep-report/ep-rpt09-dealerwaitconfirm/ep-rpt09-dealerwaitconfirm';
import { EpPainterPage } from '../../../pages/epocket/ep-painter/ep-painter';
import { DirectivesModule } from '../../../directives/directives.module';
import { EpChangeprofileStep3Page } from '../../../pages/epocket/ep-changeprofile-step3/ep-changeprofile-step3';
import { EpChangeprofileStep4Page } from '../../../pages/epocket/ep-changeprofile-step4/ep-changeprofile-step4';

@NgModule({
	declarations: [
		EPocket,
		EpHomePage,
		EpProductBrandListPage,
		EpProductBrandSizeListPage,
		EpProductInfoPage,
		EpCartPage,
		EpConfirmPhoneNoPage,
		EpDealerHistoryPage,
		EpDealerHistoryInfoPage,
		EpDealerRedeemConfirmPage,
		EpDealerNewsPage,
		EpDealerNewsInfoPage,
		EpDealerPaymentHistoryPage,
		EpDealerRequestHistoryPage,
		EpHomeCoinstockPage,
		EpCoinstockListPage,
		EpCoinstockInfoPage,
		EpCoinstockProductbrandListPage,
		EpCoinstockProductInfoPage,
		EpSaleredeemHistoryPage,
		EpSaleredeemHistoryInfoPage,
		EpCoinstockReturncoinPage,
		EpChangeprofilePage,
		EpChangeprofileStep2Page,
		EpSaleConfirmArPage,
		EpBankPage,
		EpChangeprofileHistoryMainPage,
		EpChangeprofileHistoryRequestPage,
		EpChangeprofileHistoryProcessPage,
		EpHomeDealerReportPage,
		EpDealerProfilePage,
		EpDealerProfileStep2Page,
		EpDealerPaymentHistoryInfoPage,
		EpRpt01DealerexchangecoverPage,
		EpRpt01DealerexchangecoverInfoPage,	
		EpRpt02DealerredeempainterInfoPage,
		EpRpt02DealerredeempainterPage,
		EpRpt03DealerredeembrandtypePage,
		EpRpt04DealerredeemhistoryPage,
		EpRpt04DealerredeemhistoryInfoPage,
		EpRpt05CoinstockPage,
		EpRpt05ConstockInfoPage,
		EpRpt05CoinstockdealerPage,
		EpHomeSaleReportPage,
		EpRpt06SalereceiveInfoPage,
		EpRpt06SalereceivePage,
		EpRpt07SaleredeemPage,
		EpRpt07SaleredeemInfoPage,
		EpRpt08SalecoinstockInfoPage,
		EpRpt08SalecoinstockPage,
		EpRpt09DealerwaitconfirmPage,
		EpPainterPage,
		EpChangeprofileStep3Page,
		EpChangeprofileStep4Page,
		EpBankBranchPage
	],
	imports: [
		IonicModule.forRoot(EPocket),
		NgPipesModule,
		CalendarModule,
		SafePipeModule,
		DirectivesModule
	],
	entryComponents: [
		EPocket,
		EpHomePage,
		EpProductBrandListPage,
		EpProductBrandSizeListPage,
		EpProductInfoPage,
		EpCartPage,
		EpConfirmPhoneNoPage,
		EpDealerHistoryPage,
		EpDealerHistoryInfoPage,
		EpDealerRedeemConfirmPage,
		EpDealerNewsPage,
		EpDealerNewsInfoPage,
		EpDealerPaymentHistoryPage,
		EpDealerRequestHistoryPage,
		EpHomeCoinstockPage,
		EpCoinstockListPage,
		EpCoinstockInfoPage,
		EpCoinstockProductbrandListPage,
		EpCoinstockProductInfoPage,
		EpSaleredeemHistoryPage,
		EpSaleredeemHistoryInfoPage,
		EpCoinstockReturncoinPage,
		EpChangeprofilePage,
		EpChangeprofileStep2Page,
		EpSaleConfirmArPage,
		EpBankPage,
		EpChangeprofileHistoryMainPage,
		EpChangeprofileHistoryRequestPage,
		EpChangeprofileHistoryProcessPage,
		EpHomeDealerReportPage,
		EpDealerProfilePage,
		EpDealerProfileStep2Page,
		EpDealerPaymentHistoryInfoPage,
		EpRpt01DealerexchangecoverPage,
		EpRpt01DealerexchangecoverInfoPage,
		EpRpt02DealerredeempainterInfoPage,
		EpRpt02DealerredeempainterPage,
		EpRpt03DealerredeembrandtypePage,
		EpRpt04DealerredeemhistoryPage,
		EpRpt04DealerredeemhistoryInfoPage,		
		EpRpt05CoinstockPage,
		EpRpt05ConstockInfoPage,
		EpRpt05CoinstockdealerPage,
		EpHomeSaleReportPage,
		EpRpt06SalereceiveInfoPage,
		EpRpt06SalereceivePage,
		EpRpt07SaleredeemPage,
		EpRpt07SaleredeemInfoPage,
		EpRpt08SalecoinstockInfoPage,
		EpRpt08SalecoinstockPage,
		EpRpt09DealerwaitconfirmPage,
		EpPainterPage,
		EpChangeprofileStep3Page,
		EpChangeprofileStep4Page,
		EpBankBranchPage
	],
	providers: [
		EPProductProvider,
		EPDealerProvider,
		EPSaleProvider
	]
})
export class EpocketModule {

}
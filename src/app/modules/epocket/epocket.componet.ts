import { DcHomePage } from './../../../pages/dealerconnect/dc-home/dc-home';
import { AlertController ,NavController} from 'ionic-angular';
import { ConfigProvider } from './../../../providers/eordering/config';
import { Component } from '@angular/core';
import { EpHomePage } from '../../../pages/epocket/ep-home/ep-home';
import { EpHomeCoinstockPage } from '../../../pages/epocket/ep-home-coinstock/ep-home-coinstock';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPDealerProvider } from '../../../providers/epocket/dealer';
import { EpChangeprofilePage } from '../../../pages/epocket/ep-changeprofile/ep-changeprofile';

@Component({
	templateUrl: 'epocket.html'
})
export class EPocket {

	rootPage: any;
	userInfo: any;
	customerInfo: any;

	constructor(public configProvider: ConfigProvider,
							public storageProvider: StorageProvider,
							private alertCtrl: AlertController,
							private epDealerProvider : EPDealerProvider,
							private navCtrl : NavController) {

		this.userInfo = this.storageProvider.getUserInfo();
		this.customerInfo = this.storageProvider.getCustomerInfo();

		let CusIsEmpty = this.customerInfo == undefined || this.customerInfo == null;
		
		if (this.userInfo.deptStatus === 'D') {
				if (CusIsEmpty){
					this.alertCtrl.create({
						title: "Dealer Connect Alert",
						message: "คุณไม่สามารถเข้าระบบ ePocket ได้ กรุณาติดต่อ Admin ระบบเพื่อดำเนินการต่อไป ขอบคุณครับ.",
						enableBackdropDismiss: false,
						buttons: ['ตกลง']
					}).present();
					this.rootPage = DcHomePage ;
				}else{
					//this.IsNewDealer();
					this.rootPage = EpHomePage; 
				}
			} else {
				this.rootPage = EpHomeCoinstockPage;
			}

	}

	IsNewDealer(){
		let CustomerCode =  this.customerInfo.customerCode;
		
		let ParaObject = {
			DEALER_ID : CustomerCode
		}

		this.epDealerProvider.getVendorByDealerId(ParaObject)
		.then(res => {
			if (res.result == 'SUCCESS') {
				this.rootPage = EpHomePage; 
			}
			else{
				// this.rootPage = EpChangeprofilePage ; 	//ถ้าเป็น Dealer ใหม่
				 this.navCtrl.push(EpChangeprofilePage);
			}
		});
		
	}
}
import { Component } from '@angular/core';
import { AuthProvider } from '../../../providers/shared/auth';
import { ConfigProvider } from '../../../providers/eordering/config';
import { DcPinRequestPage } from '../../../pages/dealerconnect/dc-pin-request/dc-pin-request';
import { AlertController, App, NavParams, Platform } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { DcHomePage } from '../../../pages/dealerconnect/dc-home/dc-home';
import { PermissionProvider } from '../../../providers/eordering/permission';
import { DcSetPinPage } from '../../../pages/dealerconnect/dc-set-pin/dc-set-pin';
import { DcLoginPage } from '../../../pages/dealerconnect/dc-login/dc-login';

@Component({
	templateUrl: 'dealerconnect.html'
})
export class DealerConnect {

	userInfo: any;
	rootPage: any;
	from: any;
	action: any;

	constructor(public authProvider: AuthProvider,
					    private configProvider: ConfigProvider,
							private navParams: NavParams,
							private storageProvider: StorageProvider,
							private permissionProvider: PermissionProvider,
              private alertCtrl: AlertController,
              private platform: Platform,
              private app: App) {
    
    // events
    this.from   = this.navParams.get('from');
    this.action = this.navParams.get('action');
    
    if (platform.is('android')) {
      let count = 0;
      
      this.platform.registerBackButtonAction(() => {
        let nav = app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          nav.pop();
        } else {
          count++;

          if (count <= 1) {
            let alert = this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'คุณต้องการออกจากระบบใช่หรือไม่ ?',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'ยกเลิก'
                },
                {
                  text: 'ตกลง',
                  handler: () => {
                    this.platform.exitApp();
                  }
                }
              ]
            });
            
            alert.present();
            alert.onDidDismiss(() => count = 0);
          }
        }
      });

    }
    
    // TODO: รอแก้ไข Code
    if (this.authProvider.isAuthenticated()) {
      this.userInfo = this.storageProvider.getUserInfo();
      
      // if (this.storageProvider.getEoMenuPermission() == undefined) {
      //   this.permissionProvider.getMenuPermission(this.userInfo.roleId)
      //     .then(res => {
      //       if (res.result == 'SUCCESS') {
      //         localStorage.setItem('eo_permission', JSON.stringify(res.data.permissionList));
      //       }
      //     });
      // }
      /*
      // set menu permission for eOrdering only!
      if (this.storageProvider.getPermission() == undefined) {
        this.permissionProvider.getMenuPermission(this.userInfo.roleId)
          .then(res => {
            if (res.result == 'SUCCESS') {
              localStorage.setItem('eo_permission', JSON.stringify(res.data.permissionList));
            }
          });
      }
      */
      
      if (this.userInfo.PIN == '') {
        this.rootPage = DcSetPinPage;
      } else {
        if (this.from == 'login') {
          switch (this.action) {
            case 'RP':
            case 'CP':
              this.rootPage = DcSetPinPage;
              break;
            default:
              this.rootPage = DcHomePage;
              break;
          }
        } else {
          if (this.action == 'NP') {
          	this.rootPage = DcHomePage;
					} else {
            this.rootPage = DcPinRequestPage;
					}
        }
      }
    } else {
      this.rootPage = DcLoginPage;
    }
	}
}
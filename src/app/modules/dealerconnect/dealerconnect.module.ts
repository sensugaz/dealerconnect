import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { DealerConnect } from './dealerconnect.component';
import { NgPipesModule } from 'ngx-pipes';

import { DcMenuPage } from '../../../pages/dealerconnect/dc-menu/dc-menu';
import { DcPinRequestPage } from '../../../pages/dealerconnect/dc-pin-request/dc-pin-request';
import { DcHomePage } from '../../../pages/dealerconnect/dc-home/dc-home';
import { UserProvider } from '../../../providers/dealerconnect/user';
import { DcNewsPage } from '../../../pages/dealerconnect/dc-news/dc-news';
import { DcContactPage } from '../../../pages/dealerconnect/dc-contact/dc-contact';
import { DcChangePasswordPage } from '../../../pages/dealerconnect/dc-change-password/dc-change-password';
import { NewsProvider } from '../../../providers/dealerconnect/news';
import { DcNewsDetailPage } from '../../../pages/dealerconnect/dc-news-detail/dc-news-detail';
import { DcSetPinPage } from '../../../pages/dealerconnect/dc-set-pin/dc-set-pin';
import { SafePipeModule } from 'safe-pipe';
import { DcForgotPasswordPage } from '../../../pages/dealerconnect/dc-forgot-password/dc-forgot-password';
import { DcLoginPage } from './../../../pages/dealerconnect/dc-login/dc-login';
import { EpRecipienttypePage } from '../../../pages/epocket/ep-recipienttype/ep-recipienttype';
import { DirectivesModule } from '../../../directives/directives.module';
import { EpVendorcityPage } from '../../../pages/epocket/ep-vendorcity/ep-vendorcity';
import { EpVendordistrictPage } from '../../../pages/epocket/ep-vendordistrict/ep-vendordistrict';


@NgModule({
	declarations: [
		DealerConnect,
		DcLoginPage,
		DcMenuPage,
    DcPinRequestPage,
		DcHomePage,
		DcSetPinPage,
		DcChangePasswordPage,
		DcNewsPage,
		DcContactPage,
		DcNewsDetailPage,
		DcForgotPasswordPage,

		EpRecipienttypePage,
		EpVendorcityPage,
		EpVendordistrictPage
	],
	imports: [
		IonicModule.forRoot(DealerConnect),
		DirectivesModule,
		NgPipesModule,
		SafePipeModule
	],
	entryComponents: [
		DealerConnect,
		DcLoginPage,
		DcMenuPage,
		DcPinRequestPage,
		DcHomePage,
		DcSetPinPage,
    DcChangePasswordPage,
		DcNewsPage,
		DcContactPage,
		DcNewsDetailPage,
		DcForgotPasswordPage,

		EpRecipienttypePage,
		EpVendorcityPage,
		EpVendordistrictPage
	],
	providers: [
		UserProvider,
		NewsProvider
	]
})
export class DealerConnectModule {

}
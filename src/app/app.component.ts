import { Component } from '@angular/core';
import { AlertController, App, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DealerConnect } from './modules/dealerconnect/dealerconnect.component';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Network } from '@ionic-native/network';
import { ConfigProvider } from "../providers/eordering/config";
import { Keyboard } from '@ionic-native/keyboard';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = DealerConnect;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private androidPermissions: AndroidPermissions,
              private network: Network,
              private alertCtrl: AlertController,
              private configProvider: ConfigProvider,
              private keyboard: Keyboard,
              private app: App) {

	  this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      
      if (this.platform.is('android')) {    
        this.statusBar.styleLightContent();
        
        this.androidPermissions.requestPermissions(
          [
            this.androidPermissions.PERMISSION.CAMERA, 
            this.androidPermissions.PERMISSION.CALL_PHONE, 
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          ]
        );
      }

      this.app.viewDidEnter.subscribe(e => {
        this.configProvider.loadConfig();

        if (this.platform.is('ios')) {
          let appEl = <HTMLElement>(document.getElementsByTagName('ION-APP')[0]);
          let appElHeight = appEl.clientHeight;

          this.keyboard.disableScroll(false);
          this.keyboard.hideKeyboardAccessoryBar(false);

          // window.addEventListener('native.keyboardshow', (e) => {
          //   appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
          //   //appEl.style.height = '0px';
          // });

          // window.addEventListener('native.keyboardhide', () => {
          //   appEl.style.height = '100%';
          // });
        }
      });

      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    });
  }
}
import { SplashScreen } from '@ionic-native/splash-screen';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DealerConnectModule } from './modules/dealerconnect/dealerconnect.module';
import { EorderingModule } from './modules/eordering/eordering.module';
import { EpocketModule } from './modules/epocket/epocket.module';
import { AuthProvider } from '../providers/shared/auth';
import { StorageProvider } from '../providers/shared/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { PermissionProvider } from '../providers/eordering/permission';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FileTransfer } from '@ionic-native/file-transfer';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import { Network } from '@ionic-native/network';

@NgModule({
	declarations: [
		MyApp
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp, {
			tabsHideOnSubPages: true,
			backButtonText: '',
			//pageTransition: 'ios',
			platforms: {
				// ios: {
				// 	statusbarPadding: true,
				// 	tabsHideOnSubPages: true
				// }
			},
			scrollAssist: false,
			autoFocusAssist: false
		}),
		HttpClientModule,
		DealerConnectModule,
		EorderingModule,
		EpocketModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp
	],
	providers: [
		StatusBar,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		AuthProvider,
		StorageProvider,
		BarcodeScanner,
		PermissionProvider,
		SplashScreen,
		AndroidPermissions,
		FileTransfer,
		DocumentViewer,
		InAppBrowser,
		FileOpener,
		Camera,
		Keyboard,
		DatePicker,
		File,
    Network
	]
})
export class AppModule {

}

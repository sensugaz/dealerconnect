 import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderFilter',
  pure: false
})
export class OrderFilterPipe implements PipeTransform {

  transform(items: any[], optional1, optional2) {
    let output;

    if (optional1 != undefined) {
      output = items.filter(value => {
        return value.docName.includes(optional1) || value.docNumber.includes(optional1.toUpperCase()) || value.salesOrderNumber.includes(optional1);
      });
    } else {
      output = items;
    }

    if (optional2 != undefined) {
      output = output.filter(value => {
        switch (optional2) {
          case '':
            return value;
          case 'CP':
            return value.percentComplete == 100;
          case 'CC':
            return (value.percentComplete == 0) && (value.rejectHStatus == 'C');
          case 'IP':
            return (value.percentComplete > 0) && (value.percentComplete != 100) && (value.rejectHStatus != 'C');
          case 'WT':
            return (value.percentComplete == 0) && (value.rejectHStatus != 'C');
          default:
            return value;
        }
      });
    }

    return output;
  }
}

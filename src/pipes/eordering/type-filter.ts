import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeFilter',
  pure: false
})
export class TypeFilterPipe implements PipeTransform {
  transform(items: any[], search: any[]): any {
    if (!Array.isArray(search)) {
      return items;
    }

    if (Array.isArray(items)) {
      if (search.length > 0) {
        return items.filter(item => {
          return search.indexOf(item.marketingCode) !== -1;
        });
      } else {
        return items;
      }
    }
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'salesFilter',
})
export class SalesFilterPipe implements PipeTransform {
  transform(items: any[], optional1: any) {
    let output;

    if (optional1 != undefined && optional1 != 'ALL') {
      output = items.filter(item => item.area_no.includes(optional1));
    } else {
      output = items;
    }

    return output;
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'brandFilter',
  pure: false
})
export class BrandFilterPipe implements PipeTransform {
  transform(items: any[], search: any[]): any {
    if (!Array.isArray(search)) {
      return items;
    }

    if (Array.isArray(items)) {
      if (search.length) {
        return items.filter(item => {
          return search.indexOf(item.marketingCode) !== -1;
        });
      } else {
        return items;
      }
    }
  }
}

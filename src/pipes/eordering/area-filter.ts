import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'areaFilter',
})
export class AreaFilterPipe implements PipeTransform {
  transform(items: any[], optional1: any, optional2: any) {
    let output;

    if (optional1 != undefined && optional1 != 'ALL') {
      output = items.filter(item => item.sales_group.includes(optional1));
    } else {
      output = items;
    }

    if (optional2 != undefined) {
      output = output.filter(item => item.commission_type.includes(optional2));
    }

    return output;
  }
}

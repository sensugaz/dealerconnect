import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/dealerconnect/dealerconnect.constant';

@Injectable()
export class NewsProvider {

  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
  }

  handleError(error: any): Promise<any> {
    return Promise.reject(
      error.message || error);
  }

  getNews() {
    return this.http.get(this.API_ENDPOINT + 'DCNews')
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }
}

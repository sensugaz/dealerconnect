import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/dealerconnect/dealerconnect.constant';

@Injectable()
export class UserProvider {

  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
  }

  handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  setNewpin(formData: any) {
    return this.http.post(this.API_ENDPOINT + 'DCUser/UpdPinByUserId', formData)
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError)
  }

  getCheckLoginByUser(formData: any) {
    return this.http.post(this.API_ENDPOINT + 'DCLogin/CheckLoginByUser', formData)
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError)
  }
}

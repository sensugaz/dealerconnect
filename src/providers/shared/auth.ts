import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/dealerconnect/dealerconnect.constant';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthProvider {

	API_ENDPOINT: string;

	jwtHelper: JwtHelper;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
    this.jwtHelper = new JwtHelper();
  }

	handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	isAuthenticated() {
		return tokenNotExpired(null, localStorage.getItem('token'));
	}

	login(formData: any, store: boolean) {
	  return this.http.post(this.API_ENDPOINT + 'DCLogin/DCLoginByUser', formData)
		  .toPromise()
		  .then((res: any) => {
			  if (res.result == 'SUCCESS' && store == true) {
          let payload = this.jwtHelper.decodeToken(res.token);
          if (payload.userInfo.userTypeId == 2) {
          	localStorage.setItem('customerInfo', JSON.stringify(payload.customerInfo));
					}
					else{ 
						if (payload.userInfo.deptStatus == 'D') {
							switch (payload.userInfo.dcrolesId) {
								case 5:
								case 7:
								case 11:
									localStorage.setItem('customerInfo', JSON.stringify(payload.customerInfo));
									break;
							}
						}
					}

          localStorage.setItem('token', res.token);
			  }

			  return res;
		  })
		  .catch(this.handleError);
	}

	changePassword(formData: any) {
    return this.http.post(this.API_ENDPOINT + 'ChangePassword', formData)
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
	}

  login_old(formData: any) {
	  return this.http.post(this.API_ENDPOINT + 'DCLogin', formData)
		  .toPromise()
		  .then((res: any) => {
			  if (res.result == 'SUCCESS') {
          let payload = this.jwtHelper.decodeToken(res.token);

          if (payload.userInfo.userTypeId == 2) {
          	localStorage.setItem('customerInfo', JSON.stringify(payload.customerInfo));
					}

          localStorage.setItem('token', res.token);
			  }

			  return res;
		  })
		  .catch(this.handleError);
	}

  logout() {
		localStorage.removeItem('token');
		localStorage.removeItem('customerInfo');
	}
	
	ResetPINCode(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'DCLogin/ResetPINCODE', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	forgotPassword(formData: any) {
		return this.http.post(this.API_ENDPOINT + 'ForgotPassword', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getDcPermission(formData: any) {
	  return this.http.post(this.API_ENDPOINT + 'DCAuthen/GetPermission', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getEoMenuPermission(roleId: any) {
    let params = new HttpParams()
      .append('roleId', roleId);

    return this.http.get(this.API_ENDPOINT + 'Permission', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }
}

import { Injectable } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class StorageProvider {

  jwtHelper: JwtHelper;

  constructor() {
    this.jwtHelper = new JwtHelper();
  }

  getUserInfo() {
    return this.jwtHelper.decodeToken(localStorage.getItem('token')).userInfo;
  }

  getCustomerInfo() {
    return JSON.parse(localStorage.getItem('customerInfo'));
  }

  getConfig() {
    return JSON.parse(localStorage.getItem('config'));
  }

  /** permission for e-Ordering menu **/
  getPermission() {
    return JSON.parse(localStorage.getItem('eo_permission'));
  }

  getEoMenuPermission() {
    return JSON.parse(localStorage.getItem('eo_permission'));
  }

  getDcMenuPermission() {
    return JSON.parse(localStorage.getItem('dc_permission'));
  }

  clearLocalStorage(){
    localStorage.clear();
  }

  clear() {
    localStorage.clear();
  }
}

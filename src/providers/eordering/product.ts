import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant'

@Injectable()
export class ProductProvider {

	API_ENDPOINT: string;

	constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constant.API_URL;
	}

	handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	getProduct(paramsData: any) {
    let params = new HttpParams()

		params = params.append('customerId', paramsData.customerId);
    params = params.append('isBTFView', (paramsData.isBTFView == undefined) ? true : paramsData.isBTFView);

    if (paramsData.marketingCodeList != undefined && paramsData.marketingCodeList.length > 0) {
      paramsData.marketingCodeList.forEach(value => {
        params = params.append('marketingCodeList', value);
      })
		}

    if (paramsData.brandCodeList != undefined && paramsData.brandCodeList.length > 0) {
      paramsData.brandCodeList.forEach(value => {
        params = params.append('brandCodeList', value);
      });
    }

    if (paramsData.typeCodeList != undefined && paramsData.typeCodeList.length > 0) {
      paramsData.typeCodeList.forEach(value => {
        params = params.append('typeCodeList', value);
      });
    }

		return this.http.get(this.API_ENDPOINT + 'Product', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getProductInBTF(customerId: string, btf: string) {
		let params = new HttpParams()
			.append('customerId', customerId)
			.append('btf', btf);

		return this.http.get(this.API_ENDPOINT + 'ProductInBTF', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getProductInOrder(customerId: string) {
		let params = new HttpParams()
			.append('customerId', customerId);

		return this.http.get(this.API_ENDPOINT + 'ProductInOrder', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getProductByBarcode(barcode: string) {
		let params = new HttpParams()
			.append('Barcode', barcode);

    return this.http.get(this.API_ENDPOINT + 'EO_Barcode/GetProductByBarcode', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
	}
}

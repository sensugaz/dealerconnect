import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant';

@Injectable()
export class OrderProvider {

  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  addOrder(formData: any) {
    return this.http.post(this.API_ENDPOINT + 'Order', { order: formData })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getOrderInfo(orderId: string) {
    let params = new HttpParams()
      .append('orderId', orderId);

    return this.http.get(this.API_ENDPOINT + 'OrderInfo', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getOrderPrepare(customerId: string, userName: string) {
    let params = new HttpParams()
      .append('customerId', customerId)
      .append('userName', userName);

    return this.http.get(this.API_ENDPOINT + 'OrderPrepare', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getOrderPrecess(customerId: string, startDate: any, endDate: any) {
    let params = new HttpParams()
      .append('customerId', customerId)
      .append('startDocumentDate', startDate)
      .append('endDocumentDate', endDate);

    return this.http.get(this.API_ENDPOINT + 'OrderPrecess', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getRequestDate() {
    return this.http.get(this.API_ENDPOINT + 'RequestDate')
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getOrderHistory(saleOrderNumber: string) {
    let params = new HttpParams()
      .append('saleOrderNumber', saleOrderNumber);

    return this.http.get(this.API_ENDPOINT + 'OrderHistory', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }
}

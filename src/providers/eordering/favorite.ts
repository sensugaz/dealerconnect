import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant'

@Injectable()
export class FavoriteProvider {

	API_ENDPOINT: string;

	constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constant.API_URL;
	}

	private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	getFavorite(customerId: string) {
		let params = new HttpParams()
			.append('customerId', customerId);

		return this.http.get(this.API_ENDPOINT + 'Favorites', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	addFavorite(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'Favorites', { favoriteInfo: formData })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	removeFavorite(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'RemoveFavorites', { favoriteInfo: formData })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
}

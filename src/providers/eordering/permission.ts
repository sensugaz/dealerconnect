import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant';

@Injectable()
export class PermissionProvider {
  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
  }

  handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  // getEoMenuPermission(roleId: any) {
  //   let params = new HttpParams()
  //     .append('roleId', roleId);

  //   return this.http.get(this.API_ENDPOINT + 'Permission', { params: params })
  //     .toPromise()
  //     .then((res: Response) => res)
  //     .catch(this.handleError)
  // }

  // getDcMenuPermission(roleId: any) {
  //   let params = new HttpParams()
  //     .append('roleId', roleId);

  //   return this.http.get(this.API_ENDPOINT + 'Permission', { params: params })
  //     .toPromise()
  //     .then((res: Response) => res)
  //     .catch(this.handleError)
  // }
}

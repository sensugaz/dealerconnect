import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant'

@Injectable()
export class PromotionProvider {

	API_ENDPOINT: string;

	constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constant.API_URL;
	}

  handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	getPromotionInBTF(customerId: any, btf: string) {
    let params = new HttpParams()
			.append('customerId', customerId)
			.append('btf', btf);

		return this.http.get(this.API_ENDPOINT + 'PromotionInBTF', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getPromotion(customerId: any, marketingCodeList: any[]) {
		let params = new HttpParams();

		params = params.append('customerId', customerId);

		if (marketingCodeList != undefined && marketingCodeList.length > 0) {
			marketingCodeList.forEach(value => {
				params = params.append('marketingCodeList', value);
			});
		}

		return this.http.get(this.API_ENDPOINT + 'Promotion', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getPromotionInfo(promotionId: any) {
		let params = new HttpParams()
			.append('promotionId', promotionId);

		return this.http.get(this.API_ENDPOINT + 'PromotionInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	chkPromotionValidate(formData: any) {
		return this.http.post(this.API_ENDPOINT + 'PromotionValidate', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getPromotionOrderCount(promotionId: any, customerId: any) {
		let params = new HttpParams()
			.append('promotionId', promotionId)
			.append('customerId', customerId);

			return this.http.get(this.API_ENDPOINT + 'OrderCountDocument/GetOrderCountDocument', { params: params })
				.toPromise()
				.then((res: Response) => res)
				.catch(this.handleError);
	}
}

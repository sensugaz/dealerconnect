import { HttpClient, HttpParams } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant';

@Injectable()
export class CartProvider {

	API_ENDPOINT: string;

	onUpdated: EventEmitter<any> = new EventEmitter<any>();

  constructor(public http: HttpClient) {
	  this.API_ENDPOINT = Constant.API_URL;
  }

	handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	getCart(userName: string, customerId: string) {
  	let params = new HttpParams()
			.append('userName', userName)
		  .append('customerId', customerId);

  	return this.http.get(this.API_ENDPOINT + 'Cart', { params: params })
		  .toPromise()
		  .then((res: Response) => res)
		  .catch(this.handleError)
	}

	addCart(formData: any) {
		return this.http.post(this.API_ENDPOINT + 'Cart', formData)
			.toPromise()
			.then((res: any) => {
				this.onUpdated.emit('updated');

				return res;
			})
			.catch(this.handleError)
	}

	reloadCart() {
    this.onUpdated.emit('reloaded');
	}

	updateCart(formData: any) {
		return this.http.post(this.API_ENDPOINT + 'UpdateCart', formData)
			.toPromise()
			.then((res: any) => {
				this.onUpdated.emit('updated');

				return res;
			})
			.catch(this.handleError)
	}

	removeCart(formData: any) {
		return this.http.post(this.API_ENDPOINT + 'RemoveCart', formData)
			.toPromise()
			.then((res: any) => {
				this.onUpdated.emit('updated');

				return res;
			})
			.catch(this.handleError)
	}
}

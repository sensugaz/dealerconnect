import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant';

@Injectable()
export class CustomerProvider {

	API_ENDPOINT: string;

	constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constant.API_URL;
	}

	handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	getCustomerInfo(customerId: string) {
    let params = new HttpParams()
      .append('customerId', customerId);

    return this.http.get(this.API_ENDPOINT + 'CustomerInfo', { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
	}

	getCustomer(userId: string) {
		let params = new HttpParams()
			.append('userId', userId);

		return this.http.get(this.API_ENDPOINT + 'EO_Customer/GetCustomer', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);

		// return this.http.get(this.API_ENDPOINT + 'Customer', { params: params })
		// 	.toPromise()
		// 	.then((res: Response) => res)
		// 	.catch(this.handleError);

	}

	checkCustomerBlock(customerCode: string) {
		let params = new HttpParams()
			.append('customercode', customerCode);

		return this.http.get(this.API_ENDPOINT + 'CustomerBlock', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

  updateCustomer(formData: any) {
    return this.http.post(this.API_ENDPOINT + 'CustomerUpdate', formData)
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
	}

	problem(formData: any) {
    return this.http.post(this.API_ENDPOINT + 'CustomerProblem', formData)
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
	}
}

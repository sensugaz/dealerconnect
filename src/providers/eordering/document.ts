import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant';

@Injectable()
export class DocumentProvider {
  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
  }

  handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  getDocument() {
		return this.http.get(this.API_ENDPOINT + 'GeneralDocument')
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
}

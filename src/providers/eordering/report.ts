import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant';

@Injectable()
export class ReportProvider {

  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_REPORT;
  }

  handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  getReportMonthlyFile() {
    return this.http.get(this.API_ENDPOINT + 'report/monthly')
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getReportMonthly(fileName, userName, deptId) {
    let params = new HttpParams()
      .append('userName', userName)
      .append('deptId', deptId);


    return this.http.get(this.API_ENDPOINT + 'report/monthly/' + fileName, { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getReportQuarterFile() {
    return this.http.get(this.API_ENDPOINT + 'report/quarter')
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }

  getReportQuarter(fileName, userName, deptId) {
    let params = new HttpParams()
      .append('userName', userName)
      .append('deptId', deptId);


    return this.http.get(this.API_ENDPOINT + 'report/quarter/' + fileName, { params: params })
      .toPromise()
      .then((res: Response) => res)
      .catch(this.handleError);
  }
}

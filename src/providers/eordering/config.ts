import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constant from '../../app/modules/eordering/eordering.constant'

@Injectable()
export class ConfigProvider {

	API_ENDPOINT: string;
	config: any;

  constructor(public http: HttpClient) {
    this.API_ENDPOINT = Constant.API_URL;
  }

	handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	loadConfig() {
		return this.http.get(this.API_ENDPOINT + 'Config')
			.toPromise()
			.then((res: any) => {
				if (res.result == 'SUCCESS') {
					this.config = res.data.configList[0];
					console.log('loadConfig');
					localStorage.setItem('config', JSON.stringify(res.data.configList[0]));
				}
			})
			.catch(this.handleError);
	}
}

import { HttpClient , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../../app/modules/epocket/epocket.constant';

/*
  Generated class for the DealerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EPDealerProvider {

  API_ENDPOINT: string;

  constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constants.API_URL;
  }

  private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
  }

  getRedeemHistory(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPDealerRedeemHistory/GetList', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }
  
  getRedeemHistoryInfo(RefDocNo: string) {
  	let params = new HttpParams()
		  .append('RefDocNo', RefDocNo);

		return this.http.get(this.API_ENDPOINT + 'EPDealerRedeemHistory/GetInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }

  getDealerReceive(DealerID: string) {
  	let params = new HttpParams()
		  .append('DealerID', DealerID);

		return this.http.get(this.API_ENDPOINT + 'EPDealerReceive/GetDealerReceive', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	updateDealerReceive(formData: object) {
		return this.http.put(this.API_ENDPOINT + 'EPDealerReceive/DealerReceiveConfirm', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getDealerPayment(Begindate : string , Enddate : string , DealerID: string) {
		let params = new HttpParams()
			.append('Begindate', Begindate)
			.append('Enddate', Enddate)
		  .append('DealerID', DealerID);

		return this.http.get(this.API_ENDPOINT + 'EPPayment/GetPayment', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getEPocketNews(usertype: string) {
  	let params = new HttpParams()
		  .append('usertype', usertype);

		return this.http.get(this.API_ENDPOINT + 'EPNews/GetNews', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getEPocketNewsInfo(newid: string) {
  	let params = new HttpParams()
		  .append('newid', newid);

		return this.http.get(this.API_ENDPOINT + 'EPNews/GetNewsInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	updateDealerRejectReceive(formData: object) {
		return this.http.put(this.API_ENDPOINT + 'EPDealerReceive/DealerRejectReceive', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getPayableInfo(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPPayable/GetPayableInfo', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
}

	getRPT01DelaerExchangeCover(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT01DealerExchangeCover', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT01DelaerExchangeCoverInfo(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT01DelaerExchangeCoverInfo', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	GetRPT01DealerExchangeCover_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT01DealerExchangeCover_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	
	getRPT01DelaerExchangeCoverInfo_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT01DelaerExchangeCoverInfo_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT02DealerRedeemPainter(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT02DealerRedeemPainter', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT02DealerRedeemPainterInfo(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT02DealerRedeemPainterInfo', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	
	getRPT02DealerRedeemByPainter(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT02DealerRedeemByPainter', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT02DealerRedeemByPainter_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT02DealerRedeemByPainter_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT02DealerRedeemPainterInfo_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT02DealerRedeemPainterInfo_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	
	getRPT03DealerRedeemBT(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT03DealerRedeemBT', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT03DealerRedeemBT_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT03DealerRedeemBT_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT04DealerRedeemHistory(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT04DealerRedeemHistory', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	
	getRPT04DealerRedeemHistoryInfo(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT04DealerRedeemHistoryInfo', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT04DealerRedeemHistory_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT04DealerRedeemHistory_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT04DealerRedeemHistoryInfo_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT04DealerRedeemHistoryInfo_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT05CoinStock(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT05CoinStock', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT05CoinStockInfo(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT05CoinStockInfo', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT05DealerCoinStock(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT05DealerCoinStock', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT05DealerCoinStock_Excel(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT05DealerCoinStock_Excel', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getVendorByDealerId(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfile/GetVendorByDealerId', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getPainter(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPDealerReceive/GetEPPainter', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getRPTDealerPayment(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPTEPReportPaymentInfo', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	getBankBranchByBankCode(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPBank/GetBankBranchByBankCode', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	getVendorCity(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPVendor/GetVendorCity', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}
	
	getVendorDistrict(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPVendor/GetVendorDistrictByCityCode', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	sendSMSPainter(formData : object){
		return this.http.post(this.API_ENDPOINT + 'EPSendSMS/SendSMSPainter', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}





}

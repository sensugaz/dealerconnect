import { HttpClient , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../../app/modules/epocket/epocket.constant';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EPSaleProvider {

  API_ENDPOINT: string;

  //API_ENDPOINT: string = 'http://103.22.181.36:8011/api/' ;

  constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constants.API_URL;
  }

  private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
  }


	GetSaleRedeem(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeem/GetSaleDealerRedeem', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetSaleRedeemInfo(dealerID: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeemInfo', dealerID)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetProductBrandByDealer(dealerID: object) {
		return this.http.post(this.API_ENDPOINT + 'EPProductBrandByDealer', dealerID)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	UpdSaleRedeemQty(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeemQtyUpd', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetSaleRedeemQtyAllMsg(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeemConfirm/SaleConfirmMsg', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetSaleRedeemQtyAll(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeemConfirm/SaleConfirm', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getSaleProductInfo(BTFS: string,DEALER_ID: string) {
  	let params = new HttpParams()
		  .append('BTFS', BTFS).append('DEALER_ID',DEALER_ID);

		return this.http.get(this.API_ENDPOINT + 'EPSaleProductInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getSaleRedeemHistory(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeemHistory/GetList', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }
  
  getSaleRedeemHistoryInfo(RefDocNo: string) {
  	let params = new HttpParams()
		  .append('RefDocNo', RefDocNo);

		return this.http.get(this.API_ENDPOINT + 'EPSaleRedeemHistory/GetInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getSaleRedeemReceive(SALEID: string) {
  	let params = new HttpParams()
		  .append('SALEID', SALEID);

		return this.http.get(this.API_ENDPOINT + 'EPWarehouse/GetInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	updSaleRedeemReceive(SALEID: string) {
  	let params = new HttpParams()
		  .append('SALEID', SALEID);

		return this.http.get(this.API_ENDPOINT + 'EPWarehouse/Confrim', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getProfileInfo(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfile/GetProfileInfo', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getChangeProfileInfo(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfile/GetChangeProfileInfo', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	updProfile(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfile/UpdateProfile', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	updProfileSendMail(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfile/sendMail', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}


	getWarehouseRedeem(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPWarehouseRedeem/GetWHReceive', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getSentToWarehouse(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPWarehouseRedeem/senttoWH', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getBank(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPBank/GetList', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getProfileHistoryRequest(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfileHistory/GetProfileHistoryRequest', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	getProfileHistoryProcess(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPChangeProfileHistory/GetProfileHistoryProcess', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	updSaleReject(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeem/SaleRejectReceive', formData)
		.toPromise()
		.then((res: Response) => res)
		.catch(this.handleError);
	}

	getRPT06SaleReceive(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT06SaleReceive', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getRPT06SaleReceiveInfo(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT06SaleReceiveInfo', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getRPT06SaleReceive_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT06SaleReceive_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getRPT06SaleReceiveInfo_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT06SaleReceiveInfo_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	

	GetRPT07SaleRedeem(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT07SaleRedeem', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getGetRPT07SaleRedeemInfo(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT07SaleRedeemInfo', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetRPT07SaleRedeem_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT07SaleRedeem_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getGetRPT07SaleRedeemInfo_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT07SaleRedeemInfo_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	

	GetRPT08SaleCoinStock(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT08SaleCoinStock', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getGetRPT08SaleCoinStockDocNo(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT08SaleCoinStockDocNo', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetRPT08SaleCoinStock_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT08SaleCoinStock_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getGetRPT08SaleCoinStockDocNo_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT08SaleCoinStockDocNo_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getGetRPT09WaitDealerConfirm(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT09WaitDealerConfirm', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getGetRPT09WaitDealerConfirm_Excel(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPReport/GetRPT09WaitDealerConfirm_Excel', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
}

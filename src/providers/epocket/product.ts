import { HttpClient , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../../app/modules/epocket/epocket.constant';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EPProductProvider {

  API_ENDPOINT: string;

  //API_ENDPOINT: string = 'http://103.22.181.36:8011/api/' ;

  constructor(public http: HttpClient) {
		this.API_ENDPOINT = Constants.API_URL;
  }

  private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
  }
  
  getProductBrand() {
    console.log(this.API_ENDPOINT);
		return this.http.get(this.API_ENDPOINT + 'EPProductBrand')
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }
  
  getProductBrandSize(BrandCode: string,DealerID: string) {
  	let params = new HttpParams()
			.append('BrandCode', BrandCode)
			.append('DealerID', DealerID);
		
		console.log(BrandCode,' >> ',DealerID);

		return this.http.get(this.API_ENDPOINT + 'EPProductBrandSize', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }

  getProductInfo(BTFS: string , DealerID: string) {
  	let params = new HttpParams()
			.append('BTFS', BTFS)
			.append('DealerID', DealerID);

		return this.http.get(this.API_ENDPOINT + 'EPProductInfo', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }

  addProductDealerRedeem(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPDealerRedeem/AddOrderToCart', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getCartInfo(DealerID: string , PainterID : string) {

		let params = new HttpParams()
			.append('DealerID', DealerID)
			.append('PainterID', PainterID);

		return this.http.get(this.API_ENDPOINT + 'EPDealerRedeem/GetCartDetail', { params: params } )
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	RemoveOrderAll(DealerID: string) {
  	let params = new HttpParams()
		  .append('DealerID', DealerID);

		return this.http.delete(this.API_ENDPOINT + 'EPDealerRedeem/RemoveOrderAll', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	RemoveOrder(RID: string) {
  	let params = new HttpParams()
		  .append('RID', RID);

		return this.http.delete(this.API_ENDPOINT + 'EPDealerRedeem/RemoveOrder', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}
	
	Checkout(formData: object) {
		return this.http.put(this.API_ENDPOINT + 'EPDealerRedeem/DealerCheckOut', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	GetSaleRedeem(formData: object) {
		return this.http.post(this.API_ENDPOINT + 'EPSaleRedeem', formData)
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
	}

	getPainter(MobileNo: string ,  Username: string) {
  	let params = new HttpParams()
			.append('MobileNo', MobileNo)
			.append('Username', Username);

		return this.http.get(this.API_ENDPOINT + 'EPDealerRedeem/GetPainter', { params: params })
			.toPromise()
			.then((res: Response) => res)
			.catch(this.handleError);
  }

}

import { AbstractControl } from '@angular/forms';

export class PasswordValidation {
  static MatchPassword(AC: AbstractControl) {
    let newpassword = AC.get('newpassword').value;
    let newpassconf = AC.get('newpassconf').value;

    if (newpassword != newpassconf) {
      AC.get('newpassconf').setErrors({ MatchPassword: true });
    } else {
      return null;
    }
  }
}
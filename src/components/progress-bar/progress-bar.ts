import { Component, Input } from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html'
})
export class ProgressBarComponent {

  @Input('progress') progress;
  @Input('mode') mode;
  @Input('text') text;
  @Input('color') color;

  width: any;

  constructor() {
    this.width = this.progress;
  }

}

import { NgModule } from '@angular/core';
import { EoOrderComponent } from './eordering/eo-order/eo-order';
import { IonicModule } from 'ionic-angular';
import { NgPipesModule } from 'ngx-pipes';
import { EoOrderDetailComponent } from './eordering/eo-order-detail/eo-order-detail';
import { ProgressBarComponent } from './progress-bar/progress-bar';
import { EoHomeComponent } from './eordering/eo-home/eo-home';
import { EoNewsComponent } from './eordering/eo-news/eo-news';
import { EoDocumentComponent } from './eordering/eo-document/eo-document';
import { EoFavoriteComponent } from './eordering/eo-favorite/eo-favorite';
import { EoHistoryComponent } from './eordering/eo-history/eo-history';
import { EoNewsDetailComponent } from './eordering/eo-news-detail/eo-news-detail';
import { EoReportComponent } from './eordering/eo-report/eo-report';

@NgModule({
	declarations: [
	  EoOrderComponent,
    EoOrderDetailComponent,
    ProgressBarComponent,
    EoHomeComponent,
    EoNewsComponent,
    EoDocumentComponent,
    EoFavoriteComponent,
    EoFavoriteComponent,
    EoHistoryComponent,
    EoNewsDetailComponent,
    EoReportComponent,
  ],
	imports: [
	  IonicModule,
    NgPipesModule
  ],
	exports: [
	  EoOrderComponent,
    EoOrderDetailComponent,
    ProgressBarComponent,
    EoHomeComponent,
    EoNewsComponent,
    EoDocumentComponent,
    EoFavoriteComponent,
    EoFavoriteComponent,
    EoHistoryComponent,
    EoNewsDetailComponent,
    EoReportComponent,
  ],
  entryComponents: [
    EoOrderComponent,
    EoOrderDetailComponent
  ]
})
export class ComponentsModule {}

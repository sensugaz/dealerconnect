import { Component } from '@angular/core';
import { StorageProvider } from '../../../providers/shared/storage';
import { NavController, Platform, App } from 'ionic-angular';
import { EoReportMonthlyPage } from '../../../pages/eordering/eo-report-monthly/eo-report-monthly';
import { EoReportQuarterPage } from '../../../pages/eordering/eo-report-quarter/eo-report-quarter';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'eo-report',
  templateUrl: 'eo-report.html'
})
export class EoReportComponent {

  userInfo: any;
  permissions: any;

  constructor(private navCtrl: NavController,
              private storageProvider: StorageProvider,
              private platform: Platform,
              private app: App) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.permissions = this.storageProvider.getEoMenuPermission();
    
    this.backButtonHandler();
  }

  verifyPermission(menuCode) {
    let menu = this.permissions.filter(item => item.menuCode == menuCode && item.privilegeId != 0);

    return (menu != undefined) ? (menu.length) ? menu[0] : false : true;
  }

  openMonthly() {
    this.navCtrl.push(EoReportMonthlyPage);
  }

  openQuarter() {
    this.navCtrl.push(EoReportQuarterPage);
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}
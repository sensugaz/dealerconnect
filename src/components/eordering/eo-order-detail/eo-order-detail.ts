import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'eo-order-detail',
  templateUrl: 'eo-order-detail.html'
})
export class EoOrderDetailComponent {

  order: any;

  constructor(private viewCtrl: ViewController,
              public navParams: NavParams) {

    this.order = this.navParams.get('order');
  }

  close() {
    this.viewCtrl.dismiss();
  }
}

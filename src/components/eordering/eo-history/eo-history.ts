import { EoProductDetailPage } from './../../../pages/eordering/eo-product-detail/eo-product-detail';
import { ProductProvider } from './../../../providers/eordering/product';
import { StorageProvider } from './../../../providers/shared/storage';
import { NavController, NavParams, LoadingController, ToastController, Events, Platform, App } from 'ionic-angular';
import { Component } from '@angular/core';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'eo-history',
  templateUrl: 'eo-history.html'
})
export class EoHistoryComponent {

  userInfo: any;
  customerInfo: any;
  config: any;
  loading: any;
  products: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storageProvider: StorageProvider,
              private productProvider: ProductProvider,
              private loadingCtrl: LoadingController,
              private platform: Platform,
              private events: Events,
              private app: App) {

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.fetchProduct();
    this.backButtonHandler();
  }

  selectProduct(product) {
    this.navCtrl.push(EoProductDetailPage, { product: product, mode: 'POP', page: 3 });
    //this.events.publish('product:open', { product: product, mode: 'POP' });
  }

  fetchProduct() {
    this.showLoading();

    this.productProvider.getProductInOrder(this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          let product = [];

          res.data.productInOrderList.forEach(value => {
            var _ = product.filter(item => {
              if ((item.btfWeb == value.btfWeb) && (item.documentDate == value.documentDate)) {
                return item;
              }
            })[0];

            if (_ == undefined) {
              product.push(value);
            }
          });

          this.products = product;
        }
        
        this.hideLoading();
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}

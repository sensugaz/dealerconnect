import { EorderetcProvider } from './../../../providers/eordering/eorderetc';
import { StorageProvider } from './../../../providers/shared/storage';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Component } from '@angular/core';

/**
 * Generated class for the EoNewsDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'eo-news-detail',
  templateUrl: 'eo-news-detail.html'
})
export class EoNewsDetailComponent {

  userInfo: any;
  customerInfo: any;
  config: any;
  loading: any;
  DataHD: any;
  DataDT: any;
  text: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storageProvider: StorageProvider,
    private eorderetcProvider : EorderetcProvider,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController) {
  
    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.loading = true;

    console.log('Before new details');

    //this.LoadData();
  }

}

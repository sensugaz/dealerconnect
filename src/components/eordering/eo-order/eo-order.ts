import { Component } from '@angular/core';
import { OrderProvider } from '../../../providers/eordering/order';
import { StorageProvider } from '../../../providers/shared/storage';
import { LoadingController, ModalController, NavController, Platform, App } from 'ionic-angular';
import { EoOrderDetailComponent } from '../eo-order-detail/eo-order-detail';
import { CalendarModal } from 'ion2-calendar';
import { EoOrderListPage } from '../../../pages/eordering/eo-order-list/eo-order-list';
import * as moment from 'moment';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { DatePicker } from '@ionic-native/date-picker';

@Component({
  selector: 'eo-order',
  templateUrl: 'eo-order.html'
})
export class EoOrderComponent {
  config: any;
  userInfo: any;
  customerInfo: any;
  startDate: any;
  endDate: any;
  orders: any[];
  months: any;
  loading: any;

  logs: any;

  constructor(public navCtrl: NavController,
              private orderProvider: OrderProvider,
              private storageProvider: StorageProvider,
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController,
              private platform: Platform,
              private datePicker: DatePicker,
              private app: App) {

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.startDate = moment().subtract(this.config.mobileorderdaterang, 'days').toDate();
    this.endDate = moment().toDate();

    this.fetchOrder();
    this.backButtonHandler();
  }

  openOrderModal(order) {
    this.modalCtrl.create(EoOrderDetailComponent, { order: order }).present();
  }

  openStartDate() {
    this.datePicker.show({
      date: this.startDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก'
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.startDate = date;
      }
    })
    .catch(() => {});
  }

  openEndDate() {
    this.datePicker.show({
      date: this.endDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก'
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.endDate = date;
      }
    })
    .catch(() => {});
  }

  searchOrder() {
    this.fetchOrder();
  }

  gotoOrderList(month) {
    this.navCtrl.push(EoOrderListPage, { month: month });
  }

  fetchOrder() {
    this.showLoading();

    this.orderProvider.getOrderPrecess(this.customerInfo.customerId, moment(this.startDate).format('YYYY-MM-DD'), moment(this.endDate).format('YYYY-MM-DD'))
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.months = [];


          res.data.orderProcessList.forEach(value => {
            let month = moment(value.docDate).format('MM');
            let year = moment(value.docDate).format('YYYY');
            let exists = this.months.filter(item => item.date == year + '-' + month)[0];

            if (exists == undefined || this.months.length == 0) {
              this.months.push({
                month: month,
                monthNameTH: this.getMonthNameTH(parseInt(month) - 1),
                year: year,
                date: year + '-' + month
              });
            }
          });
        }

        this.hideLoading();
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  getMonthNameTH(index) {
    const month = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฎาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม'
    ];

    return month[index];
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        this.hideLoading();
        
        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
        
      });
    }
  }
}

import { EoProductDetailPage } from './../../../pages/eordering/eo-product-detail/eo-product-detail';
import { FavoriteProvider } from './../../../providers/eordering/favorite';
import { StorageProvider } from './../../../providers/shared/storage';
import { NavController, NavParams, LoadingController, ToastController, Events, Platform, App } from 'ionic-angular';
import { Component } from '@angular/core';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'eo-favorite',
  templateUrl: 'eo-favorite.html'
})
export class EoFavoriteComponent {

  userInfo: any;
  customerInfo: any;
  config: any;
  loading: any;
  products: any;
  slice: number;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storageProvider: StorageProvider,
              private favoriteProvider: FavoriteProvider,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private platform: Platform,
              private app: App,
              private events: Events) {
  
    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.fetchProduct();

    this.backButtonHandler();
  }


  fetchProduct(){
    this.showLoading();

    this.favoriteProvider.getFavorite(this.customerInfo.customerId)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.products = res.data.favoriteList;
      }

      this.hideLoading();
    });
  }

  selectedProduct(product) {
    this.navCtrl.push(EoProductDetailPage, { product: product, mode: 'POP', page: 2 });
    //this.events.publish('product:open', { product: product, mode: 'POP' });
  }

  setFavorite($event, product) {
    $event.stopPropagation();

    let formData = {
      customerId: this.customerInfo.customerId,
      userName: this.userInfo.userName,
      btfCode: product.btf
    };

    if (product.isFavorite) {
      this.favoriteProvider.removeFavorite(formData)
        .then(res => {
          if (res.result == 'SUCCESS') {
            this.products.splice(this.products.indexOf(product), 1);

            this.toastCtrl.create({
              message: 'ลบรายการโปรดเรียบร้อย',
              duration: 1500
            }).present();
          }
        });
    }
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}

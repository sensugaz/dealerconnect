import { NavController, NavParams, Platform, App} from 'ionic-angular';
import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EoNewsListPage } from '../../../pages/eordering/eo-news-list/eo-news-list';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'eo-news',
  templateUrl: 'eo-news.html'
})
export class EoNewsComponent {

  userInfo: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private platform: Platform,
              private iab: InAppBrowser,
              private app: App) {

    this.userInfo = this.storageProvider.getUserInfo();
    
    this.backButtonHandler();
  }

  openNewsToa() {
    if (this.platform.is('android')) {
      this.iab.create('https://toagroup.com/news-and-events/list').show();
    } else {
      this.iab.create('https://toagroup.com/news-and-events/list', '_system').show();
    }
  }

  openNewsEor() {
    this.navCtrl.push(EoNewsListPage);
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}

import { DocumentProvider } from './../../../providers/eordering/document';
import { FavoriteProvider } from './../../../providers/eordering/favorite';
import { StorageProvider } from './../../../providers/shared/storage';
import { NavController, NavParams, LoadingController, ToastController, Platform } from 'ionic-angular';
import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'eo-document',
  templateUrl: 'eo-document.html'
})
export class EoDocumentComponent {

  userInfo: any;
  customerInfo: any;
  config: any;
  loading: any;
  documents: any;
  categorys: any[];
  type: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private documentProvider: DocumentProvider,
              private platform: Platform,
              private iab: InAppBrowser) {
  
    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.loading = true;
    this.type = '';

    this.fetchDocument();
    this.backButtonHandler();
  }

  fetchDocument(){
    this.loading = true;
    this.categorys = [{
      value: '',
      label: 'ทั้งหมด'
    }];

    this.documentProvider.getDocument()
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.documents = res.data.generalDocumentList;

          this.documents.forEach(value => {
            let _ = this.categorys.filter(item => item.value == value.categoryCode)[0];

            if (_ == undefined) {
              this.categorys.push({
                value: value.categoryCode,
                label: value.categoryDesc
              });
            }
          });
        }

        this.loading = false;
      });
  }

  downloadFile(fileName) {
    if (this.platform.is('ios')) {
      this.iab.create(this.config.partFileDocument + '/' + fileName, '_system').show();
    } else {
      this.iab.create(this.config.partFileDocument + '/' + fileName).show();
    }
  }

  previewFile(fileName) {
    if (this.platform.is('ios')) {
      this.iab.create(this.config.partFileDocument + '/' + fileName, '_system').show();
    } else {
      this.iab.create(this.config.partFileDocument + '/' + fileName).show();
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        //console.log('EoFavorite');
        //this.navCtrl.pop();
      });
    }
  }
}

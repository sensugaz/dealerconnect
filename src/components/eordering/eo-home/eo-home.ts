import { Platform } from 'ionic-angular/platform/platform';
import { ConfigProvider } from './../../../providers/eordering/config';
import { StorageProvider } from './../../../providers/shared/storage';
import { NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { EoMenuPage } from '../../../pages/eordering/eo-menu/eo-menu';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { EoStorePage } from '../../../pages/eordering/eo-store/eo-store';
import { MarketingProvider } from '../../../providers/eordering/marketing';
import { PromotionProvider } from '../../../providers/eordering/promotion';
import { EoPromotionPage } from '../../../pages/eordering/eo-promotion/eo-promotion';

@Component({
  selector: 'eo-home',
  templateUrl: 'eo-home.html'
})
export class EoHomeComponent {

  userInfo: any;
  customerInfo: any;
  slides: any;
  config: any;
  loading: any;
  marketings: any[];
  maketingSelect: any[];
  promotions: any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private marketingProvider: MarketingProvider,
              private promotionProvider: PromotionProvider,
              private loadingCtrl: LoadingController,
              private platform: Platform,
              private app: App) {

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.marketings = [];
    this.promotions = [];

    this.slides = [
      { image: this.config.partImgHome + '/' + this.config.imgHomeA1 },
      { image: this.config.partImgHome + '/' + this.config.imgHomeA2 },
      { image: this.config.partImgHome + '/' + this.config.imgHomeA3 }
    ];
   
    this.fetchMaketing();
    this.fetchPromotion();
  }

  fetchMaketing() {
    this.marketingProvider.getMarketing(this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.marketings = res.data.marketingList;
        }
      });
  }

  fetchPromotion() {
    this.showLoading();

    this.promotionProvider.getPromotion(this.customerInfo.customerId, this.maketingSelect)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.promotions = res.data.promotionHDList;

          this.hideLoading();
        }
      });
  }

  selectedPromotion(promotion) {
    this.navCtrl.push(EoPromotionPage, { promotion: promotion });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}
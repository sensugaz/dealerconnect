import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../../providers/shared/auth';
import { DcForgotPasswordPage } from '../dc-forgot-password/dc-forgot-password';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { StorageProvider } from '../../../providers/shared/storage';
import { PermissionProvider } from './../../../providers/eordering/permission';

@Component({
  selector: 'page-dc-login',
  templateUrl: 'dc-login.html',
})
export class DcLoginPage {

  form: FormGroup;
  btnLoading: boolean = false;
  action: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public authProvider: AuthProvider,
              public alertCtrl: AlertController,
              public permissionProvider: PermissionProvider,
              public storageProvider: StorageProvider) {
    
    // events
    this.action = this.navParams.get('action');
    
    this.form = this.formBuilder.group({
		  username: ['', Validators.required],
		  password: ['', Validators.required]
	  });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DcLoginPage');  
  }

  gotoForgot() {
    this.navCtrl.push(DcForgotPasswordPage);
  }

  login(form) {
    let formData = {
			Username: form.username.toLowerCase(),
			Password: form.password
    };
    
    this.btnLoading = true;

    this.authProvider.login(formData, true)
      .then(res => {
        if (res.result == 'SUCCESS') {
          let userInfo = this.storageProvider.getUserInfo();
          
          // set dc menu permission
          this.authProvider.getDcPermission({ ROLED_ID : userInfo.dcrolesId.toString() })
            .then(res => {
              if (res.result == 'SUCCESS') {
                localStorage.setItem('dc_permission', JSON.stringify(res.data.permission));
              }
            })
            .then(() => {
              // set eo menu permission
              this.authProvider.getEoMenuPermission(userInfo.roleId)
                .then(res => {
                  if (res.result == 'SUCCESS') {
                    localStorage.setItem('eo_permission', JSON.stringify(res.data.permissionList));
                  }
                });
            })
            .then(() => {
              this.navCtrl.setRoot(DealerConnect, { from: 'login', action: this.action });
            });
        } else {
          this.alertCtrl.create({
            title: res.remarkheader,
            message: res.remarktext,
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();
        }

        this.btnLoading = false;
        this.form.reset();
      });
  }
}

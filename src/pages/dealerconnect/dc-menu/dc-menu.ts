import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, AlertController, Platform, ViewController } from 'ionic-angular';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'page-dc-menu',
  templateUrl: 'dc-menu.html',
})
export class DcMenuPage {
  [x: string]: any;

  showedAlert: boolean;
  //confirmAlert:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public platform: Platform,
              private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DcMenuPage');
  }

  selectMenu(menu) {
    this.viewCtrl.dismiss({ menu: menu });
  }
  /*
  gotoResetPin(){
    this.navCtrl.setRoot(DealerConnect, { page: PinConfirmPage, action : 'resetpin' }, {
      animate: true,
      animation: 'ease-in'
    });
  }

  gotoExitApp() {
    this.showedAlert = true;
    this.confirmAlert = this.alertCtrl.create({
        title: "Dealer Connect",
        mode: 'ios',
        message: "คุณต้องการออกจากระบบหรือไม่ ?",
        buttons: [
            {
                text: 'ไม่ต้องการ',
                handler: () => {
                    this.showedAlert = false;
                    return;
                }
            },
            {
                text: 'ต้องการ',
                handler: () => {
                    this.platform.exitApp();
                }
            }
        ]
    });

    this.confirmAlert.present();
  }
  */

}

import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from '../../../other/password-validation';
import { Validations } from '../../../other/validations';
import { AuthProvider } from '../../../providers/shared/auth';

@Component({
  selector: 'page-dc-change-password',
  templateUrl: 'dc-change-password.html',
})
export class DcChangePasswordPage {

  userInfo: any;
  customerInfo: any;
  form: FormGroup;
  btnLoading: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private formBuilder: FormBuilder,
              private alertCtrl: AlertController,
              private authProvider: AuthProvider) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.btnLoading = false;

    this.form = this.formBuilder.group({
      password: ['', Validators.required],
      newpassword: ['', Validators.required],
      newpassconf: ['', Validators.required]
    }, {
      validator: PasswordValidation.MatchPassword
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DcChangePasswordPage');
  }

  gotoHome() {
    this.navCtrl.pop();
  }

  changePassword(form) {
    let formData = {
      userName: this.userInfo.userName,
      password: form.password,
      newPassword: form.newpassconf
    };

    this.btnLoading = true;

    this.authProvider.changePassword(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.alertCtrl.create({
            title: 'สำเร็จ',
            message: 'เปลี่ยนรหัสผ่านสำเร็จ',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();
        } else {
          switch (res.validateCode) {
            case '002':
              this.alertCtrl.create({
                title: 'แจ้งเตือน',
                message: 'รหัสผ่านเก่าไม่ถูกต้อง',
                mode: 'ios',
                enableBackdropDismiss: false,
                buttons: ['ตกลง']
              }).present();
              break;
          }
        }

        this.btnLoading = false;

        this.form.reset();
      });
  }
}

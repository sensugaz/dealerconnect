import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, PopoverController } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { AuthProvider } from '../../../providers/shared/auth';
import { Eordering } from '../../../app/modules/eordering/eordering.componet';
import { EPocket } from '../../../app/modules/epocket/epocket.componet';
import { DcMenuPage } from '../dc-menu/dc-menu';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { DcChangePasswordPage } from '../dc-change-password/dc-change-password';
import { DcContactPage } from '../dc-contact/dc-contact';
import { DcNewsPage } from '../dc-news/dc-news';
import { DcLoginPage } from '../dc-login/dc-login';
import { ConfigProvider } from "../../../providers/eordering/config";

@Component({
  selector: 'page-dc-home',
  templateUrl: 'dc-home.html',
})
export class DcHomePage {

  apps: any[];
  userInfo: any;
  permission: any;
  config: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private authProvider: AuthProvider,
              private alertCtrl: AlertController,
              private popoverCtrl: PopoverController) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.permission = this.storageProvider.getDcMenuPermission();
    this.config = this.storageProvider.getConfig();
    
    this.apps = [
      {
        sysid: 1,
        name: 'eOrdering',
        component: Eordering,
        mode: 'root',
        icon: './assets/icon/DealerConnect/DCEORDERING.png',
        permission: 'X'
      },
      {
        sysid: 3,
        name: 'ePocket',
        component: EPocket,
        mode: 'root',
        icon: './assets/icon/DealerConnect/DCEPOCKET.png',
        permission: 'X'
      },
      {
        name: 'eNews',
        component: DcNewsPage,
        mode: 'push',
        icon: './assets/icon/DealerConnect/DCNEWS.png',
        permission : 'Y'
      },
      {
        name: 'Contact',
        component: DcContactPage,
        mode: 'push',
        icon: './assets/icon/DealerConnect/DCCONTACT.png',
        permission : 'Y'
      }
    ];
  }

  ionViewDidLoad() {
    this.apps.forEach(value => {
      if (value.sysid != undefined) {
        let pm = this.permission.filter(item => {
          return item.sysid == value.sysid
        })[0];

        value.permission = (pm != undefined && pm.active > 0) ? 'Y' : 'X';
      }
    });
  }

  selectApp(app) {
    if (app.component != '' && app.mode == 'root') {
      if(app.permission == 'Y') {
        if (app.sysid == 1) {
          if (this.config.shutdownSystem == 1) {
            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message : 'ไม่สามารถเข้าใช้งานได้เนื่องจากอยู่นอกเหนือเวลาทำการ',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: ['ตกลง']
            }).present();
          } else {
            this.navCtrl.setRoot(app.component);
          }
        } else {
          this.navCtrl.setRoot(app.component);
        }
      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message : 'ท่านไม่มีสิทธ์ใช้งาน <br> กรุณาติดต่อผู้ดูแลระบบ',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    } else if (app.mode == 'push') {
      this.navCtrl.push(app.component);
    }
  }

  openMenu($event) {
    let popover = this.popoverCtrl.create(DcMenuPage);

    popover.present({
      ev: $event
    });

    popover.onDidDismiss(data => {
      if (data != undefined) {
        switch (data.menu) {
          case 1:
            this.navCtrl.push(DcChangePasswordPage);
            break;
          case 2:
            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'คุณต้องการเปลี่ยน PIN ใช่หรือไม่ ?',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'ยกเลิก',
                  role: 'cancel'
                },
                {
                  text: 'ตกลง',
                  handler: () => {
                    this.navCtrl.setRoot(DcLoginPage, { action: 'CP' }, {
                      animate: true,
                      animation: 'ease-in'
                    });
                  }
                }
              ]
            }).present();
            break;
          case 3:
            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'คุณต้องการเปลี่ยนชื่อผู้ใช้งานใช่หรือไม่ ?',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'ยกเลิก',
                  role: 'cancel'
                },
                {
                  text: 'ตกลง',
                  handler: () => {
                    this.storageProvider.clear();
                    this.navCtrl.setRoot(DealerConnect);
                  }
                }
              ]
            }).present();
            break;
          case 4:
            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'คุณต้องการออกจากระบบใช่หรือไม่ ?',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'ยกเลิก',
                  role: 'cancel'
                },
                {
                  text: 'ตกลง',
                  handler: () => {
                    this.navCtrl.setRoot(DealerConnect, { action: '' });
                  }
                }
              ]
            }).present();
            break;
        }
      }
    });
  }
}

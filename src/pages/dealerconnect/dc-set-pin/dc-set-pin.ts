import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { UserProvider } from '../../../providers/dealerconnect/user';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { DcHomePage } from '../dc-home/dc-home';

@Component({
  selector: 'page-dc-set-pin',
  templateUrl: 'dc-set-pin.html',
})
export class DcSetPinPage {

  userInfo: any;
  step: number;
  number1: any;
  number2: any;

  loading: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private alertCtrl: AlertController,
              private userProvider: UserProvider,
              private loadingCtrl: LoadingController) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.step = 1;
    this.number1 = '';
    this.number2 = '';
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DcSetNewpinPage');
  }

  keypress(key) {
    if (this.step == 1) {
      if (this.number1.length < 6) {
        this.number1 += key.toString();

        if (this.number1.length == 6) {
          this.step = 2;
        }
      }
    } else {
      if (this.number2.length < 6) {
        this.number2 += key.toString();

        if (this.number2.length == 6) {
          if (this.number1 == this.number2) {
            let formData = {
              USER_ID: this.userInfo.userId.toString(),
              PIN: this.number2
            };

            this.showLoading();

            this.userProvider.setNewpin(formData)
              .then(res => {
                if (res.result == 'SUCCESS') {
                  localStorage.setItem('token', res.token);
                  
                  this.navCtrl.setRoot(DcHomePage);
                }
              });

            this.hideLoading();
          } else {
            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'PIN ทั้งสองไม่เหมือนกัน',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: ['ตกลง']
            }).present();

            this.number2 = '';
          }
        }
      }
    }
  }

  delNumber() {
    if (this.step == 1) {
      if (this.number1.length > 0) {
        this.number1 = this.number1.slice(0, -1);
      }
    } else {
      if (this.number2.length > 0) {
        this.number2 = this.number2.slice(0, -1);
      }
    }
  }

  clearNumber() {
    if (this.step == 1) {
      this.number1 = '';
    } else {
      this.number2 = '';
    }
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}

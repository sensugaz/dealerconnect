import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { DcHomePage } from '../dc-home/dc-home';
import { AuthProvider } from '../../../providers/shared/auth';
import { DcLoginPage } from '../dc-login/dc-login';

@Component({
  selector: 'page-dc-pin-request',
  templateUrl: 'dc-pin-request.html',
})
export class DcPinRequestPage {

  number: string;
  userInfo: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private alertCtrl: AlertController,
              private authProvider: AuthProvider) {

    this.number = '';
    this.userInfo = this.storageProvider.getUserInfo();
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DcPinRequestPage');
  }

  keypress(key) {
    if (this.number.length < 6) {
      this.number += key.toString();

      if (this.number.length == 6) {
        if (this.userInfo.PIN == this.number) {
          this.navCtrl.setRoot(DcHomePage);
        } else {
          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'คุณใส่รหัส PIN ไม่ถูกต้อง กรุณาป้อนรหัสใหม่อีกครั้ง',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();

          this.number = '';
        }
      }
    }
  }

  delNumber() {
    if (this.number.length > 0) {
      this.number = this.number.slice(0, -1);
    }
  }

  forgotPin() {
    this.alertCtrl.create({
      title: 'แจ้งเตือน',
      message: 'คุณต้องการรีเช็ท PIN หรือไม่ ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก'
        },
        {
          text: 'ตกลง',
          handler: (data) => {
            this.storageProvider.clear();
            this.navCtrl.setRoot(DcLoginPage, { action: 'RP' });
          }
        }
      ]
    }).present();
  }

  logout() {
    this.alertCtrl.create({
      title: 'แจ้งเตือน',
      message: 'ต้องการเปลี่ยนผู้ใช้งานใช่หรือไม่ ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก'
        },
        {
          text: 'ตกลง',
          handler: () => {
            this.storageProvider.clear();
            this.navCtrl.setRoot(DealerConnect);
          }
        }
      ]
    }).present();
  }
}

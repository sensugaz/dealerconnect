import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-dc-news-detail',
  templateUrl: 'dc-news-detail.html',
})
export class DcNewsDetailPage {

  news: any;
  config: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private platform: Platform) {

    this.news = this.navParams.get('news');
    this.config = this.navParams.get('config');

    this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.viewCtrl.dismiss();
      });
    }
  }
}

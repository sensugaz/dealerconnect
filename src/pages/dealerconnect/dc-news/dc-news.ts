import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, Platform, App } from 'ionic-angular';
import { NewsProvider } from '../../../providers/dealerconnect/news';
import { StorageProvider } from '../../../providers/shared/storage';
import { DcNewsDetailPage } from '../dc-news-detail/dc-news-detail';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'page-dc-news',
  templateUrl: 'dc-news.html',
})
export class DcNewsPage {

  config: any;
  newsHD: any[];
  newsDT: any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private newsProvider: NewsProvider,
              private modalCtrl: ModalController,
              private storageProvider: StorageProvider,
              private platform: Platform,
              private app: App) {

    this.config = this.storageProvider.getConfig();

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.fetchNews();
  }

  openNews(news) {
    let modal = this.modalCtrl.create(DcNewsDetailPage, {
      news: this.newsDT.filter(item => item.newsId == news.newsId),
      config: this.config
    });

    modal.present();

    modal.onDidDismiss(() => {
      this.backButtonHandler();
    });
  }

  fetchNews() {
    this.newsProvider.getNews()
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.newsHD = res.data.newsActivityHDList;
          this.newsDT = res.data.newsActivityDTList;
        }
      });
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' }, {
          animate: true,
          animation: 'ease-in'
        });
      });
    }
  }
}

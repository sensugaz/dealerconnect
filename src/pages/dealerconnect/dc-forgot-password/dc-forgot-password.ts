import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../../providers/shared/auth';

@Component({
  selector: 'page-dc-forgot-password',
  templateUrl: 'dc-forgot-password.html',
})
export class DcForgotPasswordPage {

  form: FormGroup;
  btnLoading: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private authProvider: AuthProvider,
              private alertCtrl: AlertController) {
    
    this.form = this.formBuilder.group({
      username: ['', Validators.required]
    });

    this.btnLoading = false;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DcForgotPasswordPage');
  }

  onForgotPassword(form) {
    this.btnLoading = true;

    let formData = {
      userName: form.username.toLowerCase(),
      email: '',
      key: ''
    };

    this.authProvider.forgotPassword(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.alertCtrl.create({
            title: 'สำเร็จ',
            message: 'กรุณาตรวจสอบอีเมลของท่าน',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();
        } else {
          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'ชื่อผู้ใช้งานไม่ถูกต้อง',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();
        }
      
        this.form.reset();
        this.btnLoading  = false;
      });
  }

  gotoHome() {
    this.navCtrl.pop();
  }
}

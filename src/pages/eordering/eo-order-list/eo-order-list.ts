import { Component } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController, NavParams, Platform, App } from 'ionic-angular';
import * as moment from 'moment';
import { OrderProvider } from '../../../providers/eordering/order';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoOrderPdfPage } from '../eo-order-pdf/eo-order-pdf';
import { EoTabsPage } from '../eo-tabs/eo-tabs';

@Component({
  selector: 'page-eo-order-list',
  templateUrl: 'eo-order-list.html',
})
export class EoOrderListPage {

  search: any;
  status: any;
  userInfo: any;
  customerInfo: any;
  month: any;
  startDate: any;
  endDate: any;
  orders: any[];
  loading: any;
  alertIsOpen: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private orderProvider: OrderProvider,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private platform: Platform,
              private app: App) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.month = this.navParams.get('month');
    this.startDate = moment(this.month.date, 'YYYY-MM').startOf('month').format('YYYY-MM-DD');
    this.endDate = moment(this.month.date, 'YYYY-MM').endOf('month').format('YYYY-MM-DD');

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.fetchOrder();
  }

  openPdf(type, order) {
    let url = 'http://qasorder2.toagroup.com:8050/print/order/';
    let title = '';
    
    switch(type) {
      case 'I':
        title = 'ใบสั่งซื้อ';
        url = url + 'invoice?arr=' + [order.orderId];

        let modal = this.modalCtrl.create(EoOrderPdfPage, { url: url, title: title });

        modal.present();

        modal.onDidDismiss(() => {
          this.backButtonHandler();
        });
        break;
      case 'D':
        if (order.salesOrderNumber == undefined || order.salesOrderNumber == '') {
          this.alertIsOpen = true;

          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'ไม่มีเลขที่เอกสาร',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.alertIsOpen = false;
                }
              }
            ]
          }).present();
        } else {
          title = 'รายละเอียดการสั่งซื้อ';
          url = url + 'detail?arr=' + [order.orderId, order.salesOrderNumber];

          let modal = this.modalCtrl.create(EoOrderPdfPage, { url: url, title: title });

          modal.present();

          modal.onDidDismiss(() => {
            this.backButtonHandler();
          });
        }
        break;
      case 'S':
        if (order.salesOrderNumber == undefined || order.salesOrderNumber == '') {
          this.alertIsOpen = true;

          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'ไม่มีเลขที่เอกสาร',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.alertIsOpen = false;
                }
              }
            ]
          }).present();
        } else {
          title = 'สถานะการสั่งซื้อ';
          url = url + 'status?arr=' + [order.orderId, order.salesOrderNumber];

          let modal = this.modalCtrl.create(EoOrderPdfPage, { url: url, title: title });

          modal.present();

          modal.onDidDismiss(() => {
            this.backButtonHandler();
          });
        }
        break;
      case 'B':
        if (order.salesOrderNumber == undefined || order.salesOrderNumber == '') {
          this.alertIsOpen = true;

          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'ไม่มีเลขที่เอกสาร',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.alertIsOpen = false;
                }
              }
            ]
          }).present();
        } else {
          title = 'Order / Billing Tracking';
          url = url + 'billing?arr=' + [order.orderId, order.salesOrderNumber];

          let modal = this.modalCtrl.create(EoOrderPdfPage, { url: url, title: title });

          modal.present();

          modal.onDidDismiss(() => {
            this.backButtonHandler();
          });
        }
        break;
    }
  }

  showBilling(order) {
    if (order.orderHistoryHeader != undefined && order.orderHistoryHeader.length > 0) {
      order.showBilling = !order.showBilling;
    } else {
      this.alertIsOpen = true;
      
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'ไม่มีเลข billing',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.alertIsOpen = false;
            }
          }
        ]
      }).present();
    }
  }

  fetchOrder() {
    this.showLoading();

    this.orderProvider.getOrderPrecess(this.customerInfo.customerId, this.startDate, this.endDate)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.orders = res.data.orderProcessList;

          this.orders.forEach(value => {
            value.showBilling = false;

            if (value.salesOrderNumber != '') {
              this.orderProvider.getOrderHistory(value.salesOrderNumber)
                .then(res2 => {
                  if (res.result == 'SUCCESS') {
                    if (res2.data.orderHistoryHeaderList.length > 0) {
                      value.orderHistoryHeader = res2.data.orderHistoryHeaderList;
                    }
                  }
                });
            }
          });

          this.hideLoading();
        }
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alertIsOpen) {
          this.hideLoading();
        
          this.app.getRootNav().setRoot(EoTabsPage, { page: 1 }, {
            animate: true,
            animation: 'ease-in'
          });
        }
      });
    }
  }
}
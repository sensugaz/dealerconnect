import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, Platform, App } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerProvider } from '../../../providers/eordering/customer';
import { EoMenuPage } from '../eo-menu/eo-menu';

@Component({
  selector: 'page-eo-profile-update',
  templateUrl: 'eo-profile-update.html',
})
export class EoProfileUpdatePage {

  form: FormGroup;
  userInfo: any;
  customerInfo: any;
  btnLoading: boolean;
  alerIsOpen: boolean = true;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private storageProvider: StorageProvider,
              private customerProvider: CustomerProvider,
              private alertCtrl: AlertController,
              private platform: Platform,
              private app: App) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.form = this.formBuilder.group({
      address: this.customerInfo.address,
      street: this.customerInfo.street,
      subDistrictName: this.customerInfo.subDistrictName,
      districtName: this.customerInfo.districtName,
      cityName: this.customerInfo.cityName,
      postCode: this.customerInfo.postCode,
      telNo: this.customerInfo.telNo,
      eMail: [{ value: this.customerInfo.email, disabled: true }],
      mobileDC: [{ value: this.customerInfo.mobileDC, disabled: true }],
      customerEmailDC: [{ value: this.customerInfo.customerEmailDC, disabled: true }]
    });

    this.btnLoading = false;
    //this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  gotoHome() {
    this.navCtrl.popToRoot();
  }

  updateProfile(form) {
    this.btnLoading = true;

    let formData = {
      customerId: this.customerInfo.customerId,
      customerCode: this.customerInfo.customerCode,
      customerName: this.customerInfo.customerName,
      address: form.address,
      street: form.street,
      subDistinct: form.subDistrictName,
      distinct: form.districtName,
      city: form.cityName,
      postCode: form.postCode,
      phoneNumber: form.telNo,
      email: form.eMail,
      shipToText: this.customerInfo.transportZoneDesc,
      remark: '',
      userName: this.userInfo.userName,
    };

    this.customerProvider.updateCustomer({ customer: formData })
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.alerIsOpen = true;

          this.alertCtrl.create({
            title: 'สำเร็จ',
            message: 'แจ้งเปลี่ยนแปลงข้อมูลสำเร็จ',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.alerIsOpen = false;
                }
              }
            ]
          }).present();
        }

        this.btnLoading = false;
      });
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alerIsOpen) {
          this.app.getRootNav().setRoot(EoMenuPage, {}, {
            animate: true,
            animation: 'ease-in'
          });
        }
      });
    }
  }
}

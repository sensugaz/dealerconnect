import { Component } from '@angular/core';
import { NavController, NavParams, Platform, App } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoProfileUpdatePage } from './../eo-profile-update/eo-profile-update';
import { EoMenuPage } from '../eo-menu/eo-menu';

@Component({
  selector: 'page-eo-profile',
  templateUrl: 'eo-profile.html',
})
export class EoProfilePage {

  customerInfo: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private platform: Platform,
              private app: App) {

    this.customerInfo = this.storageProvider.getCustomerInfo();
    //this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  gotoHome() {
    this.navCtrl.popToRoot();
  }

  gotoProfileUpdate() {
    this.navCtrl.push(EoProfileUpdatePage);
  }
  
  backButtonHandler() {
    if (this.platform.is('android')) {
      /*
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
        //this.navCtrl.remove(this.navCtrl.getPrevious().index);
      });
      */
    }
  }
}

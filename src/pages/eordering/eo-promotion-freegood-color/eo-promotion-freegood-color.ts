import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-eo-promotion-freegood-color',
  templateUrl: 'eo-promotion-freegood-color.html',
})
export class EoPromotionFreegoodColorPage {

  colors: any[];
  size: any;
  btfCode: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private viewCtrl: ViewController) {

    this.colors = this.navParams.get('colors');
    this.size = this.navParams.get('size');
    this.btfCode = this.navParams.get('btfCode');

    this.colors = this.colors.filter(item => {
      if (item.sizeCode == this.size.sizeCode && item.btfCode == this.btfCode) {
        return item;
      }
    });
  }

  ionViewDidLoad() {
    
  }

  selectColor(color) {
    this.viewCtrl.dismiss({ color: color });
  }

  close() {
    this.viewCtrl.dismiss();
  }
}

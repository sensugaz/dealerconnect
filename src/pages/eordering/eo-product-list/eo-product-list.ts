import { EoTabsPage } from './../eo-tabs/eo-tabs';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ToastController, Platform, ViewController, LoadingController } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { ProductProvider } from '../../../providers/eordering/product';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EoProductDetailPage } from '../eo-product-detail/eo-product-detail';
import { FavoriteProvider } from '../../../providers/eordering/favorite';
import { CustomerProvider } from '../../../providers/eordering/customer';

@Component({
  selector: 'page-eo-product-list',
  templateUrl: 'eo-product-list.html',
})
export class EoProductListPage {
  search: any;
  userInfo: any;
  customerInfo: any;
  products: any;
  config: any;
  loading: any;

  marketingCodes: any[];
  brandCodes: any[];
  typeCodes: any[];
  slice: number;
  alertIsOpen: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private productProvider: ProductProvider,
              private configProvider: ConfigProvider,
              private favoriteProvider: FavoriteProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private platform: Platform,
              private viewCtrl: ViewController,
              private loadingCtrl: LoadingController,
              private customerProvider: CustomerProvider) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.config = this.storageProvider.getConfig();
    this.products = [];
    this.marketingCodes = this.navParams.get('marketingCodes');
    this.brandCodes = this.navParams.get('brandCodes');
    this.typeCodes = this.navParams.get('typeCodes');
    this.slice = 10;

    this.backButtonHandler();
  }

  ionViewDidEnter() {
    // this.fetchProduct();
  }

  ionViewDidLoad() {
    //this.loading = true;
    this.fetchProduct();
  }

  fetchProduct() {
    this.showLoading();

    let paramsData = {
      customerId: this.customerInfo.customerId,
      marketingCodeList: this.marketingCodes,
      brandCodeList: this.brandCodes,
      typeCodeList: this.typeCodes,
      isBTFView: true
    };

    this.productProvider.getProduct(paramsData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.products = res.data.productList;
        }

        this.hideLoading();
      });
  }

  onSelectProduct(product) {
    this.customerProvider.checkCustomerBlock('00' + this.customerInfo.customerCode)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (res.data.block != 'X') {
            this.navCtrl.push(EoProductDetailPage, { product: product, mode: 'DIS' });
          } else {
            this.alertIsOpen = true;

            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'ท่านไม่สามารถสั่งซื้อสินค้า online <br> บนระบบ TOA e-Ordering ได้ <br> เนื่องจาก ติดปัญหาการชำระบัญชี <br><br> กรุณาติดต่อ <br> ผู้แทนขาย หรือ ฝ่ายสินเชื่อและลูกหนี้ <br> บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จำกัด (มหาชน)',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'ตกลง',
                  handler: () => {
                    this.alertIsOpen = false;
                  }
                }
              ]
            }).present();
          }
        } else {
          this.alertIsOpen = true;

          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'เกิดข้อผิดพลาด',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.alertIsOpen = false;
                }
              }
            ]
          }).present();
        }
      });
  }

  onSetFavorite($event, product) {
    $event.stopPropagation();

    let formData = {
      customerId: this.customerInfo.customerId,
      userName: this.userInfo.userName,
      btfCode: product.btf
    };

    if (product.isFavorite) {
      this.favoriteProvider.removeFavorite(formData)
        .then(res => {
          if (res.result == 'SUCCESS') {
            product.isFavorite = false;

            this.toastCtrl.create({
              message: 'ลบรายการโปรดเรียบร้อย',
              duration: 1500
            }).present();
          }
        });
    } else {
      this.favoriteProvider.addFavorite(formData)
        .then(res => {
          if (res.result == 'SUCCESS') {
            product.isFavorite = true;

            this.toastCtrl.create({
              message: 'เพิ่มรายการโปรดเรียบร้อย',
              duration: 1500
            }).present();
          }
        });
    }
  }

  doInfinite() {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.slice += 10;

        resolve();
      }, 500);
    })
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  
  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alertIsOpen) {
          this.navCtrl.pop();
        }
      });
    }
  }
}

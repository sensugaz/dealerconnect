import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform, App } from 'ionic-angular';

@Component({
  selector: 'page-eo-product-detail-color',
  templateUrl: 'eo-product-detail-color.html',
})
export class EoProductDetailColorPage {

  colors: any[];
  colorFlag: any;
  size: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private platform: Platform,
              private app: App) {

    this.colors = this.navParams.get('colors');
    this.colorFlag = this.navParams.get('colorFlag');
    this.size = this.navParams.get('size');

    this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  selectColor(color) {
    this.viewCtrl.dismiss({ color: color });
  }

  close() {
    this.viewCtrl.dismiss();
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.viewCtrl.dismiss();
      });
    }
  }
}

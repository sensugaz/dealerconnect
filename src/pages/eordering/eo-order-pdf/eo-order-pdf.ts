import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-eo-order-pdf',
  templateUrl: 'eo-order-pdf.html',
})
export class EoOrderPdfPage {

  pdfLink: any;
  url: any;
  title: any;
  loading: any;
  zoom: number;
  page: number;
  totalPage: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private sanitizer: DomSanitizer,
              private viewCtrl: ViewController,
              private loadingCtrl: LoadingController,
              private platform: Platform) {

    this.url = this.navParams.get('url');
    this.title = this.navParams.get('title');
    this.url = this.url;
    this.zoom = 1;

    this.page = 1;
    this.totalPage = 0;

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.showLoading();
  }

  zoomInc() {
    this.zoom += 0.5;
  }

  zoomDec() {
    if ((this.zoom - 0.5) >= 1) {
      this.zoom -= 0.5;
    }
  }

  onComplete($event) {
    this.totalPage = $event.pdfInfo.numPages;

    this.hideLoading();
  }

  nextPage() {
    if (this.page < this.totalPage) {
      this.page++;
    }
  }

  prevPage() {
    if (this.page > 1) {
      this.page--;
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  
  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.hideLoading();
        
        this.viewCtrl.dismiss();
      });
    }
  }
}
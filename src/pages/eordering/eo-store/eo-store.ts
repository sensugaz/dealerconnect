import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, LoadingController, App, Platform, Events } from 'ionic-angular';
import { CustomerProvider } from '../../../providers/eordering/customer';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoMenuPage } from '../eo-menu/eo-menu';
import { EoTabsPage } from '../eo-tabs/eo-tabs';
import { DcHomePage } from '../../dealerconnect/dc-home/dc-home';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'page-eo-store',
  templateUrl: 'eo-store.html',
})
export class EoStorePage {

  config: any;
  userInfo: any;
  customers: any[];
  loading: any;
  slice: number;
  search: string;
  action: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storageProvider: StorageProvider,
              private customerProvider: CustomerProvider,
              public loadingCtrl: LoadingController,
              private popoverCtrl: PopoverController,
              private platform: Platform,
              private app: App,
              private events: Events) {

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.loading = false;
    this.slice = 10;
    this.action = this.navParams.get('action');
    // back button handler
    
    this.backButtonHandler();
  }

  ionViewDidLoad() { 
    this.fetchCustomer();
  }

  selectedCustomer(customer) {
    this.customerProvider.getCustomerInfo(customer.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          localStorage.setItem('customerInfo', JSON.stringify(res.data.customerInfo));
          
          this.events.publish('customer:load', customer);
          this.navCtrl.setRoot(EoTabsPage);
        }
      });
  }

  doInfinite() {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.slice += 10;

        resolve();
      }, 50);
    })
  }

  gotoDC() {
    this.navCtrl.setRoot(DealerConnect, { action: 'NP' });
  }

  fetchCustomer() {
    this.showLoading();

    this.customerProvider.getCustomer(this.userInfo.userId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.customers = res.data.customerList;
        }

        this.hideLoading();
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    this.platform.registerBackButtonAction(() => {
      this.hideLoading();
      this.navCtrl.setRoot(DealerConnect, { action: 'NP' });
    });
  }
}

import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { ReportProvider } from '../../../providers/eordering/report';

@Component({
  selector: 'page-eo-report-quarter',
  templateUrl: 'eo-report-quarter.html',
})
export class EoReportQuarterPage {

  userInfo: any;
  files: any[];
  loading: any;
  report1: any[];
  report2: any[];

  salesGroup: any;
  slice: number;
  period: any;
  fileName: any;
  alertIsOpen: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private reportProvider: ReportProvider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private storageProvider: StorageProvider,
              private platform: Platform) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.slice = 3;

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.fetchFile();
  }

  selectFile(fileName) {
    this.showLoading();

    this.report1 = [];

    this.reportProvider.getReportQuarter(fileName, this.userInfo.userName, this.userInfo.deptId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.report1 = res.data;
          this.salesGroup = 'ALL';

          switch (this.userInfo.deptId) {
            case 2:
              this.salesGroup = this.userInfo.userName.replace('sa_', '');
              break;
            case 3:
              //this.areaNo = this.userInfo.userName.replace('sa_', '');
              break;
          }

          this.period = this.report1[0].period;
        }

        this.hideLoading();
      });
  }

  showReport() {
    this.showLoading();

    let temp;

    if (this.salesGroup != 'ALL') {
      temp = this.report1.filter(item => item.sales_group == this.salesGroup);
    } else {
      temp = this.report1;
    }

    if (temp.length == 0) {
      this.alertIsOpen = true;

      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.alertIsOpen = false;
            }
          }
        ]
      }).present();
    } else {
      let data = [];

      temp.forEach(value => {
        let _ = data.filter(item => {
          switch (this.userInfo.deptId) {
            case 1:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
              if (item.area_no == value.area_no && item.sales_group == value.sales_group) {
                return value;
              }
              break;
            case 2:
              if (item.area_no == value.area_no) {
                return value;
              }
              break;
          }
        });

        if (_.length <= 0) {
          data.push({
            period: value.period,
            commission_type: value.commission_type,
            sales_group: value.sales_group,
            employee_code: value.employee_code,
            employee_name: value.employee_name,
            area_no: value.area_no,
            supervisor_code: value.supervisor_code,
            supervisor_name: value.supervisor_name,
            orders: [{
              product: value.product,
              net_amount: value.net_amount,
              target_amount: value.target_amount,
              percent: value.percent,
              reward: value.reward,
              actual_shop: value.actual_shop,
              target_shop: value.target_shop
            }],
            total: {
              net_amount: 0,
              target_amount: 0,
              percent: 0,
              reward: 0,
              actual_shop: 0,
              target_shop: 0
            }
          });
        } else {
          let index = data.findIndex(i => {
            if (i.area_no == value.area_no && i.sales_group == value.sales_group)
              return i;
          });

          data[index].orders.push({
            product: value.product,
            net_amount: value.net_amount,
            target_amount: value.target_amount,
            percent: value.percent,
            reward: value.reward,
            actual_shop: value.actual_shop,
            target_shop: value.target_shop
          });
        }
      });

      data.forEach(value => {
        value.orders.forEach(o => {
          value.total.net_amount += parseInt(o.net_amount);
          value.total.target_amount += parseInt(o.target_amount);
          value.total.percent += parseInt(o.percent);
          value.total.reward += parseInt(o.reward);
          value.total.actual_shop += parseInt(o.actual_shop);
          value.total.target_shop += parseInt(o.target_shop);
        });
      });

      this.report2 = data;
    }

    this.hideLoading();
  }

  doInfinite() {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.slice += 3;

        resolve();
      }, 500);
    })
  }

  backPage() {
    this.navCtrl.pop();
  }

  fetchFile() {
    this.showLoading();

    this.reportProvider.getReportQuarterFile()
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.files = res.data;
        }

        this.hideLoading();
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alertIsOpen) {
          this.hideLoading();
          this.navCtrl.pop();
        }
      });
    }
  }
}

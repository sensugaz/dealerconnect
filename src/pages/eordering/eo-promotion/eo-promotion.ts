import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { PromotionProvider } from '../../../providers/eordering/promotion';
import { EoPromotionColorPage } from '../eo-promotion-color/eo-promotion-color';
import { ProductProvider } from '../../../providers/eordering/product';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoPromotionBtfPage } from '../eo-promotion-btf/eo-promotion-btf';
import { EoPromotionFreegoodPage } from '../eo-promotion-freegood/eo-promotion-freegood';
import { CartProvider } from '../../../providers/eordering/cart';
import { EoPromotionSizePage } from '../eo-promotion-size/eo-promotion-size';

@Component({
  selector: 'page-eo-promotion',
  templateUrl: 'eo-promotion.html',
})
export class EoPromotionPage {

  promotionId: any;
  promotionName: any;
  loading: any;
  promotionHD: any;
  promotionDT: any[];
  promotionDT_Sel: any[];
  productInDt: any[];
  btfInDt: any[];
  freeGoods: any[];
  productInFreeGoods: any[];
  promotionFG: any[];
  promotionFG_Sel: any[];

  promotionSetValue: number;
  sizes: any[];
  colors: any[];
  sizeFreeGoods: any[];
  colorFreeGoods: any[];

  isSizes: any[];
  isColors: any[];
  isSizeFreeGoods: any[];
  isColorFreeGoods: any[];

  userInfo: any;
  customerInfo: any;
  stepCount: number;
  countOrder: number;
  hideAddPromotionSet: boolean;
  
  temps: {
    promotionHD: any,
    promotionDT: any[],
    promotionDT_Sel: any[];
  };

  btnLoading: boolean;

  countPromotionSet: any;

  constructor(private navCtrl: NavController, 
              private loadingCtrl: LoadingController,
              private promotionProvider: PromotionProvider,
              private modalCtrl: ModalController,
              private productProvider: ProductProvider,
              private storageProvider: StorageProvider,
              private alertCtrl: AlertController,
              private cartProvider: CartProvider,
              private navParams: NavParams) {
    
    let promotion = this.navParams.get('promotion');

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.promotionId = promotion.promotionId;
    this.promotionName = promotion.promotionName;
    this.promotionHD = {};
    this.promotionDT = [];
    this.promotionDT_Sel = [];
    this.productInDt = [];
    this.promotionFG = [];
    this.promotionFG_Sel = [];
    this.btfInDt = [];
    this.freeGoods = [];
    this.productInFreeGoods = [];
    this.sizes = [];
    this.colors = [];
    this.sizeFreeGoods = [];
    this.colorFreeGoods = [];

    this.isSizes = [];
    this.isColors = [];
    this.isSizeFreeGoods = [];
    this.isColorFreeGoods = [];

    // default values
    this.promotionSetValue = 1;
    this.countPromotionSet = 0;
    this.hideAddPromotionSet = false;

    // this.temps.promotionDT = null;
    // this.temps.promotionDT = [];
    // this.temps.promotionDT_Sel = [];
    this.temps = {
      promotionDT: null,
      promotionHD: [],
      promotionDT_Sel: []
    };

    this.btnLoading = false;
    
    this.fetchPromotion();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EoPromotionPage');
  }

  fetchPromotion() {
    this.showLoading();

    this.promotionProvider.getPromotionInfo(this.promotionId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.promotionHD = res.data.promotionHDList[0];
          this.promotionDT = res.data.promotionDTList;
          this.productInDt = res.data.productInDtList;
          this.btfInDt = res.data.btfInDtList;
          this.freeGoods = res.data.freeGoodsList;
          this.productInFreeGoods = res.data.productInFreeGoodsList;
          
          this.temps.promotionHD = this.promotionHD;
          
          this.setPromotionSetValue();  

          this.promotionDT.forEach(value => {
            value.editBtf = false;
            value.editSize = false;
            value.editColor = false;
            value.qty = 0;
          });
      
          this.setPromotion(this.promotionDT);
        }

        this.hideLoading();
      });
  }

  incProductQty(promotionDT) {
    let product = promotionDT.productData;
    let qty: number = 1;

    switch (product.altUnit1Code) {
      case 'BOX':
        if (product.isBox) {
          qty = product.altUnit1Amount;
        } else {
          qty = 1;
        }
        break;
      case 'DZ':
        qty = product.altUnit1Amount;
        break;
      default:
        qty = 1;
        break;
    }

    if (this.promotionHD.isPromotionSet) {
      //const limit = 
    } else {
      promotionDT.qty = parseInt(promotionDT.qty) + qty;
    }
  }

  decProductQty(promotionDT) {
    let product = promotionDT.productData;
    let qty: number = 1;
    
    switch (product.altUnit1Code) {
      case 'BOX':
        if (product.isBox) {
          qty = product.altUnit1Amount;
        } else {
          qty = 1;
        }
        break;
      case 'DZ':
        qty = product.altUnit1Amount;
        break;
      default:
        qty = 1;
        break;
    }

    if (promotionDT.qty > qty) {
      promotionDT.qty -= qty;
    }
  }

  edtProductQty(promotionDT) {
    let product = promotionDT.productData;

    if (this.promotionHD.isPromotionSet) {
      
    }

    if (promotionDT.qty == 0 || promotionDT.qty == undefined) {
      switch (product.altUnit1Code) {
        case 'BOX':
          if (product.isBox) {
            promotionDT.qty = product.altUnit1Amount;
          } else {
            promotionDT.qty = 1;
          }
          break;
        case 'DZ':
          promotionDT.qty = product.altUnit1Amount;
          break;
        default:
          promotionDT.qty = 1;
          break;
      }
    } else if (product.isBox || product.altUnit1Code == 'DZ') {
      let textAltUnit1Amount = product.altUnit1Amount;
      let textAltUnit1NameTh = product.unitNameTh;

      switch (product.altUnit1Code) {
        case 'DZ':
          textAltUnit1Amount = 1;
          textAltUnit1NameTh = product.altUnit1NameTh;
          break;
      }

      if (promotionDT.qty < product.altUnit1Amount) {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `กรุณาสั่งซื้ออย่างน้อย ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                //this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        promotionDT.qty = product.altUnit1Amount;   
      } else if (promotionDT.qty % product.altUnit1Amount) {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `ผลิตภัณฑ์ต้องสั่งซื้อทีละ ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                //this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        promotionDT.qty = product.altUnit1Amount * Math.floor(promotionDT.qty / product.altUnit1Amount);
      }
    }
  }

  addProduct(promotionDT) {
    let isProductExists = this.promotionDT_Sel.filter(item => {
      if (item.productData.productCode == promotionDT.productData.productCode) {
        return item;
      }
    })[0] != undefined;

    let productData = promotionDT.productData;
    let productForm = Object.assign({}, promotionDT);

    if (promotionDT.format == 'MG' || promotionDT.format == 'B') {  
      productForm.totalAmount = parseInt(productData.productPrice) * parseInt(productForm.qty);   
    } else {
      productForm.totalAmount = parseInt(productData.productPrice) * parseInt(productForm.qty);  
    }

    this.promotionFG = [];
    this.promotionFG_Sel = [];

    if (this.promotionHD.isPromotionSet) {

    } else {
      if (!isProductExists) {
        productForm.buyQty = productForm.qty;

        this.promotionDT_Sel.push(productForm);
      } else {
        const index = this.promotionDT_Sel.findIndex(item => {
          if (item.productData.productCode == promotionDT.productData.productCode) {
            return item;
          }
        });

        productForm.qty = parseInt(productForm.qty) + parseInt(this.promotionDT_Sel[index].qty);
        productForm.buyQty = productForm.qty;

        this.promotionDT_Sel[index] = productForm;
      }
    }
  }

  delProduct(promotionDT) {
    let index = this.promotionDT_Sel.indexOf(promotionDT);

    this.promotionDT_Sel.splice(index, 1);
    this.promotionFG = [];
    this.promotionFG_Sel = [];

    if (this.promotionHD.isPromotionSet) {
      if (!this.promotionDT_Sel.length) {
        this.countPromotionSet = 0;
      }
    }
  }

  calculate() {

  }

  /** Get & Set */
  setPromotion(promotionDT) {
    let productInDt = null;

    promotionDT.forEach(value => {
      let qty = 0;

      value.stepCount = this.stepCount;

      switch (value.format) {
        case 'MG':
        case 'B':
          break;
        case 'BTF':
          productInDt = this.getProductInDt(value.promotionDtId);

          this.getSize(value.format, value, null);
          this.getColor(value.format, value, null);

          value.editSize = true;
          value.editColor = true;
          
          value.productName = productInDt.btfWebDescTh;
          value.productData = productInDt;

          if (value.minQty <= 300) {
            if (this.promotionHD.checkStepBySKU == false) {
              qty = value.minQty;
            } else {
              qty = value.minQty;
            }
          }

          value.qty = qty;

          if (this.promotionHD.isPromotionSet) {
            value.showQty = qty;
          }
          break;
        case 'BTFS':
          productInDt = this.getProductInDt(value.promotionDtId);

          this.getSize(value.format, value, null);
          this.getColor(value.format, value, null);

          value.editColor = true;
          
          value.productName = productInDt.btfWebDescTh;
          value.productData = productInDt;

          if (value.minQty <= 300) {
            if (this.promotionHD.checkStepBySKU == false) {
              qty = value.minQty;
            } else {
              qty = value.minQty;
            }
          }

          value.qty = qty;

          if (this.promotionHD.isPromotionSet) {
            value.showQty = qty;
          }
          break;
        case 'SKU':
          productInDt = this.getProductInDt(value.promotionDtId);

          this.getSize(value.format, value, null);
          this.getColor(value.format, value, null);
          
          value.productName = productInDt.btfWebDescTh;
          value.productData = productInDt;

          if (value.minQty <= 300) {
            if (this.promotionHD.checkStepBySKU == false) {
              qty = value.minQty;
            } else {
              qty = value.minQty;
            }
          }

          value.qty = qty;

          if (this.promotionHD.isPromotionSet) {
            value.showQty = qty;
          }
          break;
      }
    });
  }

  setPromotionSetValue() {
    this.cartProvider.getCart(this.userInfo.userName, this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          let isProductExists = res.data.cartList.filter(item => {
            if (item.promotionId == this.promotionHD.promotionId) {
              return item;
            }
          });

          if (this.promotionHD.isPromotionSet && this.promotionHD.promotionSetValue == 1) {
            if (isProductExists.length > 0) {
              this.promotionSetValue = 0;
              this.hideAddPromotionSet = true;
            } else {
              this.hideAddPromotionSet = false;
            }
          }
        }
      })
  }

  setBtf(promotionDT, btfInDt) {
    this.showLoading();

    this.getProductInBtf(btfInDt.btfWeb, null)
      .then(res => {
        if (res != undefined) {
          promotionDT.productData = res;
        }
      });
    
    promotionDT.btfInDt = btfInDt;
    promotionDT.productName = btfInDt.btfDesc;

    this.getSize(promotionDT.format, promotionDT, btfInDt.btfWeb);
    this.getColor(promotionDT.format, promotionDT, btfInDt.btfWeb);
        
    promotionDT.qty = promotionDT.minQty;

    this.hideLoading();
  }

  setSize(promotionDT, size) {
    if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
      this.getProductInBtf(size.btfCode, size.productCode)
        .then(res => {
          promotionDT.productData = res;
        });

      promotionDT.qty = promotionDT.minQty;
    } else {
      let productInDt = this.productInDt.filter(item => {
        if (item.productCode == size.productCode) {
          return item;
        }
      })[0];

      promotionDT.productData = productInDt;
    }
  }

  setColor(promotionDT, color) {
    if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
      this.getProductInBtf(color.btfCode, color.productCode)
        .then(res => {
          promotionDT.productData = res;
        });

      promotionDT.qty = promotionDT.minQty;
    } else {
      let productInDt = this.productInDt.filter(item => {
        if (item.productCode == color.productCode) {
          return item;
        }
      })[0];

      promotionDT.productData = productInDt
    }
  }

  getSize(format, promotionDT, btf) {
    if (format == 'MG' || format == 'B') {
      this.productProvider.getProductInBTF(this.customerInfo.customerId, btf)
        .then(res => {
          if (res.result == 'SUCCESS') {
            res.data.productList.forEach(value => {
              let isSizeExists = this.sizes.filter(item => {
                if (item.btfCode == btf && item.productCode == value.productCode) {
                  return item;
                }
              })[0] != undefined;

              if (!isSizeExists) {
                this.sizes.push({
                  sizeCode: value.sizeCode,
                  sizeName: value.sizeName,
                  btfCode: btf,
                  promotionDtId: promotionDT.promotionDtId,
                  productId: value.productId,
                  productCode: value.productCode
                });
              }
            });
          }
        })
        .then(() => {
          let isSizeExists = this.isSizes.filter(item => {
            if (item.btfCode == btf) {
              return item;
            }
          })[0] != undefined;

          if (!isSizeExists) {
            let size = this.sizes.filter(item => {
              if (item.btfCode == btf) {
                return item;
              }
            })[0];
      
            this.isSizes.push(size);
          }
        });
    } else {
      this.productInDt.forEach(value => {
        if (promotionDT.promotionDtId == value.promotionDtId) {
          this.sizes.push({
            sizeCode: value.sizeCode,
            sizeName: value.sizeName,
            btfCode: value.btf,
            promotionId: promotionDT.promotionId,
            productId: value.productId,
            productCode: value.productCode,
            promotionDtId: value.promotionDtId
          });
        }
      });

      let size = this.sizes.filter(item => {
        if (item.promotionDtId == promotionDT.promotionDtId) {
          return item;
        }
      })[0];

      this.isSizes.push(size);
    }
  }

  getColor(format, promotionDT, btf) {
    if (format == 'MG' || format == 'B') {
      this.productProvider.getProductInBTF(this.customerInfo.customerId, btf)
        .then(res => {
          if (res.result == 'SUCCESS') {
            res.data.productList.forEach(value => {
              let isColorExists = this.colors.filter(item => {
                if (item.btfCode == btf && item.productCode == value.productCode) {
                  return item;
                }
              })[0] != undefined;

              if (!isColorExists) {
                this.colors.push({
                  colorCode: value.colorCode,
                  rgbCode: value.rgbCode,
                  btfCode: btf,
                  promotionDtId: promotionDT.promotionDtId,
                  productId: value.productId,
                  productCode: value.productCode,
                  sizeCode: value.sizeCode
                });
              } 
            });
          }
        })
        .then(() => {
          let isColorExists = this.isColors.filter(item => {
            if (item.btfCode == btf) {
              return item;
            }
          })[0] != undefined;

          if (!isColorExists) {
            let color = this.colors.filter(item => {
              if (item.btfCode == btf) {
                return item;
              }
            })[0];
            
            this.isColors.push(color);
          }
        });

    } else {
      this.productInDt.forEach(value => {
        if (promotionDT.promotionDtId == value.promotionDtId) {
          this.colors.push({
            colorCode: value.colorCode,
            rgbCode: value.rgbCode,
            btfCode: value.btf,
            productCode: value.productCode,
            sizeCode: value.sizeCode,
            promotionDtId: value.promotionDtId
          });
        }
      });

      let color = this.colors.filter(item => {
        if (item.promotionDtId == promotionDT.promotionDtId) {
          return item;
        }
      })[0];
      
      this.isColors.push(color);
    }
  }

  getProductInBtf(btf, productCode) {
    return this.productProvider.getProduct(this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (productCode == null) {
            return res.data.productList[0];
          } else {
            return res.data.productList.filter(item => {
              if (item.productCode == productCode) {
                return item;
              }
            })[0];
          }
        }
      })
  }

  getProductInDt(promotionDtId) {
    return this.productInDt.filter(item => {
      return item.promotionDtId == promotionDtId;
    })[0];
  }

  getProductInDtByProductId(productId) {
    return this.productInDt.filter(item => {
      if (item.productId == productId) {
        return item;
      }
    })[0];
  }

  getIsColor(promotionDT) {
    let color = {};
    let btf;

    if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
      btf = promotionDT.btfInDt.btfWeb;
    } else {
      btf = promotionDT.btfCode;
    }

    color = this.isColors.filter(item => {
      if (item.btfCode == btf) {
        return item;
      }  
    });

    return color[0];
  }

  getIsSize(promotionDT) {
    let size = {};
    let btf;

    if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
      btf = promotionDT.btfInDt.btfWeb;
    } else {
      btf = promotionDT.btfCode;
    }

    size = this.isSizes.filter(item => {
      if (item.btfCode == btf) {
        return item;
      }  
    });

    return size[0];
  }

  /** Check Button */
  chkBtnAddProduct(promotionDT) {
    if (this.promotionHD.isPromotionSet) {
      let limit = promotionDT.showQty * this.promotionSetValue;
      let total = 0;

      this.temps.promotionDT_Sel.forEach(value => {
        if (value.promotionDtId == promotionDT.promotionDtId) {
          total += value.qty;
        }
      });

      if (promotionDT.qty == 0 || (total >= limit)) {
        return true;
      }
    } else if (this.promotionHD.isPromotionSet && this.promotionSetValue == 0) {
      return true;
    } else if (promotionDT.qty == 0) {
      return true;
    }

    return false;
  }

  /** Other */
  altPromotionCountOrder() {
    this.promotionProvider.getPromotionOrderCount(this.promotionHD.promotionId, this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.countOrder = res.data.dealerreceivehd[0].countOrder;

          if (this.countOrder) {
            this.alertCtrl.create({
              title: 'แจ้งเตือน',
              message: 'คุณได้ใช้สิทธิโปรโมชั่นเรียบร้อยแล้ว',
              mode: 'ios',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'ตกลง',
                  handler: () => {
                    //this.alertIsOpen = false;
                  }
                }
              ]
            }).present();
          }
        }
      });
  }

  openModalBtf(promotionDT) {
    let modal = this.modalCtrl.create(EoPromotionBtfPage, { btfInDt: this.btfInDt, promotionDtId: promotionDT.btfInDt.promotionDtId });

    modal.present();

    modal.onDidDismiss(data => {
      if (data != undefined && data.btfInDt != undefined) {
        this.setBtf(promotionDT, data.btfInDt);
      }
    });
  }

  openModalSize(promotionDT) {
    let modal = this.modalCtrl.create(EoPromotionSizePage, { 
      sizes: this.sizes,
      btfCode: (promotionDT.format == 'MG' || promotionDT.format == 'B') ? promotionDT.btfInDt.btf : promotionDT.btfCode
    });

    modal.present();

    modal.onDidDismiss(data => {
      if (data != undefined && data.size != undefined) {
        let btf;

        if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
          btf = promotionDT.btfInDt.btf;
        } else {
          btf = promotionDT.btfCode;
        }

        let index = this.isSizes.findIndex(item => {
          if (item.btfCode == btf && item.promotionDtId == promotionDT.promotionDtId) {
            return item;
          }  
        });

        this.isSizes[index] = data.size;
        this.setSize(promotionDT, data.size);
      } 
    });
  }

  openModalColor(promotionDT) {
    let modal = this.modalCtrl.create(EoPromotionColorPage, { 
      colors: this.colors, 
      size: this.getIsSize(promotionDT),
      btfCode: (promotionDT.format == 'MG' || promotionDT.format == 'B') ? promotionDT.btfInDt.btf : promotionDT.btfCode
    });

    modal.present();

    modal.onDidDismiss(data => {
      if (data != undefined && data.color != undefined) {
        let btf;

        if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
          btf = promotionDT.btfInDt.btf;
        } else {
          btf = promotionDT.btfCode;
        }
    
        let index = this.isColors.findIndex(item => {
          if (item.btfCode == btf && item.promotionDtId == promotionDT.promotionDtId) {
            return item;
          }  
        });

        this.isColors[index] = data.color;
        this.setColor(promotionDT, data.color);
      }
    });
  }

  // getSize(format, promotionDT, btf) {
  //   if (format == 'MG' || format == 'B') {
  //     this.productProvider.getProductInBTF(this.customerInfo.customerId, btf)
  //       .then(res => {
  //         if (res.result == 'SUCCESS') {
  //           res.data.productList.forEach(value => {
  //             let isExists = this.sizes.filter(item => {
  //               if (item.btfCode == btf && item.productCode == value.productCode) {
  //                 return item;
  //               }
  //             })[0] != undefined;

  //             if (!isExists) {
  //               this.sizes.push({
  //                 sizeCode: value.sizeCode,
  //                 sizeName: value.sizeName,
  //                 btfCode: btf,
  //                 promotionDtId: promotionDT.promotionDtId,
  //                 productId: value.productId,
  //                 productCode: value.productCode
  //               });
  //             }
  //           });
  //         }
  //       })
  //       .then(() => {
  //         let isExists = this.isSizes.filter(item => {
  //           if (item.btfCode == btf) {
  //             return item;
  //           }
  //         })[0] != undefined;

  //         if (!isExists) {
  //           let size = this.sizes.filter(item => {
  //             if (item.btfCode == btf) {
  //               return item;
  //             }
  //           })[0];
      
  //           this.isSizes.push(size);
  //         }
  //       });
  //   } else {
  //     this.productInDt.forEach(value => {
  //       if (promotionDT.promotionDtId == value.promotionDtId) {
  //         this.sizes.push({
  //           sizeCode: value.sizeCode,
  //           sizeName: value.sizeName,
  //           btfCode: value.btf,
  //           promotionId: promotionDT.promotionId,
  //           productId: value.productId,
  //           productCode: value.productCode,
  //           promotionDtId: value.promotionDtId
  //         });
  //       }
  //     });

  //     let size = this.sizes.filter(item => {
  //       if (item.promotionDtId == promotionDT.promotionDtId) {
  //         return item;
  //       }
  //     })[0];

  //     this.isSizes.push(size);
  //   }
  // }

  // getSizeFreeGoods(format, promotionFG) {
  //   this.productInFreeGoods.forEach(value => {
  //     if (promotionFG.freeGoodId == value.freeGoodId) {
  //       this.sizeFreeGoods.push({
  //         sizeCode: value.sizeCode,
  //         sizeName: value.sizeName,
  //         btfCode: value.btf,
  //         freeGoodsId: promotionFG.freeGoodsId,
  //         productId: value.productId,
  //         productCode: value.productCode
  //       });
  //     }
  //   });

  //   let size = this.sizeFreeGoods.filter(item => {
  //     if (item.freeGoodsId == promotionFG.freeGoodsId) {
  //       return item;
  //     }
  //   })[0];

  //   this.isSizeFreeGoods.push(size);
  // }

  // getColor(format, promotionDT, btf) {
  //   if (format == 'MG' || format == 'B') {
  //     this.productProvider.getProductInBTF(this.customerInfo.customerId, btf)
  //       .then(res => {
  //         if (res.result == 'SUCCESS') {
  //           res.data.productList.forEach(value => {
  //             let isExists = this.colors.filter(item => {
  //               if (item.btfCode == btf && item.productCode == value.productCode) {
  //                 return item;
  //               }
  //             })[0] != undefined;

  //             if (!isExists) {
  //               this.colors.push({
  //                 colorCode: value.colorCode,
  //                 rgbCode: value.rgbCode,
  //                 btfCode: btf,
  //                 promotionDtId: promotionDT.promotionDtId,
  //                 productId: value.productId,
  //                 productCode: value.productCode,
  //                 sizeCode: value.sizeCode
  //               });
  //             } 
  //           });
  //         }
  //       })
  //       .then(() => {
  //         let isExists = this.isColors.filter(item => {
  //           if (item.btfCode == btf) {
  //             return item;
  //           }
  //         })[0] != undefined;

  //         if (!isExists) {
  //           let color = this.colors.filter(item => {
  //             if (item.btfCode == btf) {
  //               return item;
  //             }
  //           })[0];
            
  //           this.isColors.push(color);
  //         }
  //       });

  //   } else {
  //     this.productInDt.forEach(value => {
  //       if (promotionDT.promotionDtId == value.promotionDtId) {
  //         this.colors.push({
  //           colorCode: value.colorCode,
  //           rgbCode: value.rgbCode,
  //           btfCode: value.btf,
  //           productCode: value.productCode,
  //           sizeCode: value.sizeCode,
  //           promotionDtId: value.promotionDtId
  //         });
  //       }
  //     });

  //     let color = this.colors.filter(item => {
  //       if (item.promotionDtId == promotionDT.promotionDtId) {
  //         return item;
  //       }
  //     })[0];
      
  //     this.isColors.push(color);
  //   }
  // }

  // getColorFreeGoods(format, promotionFG) {
  //   this.productInFreeGoods.forEach(value => {
  //     if (promotionFG.freeGoodId == value.freeGoodId) {
  //       this.colorFreeGoods.push({
  //         colorCode: value.colorCode,
  //         rgbCode: value.rgbCode,
  //         btfCode: value.btf,
  //         freeGoodsId: promotionFG.freeGoodsId,
  //         productId: value.productId,
  //         productCode: value.productCode,
  //         sizeCode: value.sizeCode
  //       });
  //     }
  //   });

  //   let color = this.colorFreeGoods.filter(item => {
  //     if (item.freeGoodsId == promotionFG.freeGoodsId) {
  //       return item;
  //     }
  //   })[0];
    
  //   this.isColorFreeGoods.push(color);
  // }

  // getProductInDt(promotionDtId) {
  //   return this.productInDt.filter(item => {
  //     return item.promotionDtId == promotionDtId;
  //   })[0];
  // }

  // getProductInDtByProductId(productId) {
  //   return this.productInDt.filter(item => {
  //     if (item.productId == productId) {
  //       return item;
  //     }
  //   })[0];
  // }

  // getBtfInDt(promotionDtId) {
  //   return this.btfInDt.filter(item => {
  //     if (item.promotionDtId == promotionDtId) {
  //       return item;
  //     }
  //   })[0];
  // }

  // getProductInBtf(btf, productCode) {    
  //   return this.productProvider.getProductInBTF(this.customerInfo.customerId, btf)
  //     .then(res => {
  //       if (res.result == 'SUCCESS') {
  //         if (productCode == null) {
  //           return res.data.productList[0];
  //         } else {
  //           return res.data.productList.filter(item => {
  //             if (item.productCode == productCode) {
  //               return item;
  //             }
  //           })[0];
  //         }
  //       }
  //     });
  // }

  // getProductInFreeGoods(freeGoodsId) {
  //   return this.productInFreeGoods.filter(item => {
  //     if (item.freeGoodsId == freeGoodsId) {
  //       return item;
  //     }
  //   })[0];
  // }

  // openModalBtf(promotionDT) {
  //   let modal = this.modalCtrl.create(EoPromotionBtfPage, { btfInDt: this.btfInDt, promotionDtId: promotionDT.btfInDt.promotionDtId });

  //   modal.present();

  //   modal.onDidDismiss(data => {
  //     if (data != undefined && data.btfInDt != undefined) {
  //       this.setBtf(promotionDT, data.btfInDt);
  //     }
  //   });
  // }

  // openModalSize(promotionDT) {
  //   let modal = this.modalCtrl.create(EoPromotionSizePage, { 
  //     sizes: this.sizes,
  //     btfCode: (promotionDT.format == 'MG' || promotionDT.format == 'B') ? promotionDT.btfInDt.btf : promotionDT.btfCode
  //   });

  //   modal.present();

  //   modal.onDidDismiss(data => {
  //     if (data != undefined && data.size != undefined) {
  //       let btf;

  //       if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
  //         btf = promotionDT.btfInDt.btf;
  //       } else {
  //         btf = promotionDT.btfCode;
  //       }

  //       let index = this.isSizes.findIndex(item => {
  //         if (item.btfCode == btf && item.promotionDtId == promotionDT.promotionDtId) {
  //           return item;
  //         }  
  //       });

  //       this.isSizes[index] = data.size;
  //       this.setSize(promotionDT, data.size);
  //     } 
  //   });
  // }

  // openModalColor(promotionDT) {
  //   let modal = this.modalCtrl.create(EoPromotionColorPage, { 
  //     colors: this.colors, 
  //     size: this.getIsSize(promotionDT),
  //     btfCode: (promotionDT.format == 'MG' || promotionDT.format == 'B') ? promotionDT.btfInDt.btf : promotionDT.btfCode
  //   });

  //   modal.present();

  //   modal.onDidDismiss(data => {
  //     if (data != undefined && data.color != undefined) {
  //       let btf;

  //       if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
  //         btf = promotionDT.btfInDt.btf;
  //       } else {
  //         btf = promotionDT.btfCode;
  //       }
    
  //       let index = this.isColors.findIndex(item => {
  //         if (item.btfCode == btf && item.promotionDtId == promotionDT.promotionDtId) {
  //           return item;
  //         }  
  //       });

  //       this.isColors[index] = data.color;
  //       this.setColor(promotionDT, data.color);
  //     }
  //   });
  // }

  // getIsColor(promotionDT) {
  //   let color = {};
  //   let btf;

  //   if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
  //     btf = promotionDT.btfInDt.btfWeb;
  //   } else {
  //     btf = promotionDT.btfCode;
  //   }

  //   color = this.isColors.filter(item => {
  //     if (item.btfCode == btf) {
  //       return item;
  //     }  
  //   });

  //   return color[0];
  // }

  // getIsSize(promotionDT) {
  //   let size = {};
  //   let btf;

  //   if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
  //     btf = promotionDT.btfInDt.btfWeb;
  //   } else {
  //     btf = promotionDT.btfCode;
  //   }

  //   size = this.isSizes.filter(item => {
  //     if (item.btfCode == btf) {
  //       return item;
  //     }  
  //   });

  //   return size[0];
  // }
  
  // setBtf(promotionDT, btfInDt) {
  //   this.showLoading();

  //   this.getProductInBtf(btfInDt.btfWeb, null)
  //     .then(res => {
  //       if (res != undefined) {
  //         promotionDT.productData = res;
  //       }
  //     });
    
  //   promotionDT.btfInDt = btfInDt;
  //   promotionDT.productName = btfInDt.btfDesc;

  //   this.getSize(promotionDT.format, promotionDT, btfInDt.btfWeb);
  //   this.getColor(promotionDT.format, promotionDT, btfInDt.btfWeb);
        
  //   promotionDT.qty = promotionDT.minQty;

  //   this.hideLoading();
  // }

  // setSize(promotionDT, size) {
  //   if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
  //     this.getProductInBtf(size.btfCode, size.productCode)
  //       .then(res => {
  //         promotionDT.productData = res;
  //       });

  //     promotionDT.qty = promotionDT.minQty;
  //   } else {
  //     let productInDt = this.productInDt.filter(item => {
  //       if (item.productCode == size.productCode) {
  //         return item;
  //       }
  //     })[0];

  //     promotionDT.productData = productInDt;
  //   }
  // }

  // setColor(promotionDT, color) {
  //   if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
  //     this.getProductInBtf(color.btfCode, color.productCode)
  //       .then(res => {
  //         promotionDT.productData = res;
  //       });

  //     promotionDT.qty = promotionDT.minQty;
  //   } else {
  //     let productInDt = this.productInDt.filter(item => {
  //       if (item.productCode == color.productCode) {
  //         return item;
  //       }
  //     })[0];

  //     promotionDT.productData = productInDt
  //   }
  // }

  // incQty(promotionDT) {
  //   let product = promotionDT.productData;
  //   let qty = 1;

  //   switch (product.altUnit1Code) {
  //     case 'BOX':
  //       if (product.isBox) {
  //         qty = product.altUnit1Amount;
  //       } else {
  //         qty = 1;
  //       }
  //       break;
  //     case 'DZ':
  //       qty = product.altUnit1Amount;
  //       break;
  //     default:
  //       qty = 1;
  //       break;
  //   }
    
  //   promotionDT.qty = promotionDT.qty + qty;
  // }

  // decQty(promotionDT) {
  //   let product = promotionDT.productData;
  //   let qty = 1;

  //   switch (product.altUnit1Code) {
  //     case 'BOX':
  //       if (product.isBox) {
  //         qty = product.altUnit1Amount;
  //       } else {
  //         qty = 1;
  //       }
  //       break;
  //     case 'DZ':
  //       qty = product.altUnit1Amount;
  //       break;
  //     default:
  //       qty = 1;
  //       break;
  //   }
      
  //   if (promotionDT.qty > qty) {
  //     promotionDT.qty -= qty;
  //   }
  // }

  // edtQty(promotionDT) {
  //   let product = promotionDT.productData;

  //   if (promotionDT.qty == 0 || promotionDT.qty == undefined) {
  //     switch (product.altUnit1Code) {
  //       case 'BOX':
  //         if (product.isBox) {
  //           promotionDT.qty = 1;
  //         } else {
  //           promotionDT.qty = product.altUnit1Amount;
  //         }
  //         break;
  //       case 'DZ':
  //         promotionDT.qty = product.altUnit1Amount;
  //         break;
  //       default:
  //         promotionDT.qty = 1;
  //         break;
  //     }
  //   } else if (product.isBox || product.altUnit1Code == 'DZ') {
  //     let textAltUnit1Amount = product.altUnit1Amount;
  //     let textAltUnit1NameTh = product.unitNameTh;

  //     switch (product.altUnit1Code) {
  //       case 'DZ':
  //         textAltUnit1Amount = 1;
  //         textAltUnit1NameTh = product.altUnit1NameTh;
  //         break;
  //     }

  //     if (promotionDT.qty < product.altUnit1Amount) {
        // this.alertCtrl.create({
        //   title: 'แจ้งเตือน',
        //   message: `กรุณาสั่งซื้ออย่างน้อย ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ`,
        //   mode: 'ios',
        //   enableBackdropDismiss: false,
        //   buttons: [
        //     {
        //       text: 'ตกลง',
        //       handler: () => {
        //         //this.alertIsOpen = false;
        //       }
        //     }
        //   ]
        // }).present();

  //       promotionDT.qty = product.altUnit1Amount;
  //     } else if (promotionDT.qty % product.altUnit1Amount) {
        // this.alertCtrl.create({
        //   title: 'แจ้งเตือน',
        //   message: `ผลิตภัณฑ์ต้องสั่งซื้อทีละ ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ`,
        //   mode: 'ios',
        //   enableBackdropDismiss: false,
        //   buttons: [
        //     {
        //       text: 'ตกลง',
        //       handler: () => {
        //         //this.alertIsOpen = false;
        //       }
        //     }
        //   ]
        // }).present();
  //       promotionDT.qty = product.altUnit1Amount * Math.floor(promotionDT.qty / product.altUnit1Amount);
  //     }
  //   }
  // }
  
  // addProduct(promotionDT) {
  //   let isExists = this.promotionDT_Sel.filter(item => {
  //     if (item.productData.productCode == promotionDT.productData.productCode) {
  //       return item;
  //     }
  //   })[0] != undefined; 

  //   let productData = promotionDT.productData;
  //   let productForm = Object.assign({}, promotionDT);

  //   if (promotionDT.format == 'MG' || promotionDT.format == 'B') {  
  //     productForm.totalAmount = parseInt(productData.productPrice) * parseInt(productForm.qty);   
  //   } else {
  //     productForm.totalAmount = parseInt(productData.productPrice) * parseInt(productForm.qty);  
  //   }

  //   if (!isExists) {
  //     productForm.qty2 = productForm.qty;
  //     this.promotionDT_Sel.push(productForm);
  //   } else {
  //     let index = this.promotionDT_Sel.findIndex(item => {
  //       if (item.productData.productCode == promotionDT.productData.productCode) {
  //         return item;
  //       }
  //     });

  //     productForm.qty = parseInt(productForm.qty) + parseInt(this.promotionDT_Sel[index].qty);
  //     productForm.qty2 = productForm.qty;

  //     this.promotionDT_Sel[index] = productForm;

  //     // get total amount
  //     // get total qty
  //   }

  //   let qty = 0;

  //   if (promotionDT.minQt <= 3000) {
  //     if (this.promotionHD.checkStepBySKU) {
  //       qty = promotionDT.minQty;
  //     } else if (!this.promotionHD.checkStepBySKU) {
  //       qty = promotionDT.minQty;
  //     }
  //   }

  //   promotionDT.qty = qty;
  // }

  // delProduct(promotionDT) {
  //   let index = this.promotionDT_Sel.indexOf(promotionDT);

  //   this.promotionDT_Sel.splice(index, 1);
  //   this.promotionFG = [];
  //   //this.promotionFG_Sel = [];
  // }

  // getTotalQty() {
  //   let qty = 0;

  //   if (this.promotionDT_Sel.length > 0) {
  //     this.promotionDT_Sel.forEach(value => {
  //       qty += parseInt(value.qty);
  //     });
  //   }

  //   return qty;
  // }

  // getTotalQty2() {
  //   let qty = 0;

  //   if (this.promotionDT_Sel.length > 0) {
  //     this.promotionDT_Sel.forEach(value => {
  //       qty += parseInt((value.qty2 == 0) ? value.qty : value.qty2);
  //     });
  //   }

  //   return qty;
  // }

  // getTotalAmount() {
  //   let amount = 0;

  //   if (this.promotionDT_Sel.length > 0) {
  //     this.promotionDT_Sel.forEach(value => {
  //       if (this.promotionHD.promotionType == 'Discount') {
  //         amount += (value.productData.productPrice * value.qty);
  //       } else {
  //         amount += (value.productData.productPrice * ((value.qty2 == 0) ? value.qty : value.qty2));
  //       }
  //     });
  //   }

  //   return amount;
  // }

  // calculate() {
  //   let cartList = [];
  //   let checkQty = false;

  //   if (checkQty == false) {
  //     this.promotionDT_Sel.forEach(value => {
  //       cartList.push({
  //         promotionDTId: value.promotionDtId,
  //         customerId: this.customerInfo.customerId,
  //         productId: value.productData.productId,
  //         qty: value.qty
  //       });
  //     });

  //     let formData = {
  //       promotionId: this.promotionHD.promotionId,
  //       cartList: cartList
  //     };

  //     this.promotionProvider.chkPromotionValidate(formData)
  //       .then(res => {
  //         if (res.result == 'SUCCESS') {
  //           let chkPromotionSet = false;
  //           let totalQty = this.getTotalQty();

  //           this.promotionFG = [];

  //           if (this.promotionHD.keepBox && res.productList.length > 0) {
  //             res.productList.forEach(value => {
  //               let product = this.promotionDT_Sel.filter(item => {
  //                 if (item.promotionId == value.promotionId && item.productData.productId == value.productId) {
  //                   return item;
  //                 }
  //               })[0];

  //               product.qty2 = value.productQty;
  //             });
  //           }

  //           if (!this.promotionHD.checkStepBySKU) {
  //             chkPromotionSet = true;
  //           } else {
  //             if ((this.freeGoods[0].productQty + this.freeGoods[0].freeGoodsQty) > 0) {
  //               if (totalQty % (this.freeGoods[0].productQty + this.freeGoods[0].freeGoodsQty) == 0) {
  //                 chkPromotionSet = true;
  //               } else {
  //                 chkPromotionSet = false;
  //               }
  //             } else {
  //               chkPromotionSet = false;
  //             }
  //           }

  //           if (chkPromotionSet) {
  //             if (this.promotionHD != 'Discount' && this.freeGoods.length != 0) {
  //               res.freeGoodsList.forEach(value => {
  //                 let freeGoods = Object.assign({}, this.freeGoods.filter(item => {
  //                   if (item.freeGoodsId == value.freeGoodsId) {
  //                     return item;
  //                   }
  //                 })[0]);

  //                 if (freeGoods != undefined) {
  //                   freeGoods.editBtf = false;
  //                   freeGoods.editSize = false;
  //                   freeGoods.editColor = false;
  //                   freeGoods.qty = value.freeGoodsQty;
  //                   freeGoods.qty2 = value.freeGoodsQty;
  //                   freeGoods.freeGoodsMatId = value.freeGoodsMatId;

  //                   if (this.promotionHD.isPromotionSet) {
  //                     freeGoods.qty = value.freeGoodsQty * this.countPromotionSet;
  //                     freeGoods.qty2 = value.freeGoodsQty * this.countPromotionSet;
  //                   } else {
  //                     freeGoods.qty = value.freeGoodsQty;
  //                     freeGoods.qty2 = value.freeGoodsQty;
  //                   }

  //                   if (value.freeGoodsQty > 0) {
  //                     this.promotionFG.push(freeGoods);
  //                   }
  //                 }
                  
  //                 this.setFreeGoods(this.promotionFG);
  //               });

  //               if (this.promotionHD.checkStepBySKU) {
  //                 this.promotionFG.forEach(value => {
  //                   this.addFreeGoods(value);
  //                 });
  //               }

  //               this.navCtrl.push(EoPromotionFreegoodPage, {
  //                 promotionHD: this.promotionHD,
  //                 promotionDT: this.promotionDT,
  //                 promotionDT_Sel: this.promotionDT_Sel,
  //                 promotionFG: this.promotionFG,
  //                 promotionFG_Sel: this.promotionFG_Sel,
  //                 freeGoods: this.freeGoods,
  //                 sizeFreeGoods: this.sizeFreeGoods,
  //                 colorFreeGoods: this.colorFreeGoods,
  //                 isColorFreeGoods: this.isColorFreeGoods,
  //                 isSizeFreeGoods: this.sizeFreeGoods,
  //                 productInFreeGoods: this.productInFreeGoods
  //               });
  //             }
  //           } else {
  //             this.alertCtrl.create({
  //               title: 'แจ้งเตือน',
  //               message: 'จำนวนสั่งซื้อไม่ครบชุดโปรโมชั่น',
  //               mode: 'ios',
  //               enableBackdropDismiss: false,
  //               buttons: [
  //                 {
  //                   text: 'ตกลง',
  //                   handler: () => {
  //                     //this.alertIsOpen = false;
  //                   }
  //                 }
  //               ]
  //             }).present();
  //           }
  //         } else if (res.result == 'WARINIG') {
  //           if (this.promotionHD.checkStepBySKU) {
  //             this.alertCtrl.create({
  //               title: 'แจ้งเตือน',
  //               message: `รายการสินค้าต้องมีจำนวนสั่งซื้อรวมอย่างน้อย ${(parseInt(this.freeGoods[0].productQty) + parseInt(this.freeGoods[0].freeGoodsQty))} หน่วย`,
  //               mode: 'ios',
  //               enableBackdropDismiss: false,
  //               buttons: [
  //                 {
  //                   text: 'ตกลง',
  //                   handler: () => {
  //                     //this.alertIsOpen = false;
  //                   }
  //                 }
  //               ]
  //             }).present();
  //           } else {
  //             if (this.promotionHD.promotionType == 'Discount' && this.freeGoods.length == 0) {
  //               this.alertCtrl.create({
  //                 title: 'แจ้งเตือน',
  //                 message: 'โปรดตรวจสอบรายการสั่งซื้อ เพื่อรับโปรโมชั่น',
  //                 mode: 'ios',
  //                 enableBackdropDismiss: false,
  //                 buttons: [
  //                   {
  //                     text: 'ตกลง',
  //                     handler: () => {
  //                       //this.alertIsOpen = false;
  //                     }
  //                   }
  //                 ]
  //               }).present();
  //             } else {
  //               this.alertCtrl.create({
  //                 title: 'แจ้งเตือน',
  //                 message: res.invalidList[0],
  //                 mode: 'ios',
  //                 enableBackdropDismiss: false,
  //                 buttons: [
  //                   {
  //                     text: 'ตกลง',
  //                     handler: () => {
  //                       //this.alertIsOpen = false;
  //                     }
  //                   }
  //                 ]
  //               }).present();
  //             }
  //           }
  //         } else {
  //           this.alertCtrl.create({
  //             title: 'เกิดข้อผิดพลาด',
  //             message: res.remarkText,
  //             mode: 'ios',
  //             enableBackdropDismiss: false,
  //             buttons: [
  //               {
  //                 text: 'ตกลง',
  //                 handler: () => {
  //                   //this.alertIsOpen = false;
  //                 }
  //               }
  //             ]
  //           }).present();
  //         }
  //       });
  //   }
  // }

  // setFreeGoods(promotionFG) {
  //   let productInFreeGoods;

  //   promotionFG.forEach(value => {
  //     switch (value.format) {
  //       case 'BTF':
  //         if (this.promotionHD.checkStepBySKU) {
  //           productInFreeGoods = this.getProductInDtByProductId(value.freeGoodsMatId);
  //         } else {
  //           productInFreeGoods = this.getProductInFreeGoods(value.freeGoodsId);
  //         }

  //         this.getSizeFreeGoods(value.format, value);
  //         this.getColorFreeGoods(value.format, value);

  //         value.editSize = true;
  //         value.editColor = true;

  //         if (!this.promotionHD.checkStepBySKU) {
  //           value.editQty = true;
  //         }
  
  //         value.productName = productInFreeGoods.productNameTh;
  //         value.productFreeGoodsData = productInFreeGoods;
  //         break;
  //       case 'BTFS':
  //         if (this.promotionHD.checkStepBySKU) {
  //           productInFreeGoods = this.getProductInDtByProductId(value.freeGoodsMatId);
  //         } else {
  //           productInFreeGoods = this.getProductInFreeGoods(value.freeGoodsId);
  //         }

  //         this.getSizeFreeGoods(value.format, value);
  //         this.getColorFreeGoods(value.format, value);

  //         value.editColor = true;

  //         if (!this.promotionHD.checkStepBySKU) {
  //           value.editQty = true;
  //         }

  //         value.productName = productInFreeGoods.productNameTh;
  //         value.productFreeGoodsData = productInFreeGoods;
  //         break;
  //       case 'SKU':
  //         if (this.promotionHD.checkStepBySKU) {
  //           productInFreeGoods = this.getProductInDtByProductId(value.freeGoodsMatId);
  //         } else {
  //           productInFreeGoods = this.getProductInFreeGoods(value.freeGoodsId);
  //         }

  //         this.getSizeFreeGoods(value.format, value);
  //         this.getColorFreeGoods(value.format, value);

  //         value.productName = productInFreeGoods.productNameTh;
  //         value.productFreeGoodsData = productInFreeGoods;
  //         break;
  //     }
  //   });
  // }

  // addFreeGoods(promotionFG) {
  //   let isExists = this.promotionFG_Sel.filter(item => {
  //     if (item.productFreeGoodsData.productCode == promotionFG.productFreeGoodsData.productCode) {
  //       return item;
  //     }
  //   })[0] != undefined;

  //   let productForm = Object.assign({}, promotionFG);

  //   if (this.promotionHD.numFreeGoods > 0) {
  //     if (!this.promotionHD.checkStepBySKU && (this.promotionFG_Sel.length + 1 > this.promotionHD.numFreeGoods)) {
        // this.alertCtrl.create({
        //   title: 'แจ้งเตือน',
        //   message: `ไม่สามารถเลือกของแถมได้เกิน ${this.promotionHD.numFreeGoods} รายการ`,
        //   mode: 'ios',
        //   enableBackdropDismiss: false,
        //   buttons: [
        //     {
        //       text: 'ตกลง',
        //       handler: () => {
        //         //this.alertIsOpen = false;
        //       }
        //     }
        //   ]
        // }).present();

  //       return false;
  //     }
  //   }

  //   if (!this.promotionHD.checkStepBySKU) {
  //     let qty = 0;
  //     let frg = this.promotionFG_Sel.filter(item => {
  //       if (item.freeGoodsId == promotionFG.freeGoodsId) {
  //         return item;
  //       }
  //     });

  //     if (frg.length > 0) {
  //       frg.forEach(value => {
  //         qty += parseInt(value.qty);
  //       });

  //       if ((qty + parseInt(promotionFG.qty)) > promotionFG.qty2) {
  //         this.alertCtrl.create({
  //           title: 'แจ้งเตือน',
  //           message: 'จำนวนแถมเกินจำนวนแถมที่ได้รับ',
  //           mode: 'ios',
  //           enableBackdropDismiss: false,
  //           buttons: [
  //             {
  //               text: 'ตกลง',
  //               handler: () => {
  //                 //this.alertIsOpen = false;
  //               }
  //             }
  //           ]
  //         }).present();

  //         return false;
  //       }
  //     }
  //   }

  //   if (!isExists) {
  //     this.promotionFG_Sel.push(productForm);
  //   } else {
  //     let index = this.promotionFG_Sel.findIndex(item => {
  //       if (item.freeGoodsMatId == promotionFG.freeGoodsMatId) {
  //         return item;
  //       }
  //     });

  //     this.promotionFG_Sel[index] = productForm;
  //   }
  // } 

  // addCart() {
  //   this.btnLoading = true;

  //   let cartList = [];
  //   let promotionList = [{ 
  //     promotionId: this.promotionHD.promotionId,
  //     freeGoodsId: 0,
  //     freeProductId: 0,
  //     qty: 0
  //   }];

  //   this.promotionDT_Sel.forEach(value => {
  //     cartList.push({
  //       promotionDTId: value.promotionDtId,
  //       customerId: this.customerInfo.customerId,
  //       productId: value.productData.productId,
  //       qty: value.qty2,
  //       userName: this.userInfo.userName
  //     });
  //   });
  
  //   let formData = {
  //     cartList: cartList,
  //     promotionList: promotionList
  //   };

  //   this.cartProvider.addCart(formData)
  //     .then(res => {
  //       if (res.result == 'SUCCESS') {
  //         this.btnLoading = false;

  //         this.alertCtrl.create({
  //           title: 'สำเร็จ',
  //           message: 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย',
  //           mode: 'ios',
  //           enableBackdropDismiss: false,
  //           buttons: [
  //             {
  //               text: 'ตกลง',
  //               handler: () => {
  //                 this.navCtrl.pop();
  //               }
  //             }
  //           ]
  //         }).present();
  //       }
  //     });
  // }

  // addPromotionSet() {
  //   this.countPromotionSet += this.promotionSetValue;

  //   this.promotionDT.forEach((value, key) => {
  //     let isExists = this.promotionDT_Sel.filter(item => {
  //       if (item.productData.productCode == value.productData.productCode) {
  //         return item;
  //       }
  //     })[0] != undefined;

  //     let productData = value.productData;
  //     let productForm = Object.assign({}, value);

  //     if (!isExists) {
  //       productForm.qty = value.qty * this.promotionSetValue; 
  //       productForm.qty2 = productForm.qty;
  //       productForm.totalAmount = productData.productPrice * productForm.qty;

  //       this.promotionDT_Sel.push(productForm);
  //     } else {
  //       productForm.qty = this.promotionDT_Sel[key].qty + (value.qty * this.promotionSetValue);
  //       productForm.qty2 = productForm.qty;
  //       productForm.totalAmount = productData.productPrice * productForm.qty;
        
  //       this.promotionDT_Sel[key] = productForm;
  //     }
  //   });
  // }

  // delPromotionSet() {
  //   this.promotionDT_Sel = [];
  //   this.countPromotionSet = 0;
  // }

  // clearForm() {
  //   this.promotionDT_Sel = [];
  //   this.promotionFG_Sel = [];

  //   this.promotionDT.forEach(value => {
  //     if (this.promotionHD.checkStepBySKU) {
  //       value.qty = value.minQty;
  //     } else if (!this.promotionHD.checkStepBySKU) {
  //       value.qty = value.minQty;
  //     }
  //   });
  // }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'page-eo-contact',
  templateUrl: 'eo-contact.html',
})
export class EoContactPage {

  config: any;
  customerInfo: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private platform: Platform) {

    this.config = this.storageProvider.getConfig();
    this.customerInfo = this.storageProvider.getCustomerInfo();
  }

  ionViewDidLoad() {

  }
}

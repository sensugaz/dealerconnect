import { Component, ViewChild } from '@angular/core';
import { App, ModalController, NavController, NavParams, Platform, Tabs, Events } from 'ionic-angular';
import { EoMainPage } from '../eo-main/eo-main';
import { CartProvider } from '../../../providers/eordering/cart';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoCartPage } from '../eo-cart/eo-cart';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { EoStorePage } from '../eo-store/eo-store';
import { DcHomePage } from '../../dealerconnect/dc-home/dc-home';
import { EoMenuPage } from '../eo-menu/eo-menu';

@Component({
  selector: 'page-eo-tabs',
  templateUrl: 'eo-tabs.html',
})
export class EoTabsPage {

  @ViewChild('myTabs') tabRef: Tabs;

  tab1Root: any;

  userInfo: any;
  customerInfo: any;
  totalQty: number;
  cartSubscribe: any;
  page: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private cartProvider: CartProvider,
              private modalCtrl: ModalController,
              private platform: Platform,
              private events: Events,
              private app: App) {

    this.tab1Root = EoMainPage;
    this.page = this.navParams.get('page');
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.events.publish('customer:load', this.customerInfo);
    this.totalQty = 0;

    console.log(this.page);
    
    this.cartSubscribe = this.cartProvider.onUpdated.subscribe({
      next: (event: any) => {
        this.getTotalQty();
      }
    });

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.getTotalQty();
  }

  tabSelecte($event) {
    let index = $event.index;

    if (this.userInfo.userTypeDesc == 'Multi') {
      switch (index) {
        case 1:
          this.modalCtrl.create(EoCartPage, { mode: 'TAB' }).present();
          break
        case 2:
          this.navCtrl.setRoot(EoStorePage, { action: 'changeStore' });
          break;
        case 3:
          //this.navCtrl.setRoot(DcHomePage);
          this.app.getRootNav().setRoot(DcHomePage);
          break;
      }
    } else {
      switch (index) {
        case 1:
          this.modalCtrl.create(EoCartPage).present();
          break
        case 2:
          //this.navCtrl.setRoot(DcHomePage);
          this.app.getRootNav().setRoot(DcHomePage);
          break;
      }
    }
  }

  getTotalQty() {
    this.cartProvider.getCart(this.userInfo.userName, this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.totalQty = res.data.cartList.filter(value => !value.isBOM).length + res.data.cartBOMItems.length;
        }
      });
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];
        
        console.log('eoTabs');

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}

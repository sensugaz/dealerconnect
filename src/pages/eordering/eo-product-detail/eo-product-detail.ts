import { Eordering } from './../../../app/modules/eordering/eordering.componet';
import { Component } from '@angular/core';
import {
  AlertController, ModalController, NavController, NavParams, ToastController,
  ViewController,
  Events,
  Platform,
  App,
  LoadingController
} from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { StorageProvider } from '../../../providers/shared/storage';
import { ProductProvider } from '../../../providers/eordering/product';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EoColorPage } from '../eo-color/eo-color';
import { CartProvider } from '../../../providers/eordering/cart';
import { EoProductFilterPage } from '../eo-product-filter/eo-product-filter';
import { EoTabsPage } from '../eo-tabs/eo-tabs';
import { EoCartPage } from '../eo-cart/eo-cart';
import { EoSizePage } from '../eo-size/eo-size';
import { FavoriteProvider } from '../../../providers/eordering/favorite';
import { EoProductPromotionInfoPage } from '../eo-product-promotion-info/eo-product-promotion-info';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { PromotionProvider } from '../../../providers/eordering/promotion';
import { EoMenuPage } from '../eo-menu/eo-menu';
import { EoStorePage } from '../eo-store/eo-store';
import { EoProductDetailColorPage } from '../eo-product-detail-color/eo-product-detail-color';
import { EoProductDetailSizePage } from '../eo-product-detail-size/eo-product-detail-size';

@Component({
  selector: 'page-eo-product-detail',
  templateUrl: 'eo-product-detail.html',
})
export class EoProductDetailPage {

  mode: any;
  form: FormGroup;
  config: any;
  userInfo: any;
  customerInfo: any;
  product: any;
  btfInfo: any;
  loading: any;
  btnLoading: boolean;
  products: any[];
  boms: any[];
  sizes: any[];
  colors: any[];
  sizeFlag: string;
  colorFlag: string;
  sizeName: any;
  rgbCode: any;
  colorCode: any;
  promotions: any[];
  totalQty: number;
  cartSubscribe: any;
  page: number;
  alertIsOpen: boolean = true;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private configProvider: ConfigProvider,
              private storageProvider: StorageProvider,
              private productProvider: ProductProvider,
              private formBuilder: FormBuilder,
              private modalCtrl: ModalController,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private cartProvider: CartProvider,
              private favoriteProvider: FavoriteProvider,
              private popoverCtrl: PopoverController,
              private promotionProvider: PromotionProvider,
              private platform: Platform,
              private loadingCtrl: LoadingController,
              private app: App,
              private events: Events) {

    this.mode = this.navParams.get('mode');
    this.form = this.formBuilder.group({
      size: ['', Validators.required],
      color: [''],
      qty: ['']
    });

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.btfInfo = this.navParams.get('product');
    this.product = this.navParams.get('product');
    this.btnLoading = false;
    this.totalQty = 0;
    this.page = this.navParams.get('page');

    this.cartSubscribe = this.cartProvider.onUpdated.subscribe({
      next: (event: any) => {
        this.getTotalQty();
      }
    });

    this.events.subscribe('product:close', () => {
      this.navCtrl.pop();
    });

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.getTotalQty();
    this.fetchProduct();
  }

  addCart(form) {
    let promotions = [];
    let checkQty = false;

    if (form.qty == undefined || form.qty == 0) {
      if (this.product.altUnit1Code == 'BOX') {
        if (this.product.isBox) {
          this.form.patchValue({ qty: 1 });
        } else {
          this.form.patchValue({
            qty: this.product.altUnit1Amount
          });
        }
      } else if (this.product.altUnit1Code == 'DZ') {
        this.form.patchValue({
          qty: this.product.altUnit1Amount
        });
      } else {
        this.form.patchValue({ qty: 1 });
      }
    } else if (this.product.isBox || this.product.altUnit1Code == 'DZ') {
      let textAltUnit1Amount = this.product.altUnit1Amount;
      let textAltUnit1NameTh = this.product.unitNameTh;

      switch (this.product.altUnit1Code) {
        case 'DZ':
          textAltUnit1Amount = 1;
          textAltUnit1NameTh = this.product.altUnit1NameTh;
          break;
      }

      if (form.qty < this.product.altUnit1Amount) {
        this.alertIsOpen = true;

        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `กรุณาสั่งซื้ออย่างน้อย ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        this.form.patchValue({
          qty: this.product.altUnit1Amount
        });
      } else if (form.qty % this.product.altUnit1Amount) {
        this.alertIsOpen = true;

        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `ผลิตภัณฑ์ต้องสั่งซื้อทีละ ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        this.form.patchValue({
          qty: this.product.altUnit1Amount * Math.floor(form.qty / this.product.altUnit1Amount)
        });
      } else {
        checkQty = true;
      }
    } else {
      checkQty = true;
    }

    if (checkQty) {
      this.btnLoading = true;

      this.cartProvider.getCart(this.userInfo.userName, this.customerInfo.customerId)
        .then(res => {
          if (res.result == 'SUCCESS') {
            let product = res.data.cartList.filter(value => value.productId == this.product.productId)[0];
            let isEmpty = product == undefined || product == null;

            let formData = {
              cartList: [{
                customerId: this.customerInfo.customerId,
                userName: this.userInfo.userName,
                productId: this.product.productId,
                qty: (isEmpty) ? form.qty : product.qty + form.qty
              }],
              promotionList: promotions
            };

            if (isEmpty) {
              this.cartProvider.addCart(formData)
                .then(res => {
                  if (res.result == 'SUCCESS'){
                    this.alertIsOpen = true;

                    this.alertCtrl.create({
                      title: 'สำเร็จ',
                      message: 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย',
                      mode: 'ios',
                      enableBackdropDismiss: false,
                      buttons: [
                        {
                          text: 'ตกลง',
                          handler: () => {
                            this.alertIsOpen = false;
                          }
                        }
                      ]
                    }).present();
                    

                    //this.getProduct();
                    this.setSize(this.sizes[0]);
                    this.clearForm();
                  }
                });
            } else {
              this.cartProvider.updateCart(formData)
                .then(res => {
                  if (res.result == 'SUCCESS') {
                    this.alertIsOpen = true;

                    this.alertCtrl.create({
                      title: 'สำเร็จ',
                      message: 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย',
                      mode: 'ios',
                      enableBackdropDismiss: false,
                      buttons: [
                        {
                          text: 'ตกลง',
                          handler: () => {
                            this.alertIsOpen = false;
                          }
                        }
                      ]
                    }).present();

                    //this.getProduct();
                    this.setSize(this.sizes[0]);
                    this.clearForm();
                  }
                });
            }
          }

          this.btnLoading = false;
        });
    }
  }

  openCart() {
    let modal = this.modalCtrl.create(EoCartPage, { mode: this.mode });

    modal.present();

    modal.onDidDismiss(() => {
      this.backButtonHandler();
    });
  }

  setFavorite(btfInfo) {
    let formData = {
      customerId: this.customerInfo.customerId,
      userName: this.userInfo.userName,
      btfCode: btfInfo.btf
    };

    if (btfInfo.isFavorite) {
      this.favoriteProvider.removeFavorite(formData)
        .then(res => {
          if (res.result == 'SUCCESS') {
            btfInfo.isFavorite = false;

            this.toastCtrl.create({
              message: 'ลบรายการโปรดเรียบร้อย',
              duration: 1500
            }).present();
          }
        });
    } else {
      this.favoriteProvider.addFavorite(formData)
        .then(res => {
          if (res.result == 'SUCCESS') {
            btfInfo.isFavorite = true;

            this.toastCtrl.create({
              message: 'เพิ่มรายการโปรดเรียบร้อย',
              duration: 1500
            }).present();
          }
        });
    }
  }

  incQty() {
    let qty = 1;

    switch (this.product.altUnit1Code) {
      case 'BOX':
        if (this.product.isBox) {
          qty = this.product.altUnit1Amount;
        } else {
          qty = 1;
        }
        break;
      case 'DZ':
        qty = this.product.altUnit1Amount;
        break;
      default:
        qty = 1;
        break;
    }

    this.form.patchValue({
      qty: parseInt(this.form.value.qty) + qty
    });
  }

  decQty() {
    let qty = 1;

    switch (this.product.altUnit1Code) {
      case 'BOX':
        if (this.product.isBox) {
          qty = this.product.altUnit1Amount;
        } else {
          qty = 1;
        }
        break;
      case 'DZ':
        qty = this.product.altUnit1Amount;
        break;
      default:
        qty = 1;
        break;
    }

    if (this.form.value.qty > this.product.qtyPerBox) {
      this.form.patchValue({
        qty: this.form.value.qty -= qty
      });
    }

    if (!this.product.isBox && this.form.value.qty == 0) {
      this.form.patchValue({ qty: 1 });
    }
  }

  edtQty() {
    let qty = this.form.value.qty;

    if (qty == undefined || qty == 0) {
      if (this.product.altUnit1Code == 'BOX') {
        if (this.product.isBox) {
          this.form.patchValue({ qty: 1 });
        } else {
          this.form.patchValue({
            qty: this.product.altUnit1Amount
          });
        }
      } else if (this.product.altUnit1Code == 'DZ') {
        this.form.patchValue({
          qty: this.product.altUnit1Amount
        });
      } else {
        this.form.patchValue({ qty: 1 });
      }
    } else if (this.product.isBox || this.product.altUnit1Code == 'DZ') {
      let textAltUnit1Amount = this.product.altUnit1Amount;
      let textAltUnit1NameTh = this.product.unitNameTh;

      switch (this.product.altUnit1Code) {
        case 'DZ':
          textAltUnit1Amount = 1;
          textAltUnit1NameTh = this.product.altUnit1NameTh;
          break;
      }

      if (qty < this.product.altUnit1Amount) {
        this.alertIsOpen = true;

        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `กรุณาสั่งซื้ออย่างน้อย ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        this.form.patchValue({
          qty: this.product.altUnit1Amount
        });
      } else if (qty % this.product.altUnit1Amount) {
        this.alertIsOpen = true;

        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `ผลิตภัณฑ์ต้องสั่งซื้อทีละ ${textAltUnit1Amount} ${textAltUnit1NameTh} ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        this.form.patchValue({
          qty: this.product.altUnit1Amount * Math.floor(qty / this.product.altUnit1Amount)
        });
      }
    }
  }

  openModalColor() {
    if (this.colors.length > 0) {
      let modal = this.modalCtrl.create(EoProductDetailColorPage, {
        colors: this.colors,
        size: this.form.value.size,
        colorFlag: this.colorFlag
      });

      modal.onDidDismiss(data => {
        if (data != undefined && data.color != undefined) {
          this.colorCode = data.color.colorCode;
          this.rgbCode = data.color.rgbCode;

          this.setProduct(this.colorCode);
        }

        this.backButtonHandler();
      });

      modal.present();
    } else {
      this.toastCtrl.create({
        message: 'ไม่มีสีให้เลือก',
        duration: 1500
      }).present();
    }
  }

  openModalSize() {
    if (this.sizes.length > 0) {
      let modal = this.modalCtrl.create(EoProductDetailSizePage, {
        sizes: this.sizes
      });

      modal.onDidDismiss(data => {
        if (data != undefined && data.size != undefined) {
          this.setSize(data.size);
        }

        this.backButtonHandler();
      });
      
      modal.present();
    } else {
      this.toastCtrl.create({
        message: 'ไม่มีขนาดให้เลือก',
        duration: 2000
      }).present();
    }
  }

  openModalPromotion() {
    let modal = this.modalCtrl.create(EoProductPromotionInfoPage, { 
      config: this.config,
      btfInfo: this.btfInfo,
      promotions: this.promotions
    }, 
    { 
      cssClass: 'promotion__modal' 
    });

    modal.present();

    modal.onDidDismiss(() => {
      this.backButtonHandler();
    });
  }

  gotoBack() {
    this.navCtrl.pop();
  }

  getProduct() {
    let listColor = [];

    this.colors.forEach(value => {
      if (value.sizeCode == this.form.value.size) {
        listColor.push(value);
      }
    });

    this.rgbCode = listColor[0] ? listColor[0].rgbCode : '';
    this.colorCode = listColor[0] ? listColor[0].colorCode : '';

    this.products.forEach(value => {
      if ((value.sizeCode == this.form.value.size) && (value.colorCode == this.colorCode)) {
        this.product = value;

        this.form.patchValue({
          qty: (this.product.altUnit1Amount > 0) ? this.product.altUnit1Amount : 1
        });
      }
    });
  }

  setSize(size) {
    this.sizeName = size.sizeName;
    this.form.patchValue({ size: size.sizeCode });

    this.getProduct();
  }

  setProduct(colorCode) {
    this.products.forEach(value => {
      if ((value.sizeCode == this.form.value.size) && (value.colorCode == colorCode)) {
        this.product = value;

        this.form.patchValue({
          qty: (this.product.altUnit1Amount > 0) ? this.product.altUnit1Amount : 1,
          colorCode: colorCode
        });
      }
    });
  }

  getTotalQty() {
    this.cartProvider.getCart(this.userInfo.userName, this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.totalQty = res.data.cartList.filter(value => !value.isBOM).length + res.data.cartBOMItems.length;
        }
      });
  }

  fetchProduct() {
    this.showLoading();

    this.productProvider.getProductInBTF(this.customerInfo.customerId, this.btfInfo.btfWeb)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.btfInfo = res.data.btfInfo;

          this.products = res.data.productList;
          this.boms = res.data.productInBOMList;

          this.sizes = [];
          this.colors = [];

          this.products.forEach(value => {
            this.sizes.push({
              sizeCode: value.sizeCode,
              sizeName: value.sizeName,
              productCode: value.productCode
            });

            this.colors.push({
              colorCode: value.colorCode,
              colorNameTh: value.colorNameTh,
              rgbCode: value.rgbCode,
              sizeCode: value.sizeCode,
              productCode: value.productCode
            });
          });
          
          this.sizes = this.sizes.sort((a: any, b: any) => {
            if (a.sizeCode < b.sizeCode) {
              return -1;
            } else if (a.sizeCode > b.sizeCode) {
              return 1;
            } else {
              return 0;
            }
          });

          this.colors = this.colors.sort((a: any, b: any) => {
            if (a.colorCode < b.colorCode) {
              return -1;
            } else if (a.colorCode > b.colorCode) {
              return 1;
            } else {
              return 0;
            }
          });

          if (this.products[0].isBox) {
            this.form.patchValue({
              qty: this.products[0].qtyPerBox
            });
          }

          this.form.patchValue({
            size: this.sizes[0].sizeCode,
            color: this.colors[0].colorCode
          });

          this.sizeFlag = this.products[0].sizeFlag;
          this.colorFlag = this.products[0].colorFlag;

          this.sizeName = this.sizes[0].sizeName;
          this.colorCode = this.colors[0] ? this.colors[0].colorCode : '';
          this.rgbCode = this.colors[0].rgbCode;

          this.promotionProvider.getPromotionInBTF(this.customerInfo.customerId, this.btfInfo.btf)
            .then(res => {
              if (res.result == 'SUCCESS') {
                this.promotions = res.data.promotionHDList;

                if (this.promotions.length > 0) {
                  this.openModalPromotion();
                }
              }
            })

          this.getProduct();
        }

        this.hideLoading();
      });
  }

  clearForm() {
    this.form.patchValue({
      qty: 0,
      size: this.sizes[0].sizeCode,
      color: this.colors[0].colorCode
    });

    this.colorCode = this.colors[0] ? this.colors[0].colorCode : '';
    this.rgbCode = this.colors[0].rgbCode;

    this.getProduct();
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alertIsOpen) {
          if (this.mode == 'POP') {
            this.app.getRootNav().setRoot(EoTabsPage, { page: this.page }, {
              animate: true,
              animation: 'ease-in'
            });
          } else {
            this.navCtrl.pop();
          }
        }
      });
    }
  }
}
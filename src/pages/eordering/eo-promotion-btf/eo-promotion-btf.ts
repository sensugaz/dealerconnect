import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-eo-promotion-btf',
  templateUrl: 'eo-promotion-btf.html',
})
export class EoPromotionBtfPage {

  btfInDt: any[];
  promotionDtId: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private viewCtrl: ViewController) {

    this.btfInDt = this.navParams.get('btfInDt');
    this.promotionDtId = this.navParams.get('promotionDtId');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EoPromotionBtfPage');
  }

  selectProduct(btfInDt) {
    this.viewCtrl.dismiss({ btfInDt: btfInDt });
  } 

  close() {
    this.viewCtrl.dismiss();
  }
}

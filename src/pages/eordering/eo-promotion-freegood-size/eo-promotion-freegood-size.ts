import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EoPromotionFreegoodSizePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eo-promotion-freegood-size',
  templateUrl: 'eo-promotion-freegood-size.html',
})
export class EoPromotionFreegoodSizePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EoPromotionFreegoodSizePage');
  }

}

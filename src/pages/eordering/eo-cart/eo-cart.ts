import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController, Platform, App } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { ConfigProvider } from '../../../providers/eordering/config';
import { CartProvider } from '../../../providers/eordering/cart';
import { EoCheckoutPage } from '../eo-checkout/eo-checkout';

@Component({
  selector: 'page-eo-cart',
  templateUrl: 'eo-cart.html',
})
export class EoCartPage {

  mode: any;
  userInfo: any;
  customerInfo: any;
  config: any;
  products: any[];
  boms: any[];
  totalAmount: number;
  loading: boolean;
  cartSubscribe: any;
  alertIsOpen: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private storageProvider: StorageProvider,
              private configProvider: ConfigProvider,
              private cartProvider: CartProvider,
              private alertCtrl: AlertController,
              private platform: Platform,
              private app: App) {

    this.mode = this.navParams.get('mode');
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    
    this.config = this.storageProvider.getConfig();

    this.loading = false;

    this.cartSubscribe = this.cartProvider.onUpdated.subscribe({
      next: (event: any) => {
        //this.fetchCart();
      }
    });

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    //this.fetchCart();
  }

  ionViewCanEnter() {
    this.fetchCart();
  }

  close() {
    this.viewCtrl.dismiss();
  }

  incQty(product) {
    let qty = 1;
  
    switch (product.altUnitCode) {
      case 'BOX':
        if (product.isBox) {
          qty = product.altUnitAmount;
        } else {
          qty = 1;
        }
        break;
      case 'DZ':
        qty = product.altUnitAmount;
        break;
      default:
        qty = 1;
        break;
    }

    let formData = {
      cartList: [{
        userName: this.userInfo.userName,
        customerId: this.customerInfo.customerId,
        productId: product.productId,
        qty: product.qty += qty
      }]
    };

    this.cartProvider.updateCart(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          product.qty = formData.cartList[0].qty;
          product.totalAmount = product.amount * product.qty;

          this.getTotalAmount();
        }
      });
  }

  decQty(product) {
    let _ = Object.assign({}, product);
    let qty = 1;

    switch (product.altUnitCode) {
      case 'BOX':
        if (product.isBox) {
          qty = product.altUnitAmount;
        } else {
          qty = 1;
        }
        break;
      case 'DZ':
        qty = product.altUnitAmount;
        break;
      default:
        qty = 1;
        break;
    }

    if (_.qty > qty) {
      let formData = {
        cartList: [{
          userName: this.userInfo.userName,
          customerId: this.customerInfo.customerId,
          productId: product.productId,
          qty: _.qty - qty
        }]
      };

      this.cartProvider.updateCart(formData)
        .then(res => {
          if (res.result == 'SUCCESS') {
            product.qty = formData.cartList[0].qty;
            product.totalAmount = product.amount * product.qty;

            this.getTotalAmount();
          }
        });
    }
  }

  edtQty(product) {
    if (product.qty == undefined || product.qty == 0) {
      if (product.altUnitCode == 'BOX') {
        if (product.isBox) {
          product.qty = 1;
        } else {
          product.qty = product.altUnitAmount
        }
      } else if (product.altUnitCode == 'DZ') {
        product.qty = product.altUnitAmount;
      } else {
        product.qty = 1;
      }
    } else if (product.isBox || product.altUnitCode == 'DZ') {
      let textAltUnitAmount = product.altUnitAmount;
      let textAltUnitNameTh = product.unitNameTh;

      switch (product.altUnitCode) {
        case 'DZ':
          textAltUnitAmount = 1;
          textAltUnitNameTh = product.altUnitNameTh;
          break;
      }

      if (product.qty < product.altUnitAmount) {
        this.alertIsOpen = true;

        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `กรุณาสั่งซื้ออย่างน้อย ${textAltUnitAmount} ${textAltUnitNameTh} ค่ะ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        product.qty = product.altUnitAmount;
      } else if (product.qty % product.altUnitAmount) {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          message: `ผลิตภัณฑ์ต้องสั่งซื้อทีละ ${textAltUnitAmount} ${textAltUnitNameTh} ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ`,
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.alertIsOpen = false;
              }
            }
          ]
        }).present();

        product.qty = product.altUnitAmount * Math.floor(product.qty / product.altUnitAmount)
      }
    }

    let formData = {
      cartList: [{
        userName: this.userInfo.userName,
        customerId: this.customerInfo.customerId,
        productId: product.productId,
        qty: product.qty
      }]
    };


    this.cartProvider.updateCart(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.getTotalAmount();
        }
      });
  }

  delCart(product) {
    let index = this.products.indexOf(product);

    let formData = {
      cartList: [{
        userName: this.userInfo.userName,
        customerId: this.customerInfo.customerId,
        productId: product.productId,
        promotionId: 0
      }]
    };


    this.cartProvider.removeCart(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (this.products.length > 1) {
            this.products.splice(index, 1);
          } else {
            this.products = [];
          }

          if (product.isBOM) {
            this.boms.forEach((value, key) => {
              if (product.productCode == value.productRefCode) {
                this.boms.splice(key, 1);
              }
            });
          }

          this.getTotalAmount();
        }
      });
  }

  delCartAll() {
    if (this.products.length > 0) {
      let formData = {
        cartList: [{
          customerId: this.customerInfo.customerId,
          productId: 0,
          userName: this.userInfo.userName,
          promotionId: 0
        }]
      };

      this.alertIsOpen = true;

      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'ต้องการลบสินค้าทั้งหมดในตะกร้าใช่หรือไม่ ?',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ยกเลิก',
            handler: () => {
              this.alertIsOpen = false;
            }
          },
          {
            text: 'ตกลง',
            handler: () => {
              this.cartProvider.removeCart(formData)
                .then((res) => {
                  if (res.result == 'SUCCESS') {
                    this.products = [];
                    this.boms = [];

                    this.alertCtrl.create({
                      title: 'แจ้งเตือน',
                      message: 'ลบสินค้าเรียบร้อย',
                      mode: 'ios',
                      enableBackdropDismiss: false,
                      buttons: [
                        {
                          text: 'ตกลง',
                          handler: () => {
                            this.alertIsOpen = false;
                          }
                        }
                      ]
                    }).present();
                  }
                });
            }
          }
        ]
      }).present();
    }
  }

  continuous() {
    this.viewCtrl.dismiss();
  }

  checkout() {
    this.navCtrl.push(EoCheckoutPage, { mode: this.mode });
  }

  getTotalAmount() {
    this.totalAmount = 0;

    this.products.forEach(pv => {
      this.totalAmount += pv.totalAmount;

      this.boms.forEach(bv => {
        if (bv.productRefCode == pv.productCode) {
          this.totalAmount += bv.price * pv.qty;
        }
      });
    });
  }

  fetchCart() {
    this.loading = true;
    this.products = [];
    this.boms = [];

    this.cartProvider.getCart(this.userInfo.userName, this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.products = res.data.cartList;
          this.boms = res.data.cartBOMItems;

          this.getTotalAmount();
        }

        this.loading = false;
      });
  }

  backButtonHandler() {
    this.platform.registerBackButtonAction(() => {
      if (!this.alertIsOpen) {
        this.viewCtrl.dismiss();
      }
    });
  }
}

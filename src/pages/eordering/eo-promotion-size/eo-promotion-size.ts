import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-eo-promotion-size',
  templateUrl: 'eo-promotion-size.html',
})
export class EoPromotionSizePage {

  sizes: any[];
  btfCode: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private viewCtrl: ViewController) {

    this.sizes = this.navParams.get('sizes');
    this.btfCode = this.navParams.get('btfCode');
  }

  ionViewDidLoad() {
    
  }

  selectSize(size) {
    this.viewCtrl.dismiss({ size: size });
  }

  close() {
    this.viewCtrl.dismiss();
  }
}

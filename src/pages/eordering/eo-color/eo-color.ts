import { Component } from '@angular/core';
import { NavController, NavParams, Platform, App, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-eo-color',
  templateUrl: 'eo-color.html',
})
export class EoColorPage {

  colors: any[];
  colorFlag: any;
  size: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private platform: Platform,
              private app: App) {

    this.colors = this.navParams.get('colors');
    this.colorFlag = this.navParams.get('colorFlag');
    this.size = this.navParams.get('size');

    this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  selectColor(color) {
    this.viewCtrl.dismiss({ color: color });
  }

  close() {
    this.viewCtrl.dismiss();
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.viewCtrl.dismiss();
      });
    }
  }
}

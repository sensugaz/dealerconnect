import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController, App, ModalController, Events, Platform } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { StorageProvider } from '../../../providers/shared/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CartProvider } from '../../../providers/eordering/cart';
import { OrderProvider } from '../../../providers/eordering/order';
import * as moment from 'moment';
import { EoSummaryPage } from '../eo-summary/eo-summary';

@Component({
  selector: 'page-eo-checkout',
  templateUrl: 'eo-checkout.html',
})
export class EoCheckoutPage {

  mode: any;
  form: FormGroup;
  tabs: any;
  config: any;
  userInfo: any;
  customerInfo: any;
  products: any;
  boms: any;
  requestDateList: any[];
  shipToList: any[];
  transportList: any[];
  tVisible: boolean;
  transport: any = {};
  shipTo: any = {};
  btnLoading: boolean;
  totalQty: number;
  totalAmount: number;
  temps: {
    requestDateList: any[]
  };

  alertIsOpen: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private cartProvider: CartProvider,
              private orderProvider: OrderProvider,
              private formBuilder: FormBuilder,
              private configProvider: ConfigProvider,
              private alertCtrl: AlertController,
              private viewCtrl: ViewController,
              private modalCtrl: ModalController,
              private platform: Platform,
              private events: Events) {

    this.mode = this.navParams.get('mode');

    this.tabs = 0;
    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.totalQty = 0;
    this.tVisible = (this.customerInfo.shipCondition == '03') ? false : true;

    this.temps = {
      requestDateList: []
    };

    this.form = this.formBuilder.group({
      documentDate: moment().format(),
      customerPO: '',
      requestDate: '',
      shipTo: '',
      transport: (this.customerInfo.shipCondition == '03') ? this.customerInfo.transportZoneCode : '',
      isReceive: false,
      paymentTerm: [(this.customerInfo.paymentTerm == 'CASH') ? 'CASH' : '', Validators.required]
    });

    this.btnLoading = false;
    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.fetchCart();
    this.fetchOrder();
    this.fetchRequestDate();
  }

  next() {
    if (this.customerInfo.shipCondition == '03') {
      this.setTransport(this.form.value.transport);
    }

    this.tabs++;
  }

  prev() {
    this.tabs--;
  }

  setReceive() {
    /*
    this.form.patchValue({
      shipTo: '',
      transport: ''
    });

    this.shipTo = [];
    this.transport = [];
    */
  }

  setShipTo(shipId) {
    if (shipId != '') {
      let shipTo = this.shipToList.filter(item => item.shipId == shipId)[0];

      this.shipTo = shipTo;

      if (this.shipTo.shipCondition == '03') {
        this.form.patchValue({
          transport: this.shipTo.transportZone
        });

        this.setTransport(this.shipTo.transportZone);
      }
    } else {
      this.form.patchValue({
        transport: this.customerInfo.transportZoneCode
      });

      this.shipTo = {};
    }

    if (this.customerInfo.shipCondition == '03' && shipId == '') {
      this.tVisible = false;
    } else if (this.customerInfo.shipCondition == '03' && this.shipTo.shipCondition == '03') {
      this.tVisible = false;
    } else if (this.customerInfo.shipCondition != '03' && this.shipTo.shipCondition == '03') {
      this.tVisible = false;
    } else {
      this.tVisible = true;

      if (this.shipTo.shipCondition == '03') {
        this.tVisible = false;
      }
    }
  }

  setTransport(transportZone) {
    if (transportZone != '') {
      this.transport = this.transportList.filter(ts => ts.transportZone == transportZone)[0];
    } else {
      this.transport = {};
    }
  }

  addOrder(form) {
    let hour = Number(moment().format('H'));
    let requestDate = form.requestDate;
    
    if (form.requestDate == '' || form.requestDate == undefined) {
      if (hour >= 17) {
        if (this.userInfo.userTypeDesc == 'Multi') {
          if (moment(this.requestDateList[0].reqDate).isAfter(moment().toDate())) {
            requestDate = this.requestDateList[0].reqDate;
          } else {
            requestDate = this.requestDateList[1].reqDate;
          }
        } else {
          if (moment(this.requestDateList[0].reqDate).isAfter(moment().toDate())) {
            requestDate = this.requestDateList[0].reqDate;
          } else {
            requestDate = this.temps.requestDateList[0].reqDate;
          }
        }
      } else {
        requestDate = this.requestDateList[0].reqDate;
      }
    }

    let shipId = (this.isObjEmpty(this.shipTo) || form.isReceive) ? 0 : this.shipTo.shipId;
    let shipCode = (this.isObjEmpty(this.shipTo) || form.isReceive) ? '' : this.shipTo.shipCode;
    let shipName = (this.isObjEmpty(this.shipTo) || form.isReceive) ? 'รับสินค้าเอง' : this.shipTo.shipName;

    let shipCondition;
    let transportId;
    let transportZone;
    let transportZoneDesc;

    if (this.customerInfo.isReceive) {
      if (form.isReceive) {
        shipCondition = '01';
      } else {
        if (this.customerInfo.shipCondition == '03') {
          if (this.isObjEmpty(this.shipTo)) {
            shipCondition = this.customerInfo.shipCondition;
          } else {
            shipCondition = this.shipTo.shipCondition;
          }
        } else {
          shipCondition = '';
        }
      }
    } else {
      if (this.isObjEmpty(this.shipTo)) {
        shipCondition = this.customerInfo.shipCondition;
      } else {
        shipCondition = this.shipTo.shipCondition;
      }
    }

    if (shipCondition == '01') {
      transportId = 0;
      transportZone = '';
      transportZoneDesc = '';
    } else if (shipCondition == '03') {
      transportId = (this.isObjEmpty(this.transport)) ? 0 : this.transport.transportId;
      transportZone = (this.isObjEmpty(this.transport)) ? this.customerInfo.transportZone : this.transport.transportZone;
      transportZoneDesc = (this.isObjEmpty(this.transport)) ? this.customerInfo.transportZoneDesc : this.transport.transportZoneDesc;
    } else {
      if (!this.isObjEmpty(this.shipTo)) {
        transportId = 0;
        transportZone = this.shipTo.transportZone;
        transportZoneDesc = this.shipTo.transportZoneDesc;
      } else {
        if (!this.isObjEmpty(this.transport)) {
          transportId = this.transport.transport;
          transportZone = this.transport.transportZoneCode;
          transportZoneDesc = this.transport.transportZoneDesc;
        } else {
          transportId = 0;
          transportZone = this.customerInfo.transportZone;
          transportZoneDesc = this.customerInfo.transportZoneDesc;
        }
      }
    }

    let formData = {
      documentDate: form.documentDate,
      userName: this.userInfo.userName,
      customerId: this.customerInfo.customerId,
      customerName: this.customerInfo.customerName,
      customerCode: this.customerInfo.customerCode,
      paymentTerm: form.paymentTerm,
      shipCondition: shipCondition,
      shipId: shipId,
      shipCode: shipCode,
      shipName: shipName,
      requestDate: requestDate,
      customerPO: form.customerPO,
      transportId: transportId,
      transportZone: transportZone,
      transportZoneDesc: transportZoneDesc,
      isWeb: 'X'
    };

    this.alertIsOpen = true;

    this.alertCtrl.create({
      title: 'ยืนยัน',
      message: 'คุณต้องการยืนยันใบสั่งซื้อหรือไม่ ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            this.alertIsOpen = false;
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            this.btnLoading = true;

            if (this.config.shutdownSystem == 1) {
              this.alertCtrl.create({
                title: 'แจ้งเตือน',
                message : 'ไม่สามารถเข้าใช้งานได้เนื่องจากอยู่นอกเหนือเวลาทำการ',
                mode: 'ios',
                enableBackdropDismiss: false,
                buttons: [
                  {
                    text: 'ตกลง',
                    handler: () => {
                      this.alertIsOpen = false;
                    }
                  }
                ]
              }).present();

              this.btnLoading = false;
            } else {
              this.orderProvider.addOrder(formData)
                .then(res => {
                  if (res.result == 'SUCCESS') {
                    this.cartProvider.reloadCart();

                    this.alertCtrl.create({
                      title: 'สำเร็จ',
                      message: `ระบบดำเนินการสร้างใบสั่งซื้อ <br> เลขที่ <b>${res.data.order.documentNumber}</b> เรียบร้อยแล้ว`,
                      mode: 'ios',
                      enableBackdropDismiss: false,
                      buttons: [
                        {
                          text: 'ดูรายละเอียดสั่งซื้อ',
                          handler: () => {
                            let modal = this.modalCtrl.create(EoSummaryPage, { orderId: res.data.order.orderId });

                            modal.onDidDismiss(() => {
                              if (this.mode == undefined) {
                                this.navCtrl.popToRoot();
                              } else {
                                switch (this.mode) {
                                  case 'DIS':
                                    this.navCtrl.popToRoot()
                                      .then(() => this.navCtrl.first().dismiss());
                                    break;
                                  case 'POP':
                                    this.navCtrl.popToRoot()
                                      .then(() => this.navCtrl.first().getNav().pop())
                                      .then(() => {
                                        this.events.publish('product:close');
                                      });
                                    break;
                                  case 'TAB':
                                    this.navCtrl.popToRoot()
                                      .then(() => this.navCtrl.first().dismiss());
                                    break;
                                }
                              }
                            });

                            modal.present();
                          }
                        },
                        {
                          text: 'กลับสู่หน้าหลัก',
                          handler: () => {
                            if (this.mode == undefined) {
                              this.navCtrl.popToRoot();
                            } else {
                              switch (this.mode) {
                                case 'DIS':
                                  this.navCtrl.popToRoot()
                                    .then(() => this.navCtrl.first().dismiss());
                                  break;
                                case 'POP':
                                  this.navCtrl.popToRoot()
                                    .then(() => this.navCtrl.first().getNav().pop())
                                    .then(() => {
                                      this.events.publish('product:close');
                                    });
                                  break;
                                case 'TAB':
                                  this.navCtrl.popToRoot()
                                    .then(() => this.navCtrl.first().dismiss());
                                  break;
                              }
                            }
                          }
                        }
                      ]
                    }).present();
                  }

                  this.btnLoading = false;
                });
            }
          }
        }
      ]
    }).present();
  }

  delCart(product) {
    let index = this.products.indexOf(product);

    let formData = {
      cartList: [{
        userName: this.userInfo.userName,
        customerId: this.customerInfo.customerId,
        productId: product.productId,
        promotionId: 0
      }]
    };

    this.cartProvider.removeCart(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (this.products.length > 1) {
            this.products.splice(index, 1);
          } else {
            this.products = [];
            this.navCtrl.pop();
          }

          if (product.isBOM) {
            this.boms.forEach((value, key) => {
              if (product.productCode == value.productRefCode) {
                this.boms.splice(key, 1);
              }
            });
          }

          this.getTotalQty();
          this.getTotalAmount();
        }
      });
  }

  close() {
    this.navCtrl.pop();
  }

  fetchCart() {
    this.cartProvider.getCart(this.userInfo.userName, this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.products = res.data.cartList;
          this.boms = res.data.cartBOMItems;

          this.getTotalQty();
          this.getTotalAmount();
        }
      });
  }

  fetchOrder() {
    this.orderProvider.getOrderPrepare(this.customerInfo.customerId, this.userInfo.userName)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.requestDateList = res.data.requestDateList;
          this.shipToList = res.data.shipToList;
          this.transportList = res.data.transportList;
        }
      });
  }

  fetchRequestDate() {
    this.orderProvider.getRequestDate()
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.temps.requestDateList = res.data.requestDateList;
        }
      })
  }

  getTotalQty() {
    this.totalQty = this.products.filter(value => !value.isBOM).length + this.boms.length;
  }

  getTotalAmount() {
    this.totalAmount = 0;

    this.products.forEach(pv => {
      this.totalAmount += pv.totalAmount;

      this.boms.forEach(bv => {
        if (bv.productRefCode == pv.productCode) {
          this.totalAmount += bv.price * pv.qty;
        }
      });
    });
  }

  /**
   * Check object is empty
   * @param obj
   * @returns {boolean}
   */
  isObjEmpty(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }

    return true;
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alertIsOpen) {
          switch (this.tabs) {
            case 0:
              this.navCtrl.pop();
              break;
            case 1:
              this.tabs = 0;
          }
        }
      });
    }
  }
}

import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { NewsProvider } from '../../../providers/eordering/news';


@Component({
  selector: 'page-eo-news-detail',
  templateUrl: 'eo-news-detail.html',
})
export class EoNewsDetailPage {
  news: any;
  config: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private newsProvider: NewsProvider,
              private platform: Platform) {

    this.news = this.navParams.get('news');
    this.config = this.navParams.get('config');
    console.log(this.news);
    this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.viewCtrl.dismiss();
      });
    }
  }
}

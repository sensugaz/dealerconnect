import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, ViewController, Platform, App } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { ProductProvider } from '../../../providers/eordering/product';
import { EoProductDetailPage } from '../eo-product-detail/eo-product-detail';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-eo-product-search',
  templateUrl: 'eo-product-search.html',
})
export class EoProductSearchPage {
  customerInfo: any;

  products: any[];
  results: any[];
  search: string;
  loading: any;
  text: any;
  alertIsOpen: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private storageProvider: StorageProvider,
              private productProvider: ProductProvider,
              private barcodeScanner: BarcodeScanner,
              public loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private platform: Platform,
              private app: App) {

    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.viewCtrl.showBackButton(false);

    this.fetchProduct();
  }

  ionViewDidEnter() {
    
  }

  searchProduct() {
    this.results = this.products.filter(value => {
      return value.btfWebDescTh.includes(this.search.toUpperCase());
    });
  }

  selectProduct(product) {
    this.navCtrl.push(EoProductDetailPage, { product: product });
  }

  onCancel() {
    this.navCtrl.pop({ animate: false });
  }

  scanner(){
    this.barcodeScanner.scan()
      .then(res1 => {
        this.showLoading();

        this.productProvider.getProductByBarcode(res1.text)
          .then(res2 => {
            this.text = res2;

            if (res2.result == 'SUCCESS') {
              this.productProvider.getProductInBTF(this.customerInfo.customerId, res2.data.product[0].btfweb)
                .then(res3 => {
                  if (res3.result == 'SUCCESS') {
                    //this.text = res3.data.btfInfo;
                    this.navCtrl.push(EoProductDetailPage, { product: res3.data.btfInfo });
                  } else {
                    this.alertIsOpen = true;

                    this.alertCtrl.create({
                      title: 'แจ้งเตือน',
                      message: 'ไม่พบสินค้า',
                      mode: 'ios',
                      enableBackdropDismiss: false,
                      buttons: [
                        {
                          text: 'ตกลง',
                          handler: () => {
                            this.alertIsOpen = false;
                          }
                        }
                      ]
                    }).present();
                  }
                });
            } else {
              this.alertIsOpen = true;
              
              this.alertCtrl.create({
                title: 'แจ้งเตือน',
                message: 'ไม่พบสินค้า',
                mode: 'ios',
                enableBackdropDismiss: false,
                buttons: [
                  {
                    text: 'ตกลง',
                    handler: () => {
                      this.alertIsOpen = false;
                    }
                  }
                ]
              }).present();
            }

            this.hideLoading();
          })
      });
  }

  gotoHome(){
    this.navCtrl.pop();
  }

  fetchProduct() {
    this.productProvider.getProduct({ customerId: this.customerInfo.customerId })
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.products = res.data.productList;
        }
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        if (!this.alertIsOpen) {
          let nav = this.app.getActiveNavs()[0];
        
          this.hideLoading();

          if (nav.canGoBack()) {
            this.app.navPop();
          } else {
            this.navCtrl.pop();
          }
        }
      });
    }
  }
}

import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { CartProvider } from '../../../providers/eordering/cart';
import { EoPromotionFreegoodColorPage } from '../eo-promotion-freegood-color/eo-promotion-freegood-color';

@Component({
  selector: 'page-eo-promotion-freegood',
  templateUrl: 'eo-promotion-freegood.html',
})
export class EoPromotionFreegoodPage {
  promotionHD: any;
  promotionDT: any[];
  promotionDT_Sel: any[];
  promotionFG: any[];
  promotionFG_Sel: any[];
  freeGoods: any[];

  userInfo: any;
  customerInfo: any;

  btnLoading: boolean;
  sizeFreeGoods: any[];
  colorFreeGoods: any[];

  isSizeFreeGoods: any[];
  isColorFreeGoods: any[];

  productInFreeGoods: any[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private cartProvider: CartProvider,
              private storageProvider: StorageProvider,
              private modalCtrl: ModalController) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.promotionHD = this.navParams.get('promotionHD');
    this.promotionDT = this.navParams.get('promotionDT');
    this.promotionDT_Sel = this.navParams.get('promotionDT_Sel');
    this.promotionFG = this.navParams.get('promotionFG');
    this.promotionFG_Sel = this.navParams.get('promotionFG_Sel');
    this.freeGoods = this.navParams.get('freeGoods');
    this.sizeFreeGoods = this.navParams.get('sizeFreeGoods');
    this.colorFreeGoods = this.navParams.get('colorFreeGoods');
    this.isSizeFreeGoods = this.navParams.get('isSizeFreeGoods');
    this.isColorFreeGoods = this.navParams.get('isColorFreeGoods');
    this.productInFreeGoods = this.navParams.get('productInFreeGoods');

    console.log(this.promotionFG);

    this.btnLoading = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EoPromotionFreegoodPage');
  }

  addCart() {
    this.btnLoading = true;

    let cartList = [];
    let promotionList = [];

    this.promotionDT_Sel.forEach(value => {
      cartList.push({
        promotionDTId: value.promotionDtId,
        customerId: this.customerInfo.customerId,
        productId: value.productData.productId,
        qty: value.qty2,
        userName: this.userInfo.userName
      });
    });
    
    this.promotionFG_Sel.forEach(value => {
      if (value.qty > 0) {
        promotionList.push({
          promotionId: this.promotionHD.promotionId,
          freeGoodId: value.freeGoodsId,
          freeProductId: value.productFreeGoodsData.productId,
          qty: value.qty
        });
      }
    });

    let formData = {
      cartList: cartList,
      promotionList: promotionList
    };

    this.cartProvider.addCart(formData)
      .then(res => {
        this.btnLoading = false;
        
        if (res.result == 'SUCCESS') {
          this.alertCtrl.create({
            title: 'สำเร็จ',
            message: 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.navCtrl.pop()
                    .then(() => this.navCtrl.first().getNav().pop())
                }
              }
            ]
          }).present();
        }
      });
  }

  getIsSizeFreeGoods(promotionFG) {
    let size = {};
    let btf = promotionFG;

    size = this.isSizeFreeGoods.filter(item => {
      if (item.btfCode == btf.btfCode) {
        return item;
      }  
    });

    return size[0];
  }

  getIsColorFreeGoods(promotionFG) {
    let color = {};
    let btf = promotionFG;

    color = this.isColorFreeGoods.filter(item => {
      if (item.btfCode == btf.btfCode) {
        return item;
      }  
    });

    return color[0];
  }

  openModalSize(promotionFG) {
    
  }

  openModalColor(promotionFG) {
    let modal = this.modalCtrl.create(EoPromotionFreegoodColorPage, { 
      colors: this.colorFreeGoods, 
      size: this.getIsSizeFreeGoods(promotionFG),
      btfCode: promotionFG.btfCode
    });

    modal.present();

    modal.onDidDismiss(data => {
      if (data != undefined && data.color != undefined) {        
        let index = this.isColorFreeGoods.findIndex(item => {
          if (item.btfCode == promotionFG.btfCode) {
            return item;
          }
        });

        this.isColorFreeGoods[index] = data.color;

        this.setColorFreeGoods(promotionFG, data.color);
      }
    });
  }

  setColorFreeGoods(promotionFG, color) {
    let productInFreeGoods = this.productInFreeGoods.filter(item => {
      if (item.productCode  == color.productCode) {
        return item;
      }
    })[0];

    promotionFG.productFreeGoodsData = productInFreeGoods;
  }

  addFreeGoods(promotionFG) {
    let isExists = this.promotionFG_Sel.filter(item => {
      if (item.productFreeGoodsData.productCode == promotionFG.productFreeGoodsData.productCode) {
        return item;
      }
    })[0] != undefined;

    let productForm = Object.assign({}, promotionFG);

    if (!this.promotionHD.checkStepBySKU) {
      let qty = 0;
      let frg = this.promotionFG_Sel.filter(item => {
        if (item.freeGoodsId == promotionFG.freeGoodsId) {
          return item;
        }
      });

      if (frg.length > 0) {
        frg.forEach(value => {
          qty += parseInt(value.qty);
        });

        if ((qty + promotionFG.qty) > promotionFG.qty2) {
          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            message: 'จำนวนแถมเกินจำนวนแถมที่ได้รับ',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  //this.alertIsOpen = false;
                }
              }
            ]
          }).present();

          return false;
        }
      }

      if (!isExists) {
        this.promotionFG_Sel.push(productForm);
      } else {
        let index = this.promotionFG_Sel.findIndex(item => {
          if (item.freeGoodsMatId == promotionFG.freeGoodsMatId) {
            return item;
          }
        });

        this.promotionFG_Sel[index] = productForm;
      }
    }
  }

  delFreeGoods(promotionFG) {
    let index = this.promotionFG_Sel.indexOf(promotionFG);

    this.promotionFG_Sel.splice(index, 1);
  }

  getTotalQty() {
    let qty = 0;

    if (this.promotionFG_Sel.length > 0) {
      this.promotionFG_Sel.forEach(value => {
        qty += value.qty2;
      });
    }

    return qty;
  }

  incQty(promotionFG) {
    if (promotionFG.qty < promotionFG.qty2) {
      let qty = 1;

      switch(promotionFG.productFreeGoodsData.altUnit1Code) {
        case 'BOX':
          qty = promotionFG.productFreeGoodsData.altUnit1Amount;
          break;
        default:
          qty = 1;
          break;
      } 

      promotionFG.qty = parseInt(promotionFG.qty) +qty;
    }
  }

  decQty(promotionFG) {
    let qty = 1;

    switch(promotionFG.productFreeGoodsData.altUnit1Code) {
      case 'BOX':
        qty = promotionFG.productFreeGoodsData.altUnit1Amount;
        break;
      default:
        qty = 1;
        break;
    } 

    if (promotionFG.qty > qty) {
      promotionFG.qty -= qty;
    }
  }

  edtQty(promotionFG) {
    if (promotionFG.qty == 0 || promotionFG.qty == undefined) {
      switch(promotionFG.productFreeGoodsData.altUnit1Code) {
        case 'BOX':
          promotionFG.qty = promotionFG.productFreeGoodsData.altUnit1Amount;
          break;
        default:
          promotionFG.qty = 1;
          break;
      }
    } else {
      if (promotionFG.qty < promotionFG.productFreeGoodsData.altUnit1Amount) {
        promotionFG.qty = promotionFG.productFreeGoodsData.altUnit1Amount;
      } else if (promotionFG.qty % promotionFG.productFreeGoodsData.altUnit1Amount) {
        promotionFG.qty = promotionFG.productFreeGoodsData.altUnit1Amount * (promotionFG.qty / promotionFG.productFreeGoodsData.altUnit1Amount);
      }
    }
  }

  goBack() {
    this.navCtrl.pop();
  }
}

import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, Platform, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerProvider } from '../../../providers/eordering/customer';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoMenuPage } from '../eo-menu/eo-menu';

@Component({
  selector: 'page-eo-problem',
  templateUrl: 'eo-problem.html',
})
export class EoProblemPage {

  form: FormGroup;
  userInfo: any;
  customerInfo: any;
  btnLoading: boolean;
  alertIsOpen: boolean = true;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private storageProvider: StorageProvider,
              private customerProvider: CustomerProvider,
              private alertCtrl: AlertController,
              private platform: Platform,
              private app: App) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.form = this.formBuilder.group({
      message: ['', Validators.required],
    });

    this.btnLoading = false;

    //this.backButtonHandler();
  }

  ionViewDidLoad() {

  }

  gotoHome() {
    this.navCtrl.popToRoot();
  }

  sendProblem(form) {
    this.btnLoading = true;

    let formData = {
      customerId: this.customerInfo.customerId,
      customerCode: this.customerInfo.customerCode,
      customerName: this.customerInfo.customerName,
      problemText: form.message,
      userName: this.userInfo.userName,
    };

    this.customerProvider.problem({ problem: formData })
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.alertIsOpen = true;

          this.alertCtrl.create({
            title: 'สำเร็จ',
            message: 'ทำการแจ้งปัญหาเรียบร้อย',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                  this.alertIsOpen = false;
                }
              }
            ]
          }).present();

          this.form.reset();
        };

        this.btnLoading = false;
      });
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        /*
        this.app.getRootNav().setRoot(EoTabsPage, {}, {
          animate: true,
          animation: 'ease-in'
        });
        */
        if (!this.alertIsOpen) {
          this.navCtrl.pop();
        }
      });
    }
  }
}

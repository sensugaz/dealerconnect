import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, App, Platform } from 'ionic-angular';

@Component({
  selector: 'page-eo-size',
  templateUrl: 'eo-size.html',
})
export class EoSizePage {

  sizes: any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private platform: Platform,
              private app: App) {

    this.sizes = this.navParams.get('sizes');

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EoSizePage');
  }

  selectSize(size) {
    this.viewCtrl.dismiss({ size: size });
  }

  close() {
    this.viewCtrl.dismiss();
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.viewCtrl.dismiss();
      });
    }
  }
}

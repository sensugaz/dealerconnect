import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, Platform, App } from 'ionic-angular';
import { NewsProvider } from '../../../providers/eordering/news';
import { ConfigProvider } from '../../../providers/eordering/config';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoNewsDetailPage } from '../eo-news-detail/eo-news-detail';
import { EoMenuPage } from '../eo-menu/eo-menu';
import { EoTabsPage } from '../eo-tabs/eo-tabs';

@Component({
  selector: 'page-eo-news-list',
  templateUrl: 'eo-news-list.html',
})
export class EoNewsListPage {

  config: any;
  newsHD: any[];
  newsDT: any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private newsProvider: NewsProvider,
              private modalCtrl: ModalController,
              private storageProvider: StorageProvider,
              private platform: Platform,
              private app: App) {

    this.config = this.storageProvider.getConfig();

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.fetchNews();
  }

  openNews(news) {
    let modal = this.modalCtrl.create(EoNewsDetailPage, {
      news: this.newsDT.filter(item => item.newsId == news.newsId),
      config: this.config
    });

    modal.present();

    modal.onDidDismiss(() => {
      this.backButtonHandler();
    });
  }

  fetchNews() {
    this.newsProvider.getNews()
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.newsHD = res.data.newsActivityHDList;
          this.newsDT = res.data.newsActivityDTList;
        }
      });
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.app.getRootNav().setRoot(EoTabsPage, { page: 4 }, {
          animate: true,
          animation: 'ease-in'
        });
      });
    }
  }
}

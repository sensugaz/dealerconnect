import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, PopoverController, App, Platform, MenuController, Events } from 'ionic-angular';
import { EoProductFilterPage } from '../eo-product-filter/eo-product-filter';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoProductSearchPage } from '../eo-product-search/eo-product-search';
import { EoMenuPage } from '../eo-menu/eo-menu';
import { Eordering } from '../../../app/modules/eordering/eordering.componet';
import { ConfigProvider } from '../../../providers/eordering/config';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { EoProfilePage } from '../eo-profile/eo-profile';
import { EoProfileUpdatePage } from '../eo-profile-update/eo-profile-update';
import { EoContactPage } from '../eo-contact/eo-contact';
import { EoProblemPage } from '../eo-problem/eo-problem';
import { EoProductDetailPage } from '../eo-product-detail/eo-product-detail';
import { EoStorePage } from '../eo-store/eo-store';

@Component({
  selector: 'page-eo-main',
  templateUrl: 'eo-main.html',
})
export class EoMainPage {
  config: any;
  userInfo: any;
  customerInfo: any;
  page: number;
  menus: any;
  title: string;

  permissions: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalCtrl: ModalController,
              private storageProvider: StorageProvider,
              private popoverCtrl: PopoverController,
              private configProvider: ConfigProvider,
              private viewCtrl: ViewController,
              private menuCtrl: MenuController,
              private platform: Platform,
              private app: App,
              private events: Events) {

    this.config = {};
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.permissions = this.storageProvider.getEoMenuPermission();
    //this.page = 0;
    this.page = this.navParams.get('page');
    this.page = (this.page == undefined) ? 0 : this.page;
    
    this.menus = [
      {
        id: 0,
        name: this.verifyPermission('01100').menuName,
        icon: './assets/icon/eOrdering/home.png',
        code: '01100'
      },
      {
        id: 1,
        name: this.verifyPermission('01200').menuName,
        icon: './assets/icon/eOrdering/order.png',
        code: '01200'
      },
      {
        id: 2,
        name: this.verifyPermission('01300').menuName,
        icon: './assets/icon/eOrdering/fav.png',
        code: '01300'
      },
      {
        id: 3,
        name: this.verifyPermission('01400').menuName,
        icon: './assets/icon/eOrdering/history.png',
        code: '01400'
      },
      {
        id: 4,
        name: this.verifyPermission('01500').menuName,
        icon: './assets/icon/eOrdering/news.png',
        code: '01500'
      },
      {
        id: 5,
        name: this.verifyPermission('01600').menuName,
        icon: './assets/icon/eOrdering/doc.png',
        code: '01600'
      },
      {
        id: 6,
        name: this.verifyPermission('01700').menuName,
        icon: './assets/icon/eOrdering/report.png',
        code: '01700'
      }
    ];
    
    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.events.subscribe('product:open', (data) => {
      console.log('==>', data);
      this.navCtrl.push(EoProductDetailPage, { product: data.product, mode: data.mode });
    });
  }

  verifyPermission(menuCode) {
    let menu = this.permissions.filter(item => item.menuCode == menuCode && item.privilegeId != 0);

    return (menu != undefined) ? (menu.length) ? menu[0] : false : true;
  }

  selectedPage(index) {
    this.page = index;
    this.title = this.menus[index].name;
  }

  openProductFilter() {
    let modal = this.modalCtrl.create(EoProductFilterPage);

    modal.present();

    modal.onDidDismiss(() => {
      this.backButtonHandler();
    });
  }

  openSearchProduct() {
    this.navCtrl.push(EoProductSearchPage, {}, { animate: false });
  }

  openMenu() {
    //this.menuCtrl.enable(true);
    //this.menuCtrl.open();
    this.navCtrl.push(EoMenuPage);
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.app.navPop();
        } else {
          if (this.userInfo.userTypeDesc == 'Multi') {
            this.app.getRootNav().setRoot(EoStorePage);
          } else {
            this.app.getRootNav().setRoot(DealerConnect, { action: 'NP' });
          }
        }
      });
    }
  }
}
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, PopoverController, MenuController, Events, Platform, Nav, App } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { StorageProvider } from '../../../providers/shared/storage';
import { EoTabsPage } from '../eo-tabs/eo-tabs';
import { EoStorePage } from '../eo-store/eo-store';
import { EoProfilePage } from '../eo-profile/eo-profile';
import { EoContactPage } from '../eo-contact/eo-contact';
import { EoProfileUpdatePage } from '../eo-profile-update/eo-profile-update';
import { EoProblemPage } from '../eo-problem/eo-problem';

@Component({
  selector: 'page-eo-menu',
  templateUrl: 'eo-menu.html',
})
export class EoMenuPage {
  config: any;
  rootPage: any;
  permissions: any;
  userInfo: any;
  customerInfo: any;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private menuCtrl: MenuController,
              private popoverCtrl: PopoverController,
              private storageProvider: StorageProvider,
              private platform: Platform,
              private app: App,
              private events: Events) {
    
    this.config = this.storageProvider.getConfig();
    this.permissions = this.storageProvider.getEoMenuPermission();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
  
    this.rootPage = EoTabsPage;

    //this.backButtonHandler();
  }
  
  ionViewDidLoad() {

  }

  verifyPermission(menuCode) {
    let menu = this.permissions.filter(item => item.menuCode == menuCode && item.privilegeId != 0);

    return (menu != undefined) ? (menu.length) ? menu[0] : false : true;
  }

  selectedMenu(menu) {
    if (menu != undefined) {
      switch (menu) {
        case 1:
          this.navCtrl.push(EoProfilePage);
          break;
        case 2:
          this.navCtrl.push(EoContactPage);
          break;
        case 3:
          this.navCtrl.push(EoProfileUpdatePage);
          break;
        case 4:
          this.navCtrl.push(EoProblemPage);
          break;
      }
    }
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.popToRoot();
      });
    }
    /*
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        //this.navCtrl.pop();
        this.app.getActiveNav().popToRoot();
      });
    }
    */
  }
}

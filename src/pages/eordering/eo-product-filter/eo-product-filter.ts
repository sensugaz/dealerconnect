import { ChangeDetectorRef, Component } from '@angular/core';
import { App, NavController, NavParams, Platform, ViewController, Loading, LoadingController } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { MarketingProvider } from '../../../providers/eordering/marketing';
import { EoProductListPage } from '../eo-product-list/eo-product-list';
import { EoMainPage } from '../eo-main/eo-main';

@Component({
  selector: 'page-eo-product-filter',
  templateUrl: 'eo-product-filter.html',
})
export class EoProductFilterPage {
  customerInfo: any;

  marketings: any[];
  brands: any[];
  types: any[];

  marketingCodes: any[];
  brandCodes: any[];
  typeCodes: any[];

  loading: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private storageProvider: StorageProvider,
              private marketingProvider: MarketingProvider,
              private cdRef: ChangeDetectorRef,
              private platform: Platform,
              private loadingCtrl: LoadingController,
              private app: App) {

    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.marketings = [];
    this.brands = [];
    this.types = [];

    this.marketingCodes = [];
    this.brandCodes = [];
    this.typeCodes = [];

    if (platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        let nav = app.getActiveNavs()[0];

        if (nav.canGoBack()) {
          this.navCtrl.pop();
        } else {
          this.viewCtrl.dismiss();
        }
      }, 0);
    }
  }

  ionViewDidLoad() {
    this.fetchMarketing();
  }

  onDismiss() {
    this.viewCtrl.dismiss();
  }

  onSelectMarketing(marketingCode) {
    let index = this.marketingCodes.indexOf(marketingCode);
    let marketing = this.marketings.filter(value => value.marketingCode == marketingCode);

    if (index > -1) {
      this.marketingCodes.splice(index, 1);

      if (marketing.length > 0) {
        marketing.forEach(value => {
          value.checked = false;
        });
      } else {
        marketing[0].checked = false;
      }
    } else {
      this.marketingCodes.push(marketingCode);

      if (marketing.length > 0) {
        marketing.forEach(value => {
          value.checked = true;
        });
      } else {
        marketing[0].checked = true;
      }
    }

    this.clearBrandFilter();
  }

  onSelectBrand(brandCode) {
    let index = this.brandCodes.indexOf(brandCode);
    let brand = this.brands.filter(value => value.brandCode == brandCode);

    if (index > -1) {
      this.brandCodes.splice(index, 1);

      if (brand.length > 0) {
        if (brand.length > 0) {
          brand.forEach(value => {
            value.checked = false;
          });
        } else {
          brand[0].checked = false;
        }
      }
    } else {
      this.brandCodes.push(brandCode);

      if (brand.length > 0) {
        brand.forEach(value => {
          value.checked = true;
        });
      } else {
        brand[0].checked = true;
      }
    }
  }

  onSelectType(typeCode) {
    let index = this.typeCodes.indexOf(typeCode);
    let type = this.types.filter(value => value.typeCode == typeCode);

    if (index > -1) {
      this.typeCodes.splice(index, 1);

      if (type.length > 0) {
        type.forEach(value => {
          value.checked = false;
        })
      } else {
        type[0].checked = false;
      }
    } else {
      this.typeCodes.push(typeCode);

      if (type.length > 0) {
        type.forEach(value => {
          value.checked = true;
        })
      } else {
        type[0].checked = true;
      }
    }
  }

  clearMarketingFilter() {
    this.cdRef.detectChanges();

    if (this.marketings.length > 0) {
      this.marketings.forEach(value => {
        if (value.checked) {
          value.checked = false;
        }
      });

      this.marketingCodes.forEach((value, key) => {
        this.marketingCodes.slice(key, 1);
      });

      this.clearBrandFilter();
    }
  }

  clearBrandFilter() {
    this.cdRef.detectChanges();

    if (this.brands.length > 0) {
      this.brands.forEach(value => {
        if (value.checked) {
          value.checked = false;
        }
      });

      this.brandCodes.forEach((value, key) => {
        this.brandCodes.slice(key, 1);
      });
    }
  }

  clearTypeFilter() {
    this.cdRef.detectChanges();

    if (this.types.length > 0) {
      this.types.forEach(value => {
        if (value.checked) {
          value.checked = false;
        }
      });

      this.typeCodes.forEach((value, key) => {
        this.typeCodes.slice(key, 1);
      });
    }
  }

  filterProduct() {
    this.navCtrl.push(EoProductListPage, {
      marketingCodes: this.marketingCodes,
      brandCodes: this.brandCodes,
      typeCodes: this.typeCodes
    });
  }

  fetchMarketing() {
    this.showLoading();

    this.marketingProvider.getMarketing(this.customerInfo.customerId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.marketings = res.data.marketingList;
          this.brands = res.data.brandList;
          this.types =  res.data.typeList;

          this.marketings.forEach(value => {
            value.checked = false;
          });

          this.brands.forEach(value => {
            value.checked = false;
          });

          this.types.forEach(value => {
            value.checked = false;
          });

          this.hideLoading();
        }
      });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  onGotoHome() {
    this.navCtrl.pop();
  }
}
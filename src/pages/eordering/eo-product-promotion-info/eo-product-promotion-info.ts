import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';


@Component({
  selector: 'page-eo-product-promotion-info',
  templateUrl: 'eo-product-promotion-info.html',
})
export class EoProductPromotionInfoPage {

  config: any;
  btfInfo: any;
  promotions: any[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private platform: Platform) {

    this.config = this.navParams.get('config');
    this.btfInfo = this.navParams.get('btfInfo');
    this.promotions = this.navParams.get('promotions');

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    
  }

  close() {
    this.viewCtrl.dismiss();
  }

  backButtonHandler() {
    if (this.platform.is('android')) {
      this.platform.registerBackButtonAction(() => {
        this.viewCtrl.dismiss();
      });
    }
  }
}

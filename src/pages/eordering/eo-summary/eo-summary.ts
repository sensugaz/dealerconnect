import { Component } from '@angular/core';
import { App, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { EoTabsPage } from '../eo-tabs/eo-tabs';
import { StorageProvider } from '../../../providers/shared/storage';
import { OrderProvider } from '../../../providers/eordering/order';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { EoStorePage } from '../eo-store/eo-store';
import { EoMenuPage } from '../eo-menu/eo-menu';

@Component({
  selector: 'page-eo-summary',
  templateUrl: 'eo-summary.html',
})
export class EoSummaryPage {

  orderId: any;
  userInfo: any;
  customerInfo: any;
  config: any;

  order: any;

  products: any[];
  boms: any[];
  totalQty: number;
  totalAmount: number;
  shipTo: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storageProvider: StorageProvider,
              private orderProvider: OrderProvider,
              private platform: Platform,
              private viewCtrl: ViewController,
              private app: App) {

    this.orderId = this.navParams.get('orderId');
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.config = this.storageProvider.getConfig();
    this.totalQty = 0;
    this.totalAmount = 0;

    this.order = {};
    this.shipTo = {};

    this.backButtonHandler();
  }

  ionViewDidLoad() {
    this.fetchOrder();
  }

  gotoHome() {
    //this.navCtrl.setRoot(EoMenuPage);
    this.viewCtrl.dismiss();
  }

  fetchOrder() {
    this.orderProvider.getOrderInfo(this.orderId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.order = res.data.order;
          this.products = res.data.orderDetailList;
          this.boms = res.data.orderBOMItems;

          this.totalQty = this.products.filter(value => !value.isBOM).length + this.boms.length;
          this.totalAmount = 0;

          this.products.forEach(pv => {
            if (!pv.isBOM) {
              this.totalAmount += pv.totalAmount;
            }
            
            this.boms.forEach(bv => {
              if (bv.productRefCode == pv.productCode) {
                this.totalAmount += bv.price * pv.qty;
              }
            });
          });
          

          this.fetchShipTo();
        }
      });
  }

  fetchShipTo() {
    this.orderProvider.getOrderPrepare(this.customerInfo.customerId, this.userInfo.userName)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (this.order.shipId != '') {
            this.shipTo = res.data.shipToList.filter(value => value.shipId == this.order.shipId)[0];
          }
        }
      });
  }
  
  backButtonHandler() {
    this.platform.registerBackButtonAction(() => {
      this.viewCtrl.dismiss();
    });
  }
}

import { Component } from '@angular/core';
import { NavController, NavParams ,Platform} from 'ionic-angular';
import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  selector: 'page-ep-bank-branch',
  templateUrl: 'ep-bank-branch.html',
})
export class EpBankBranchPage {
  BankCode: string;
  BrankBranch: string;
  BankBranchList : Array<any>;
  BankBranchList_Full : Array<any>;
  alertConfirm : any = this.alertCtrl.create();
  alertStatus : boolean = false;
 

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl : AlertController,
    private epDealerProvider: EPDealerProvider,
    private platform: Platform) {

    this.BankCode = this.navParams.get('BankCode');
  }

  ionViewDidEnter(){
    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (this.alertStatus) {
          this.alertStatus = false;
          this.alertConfirm.dismiss();
        } else {
          this.navCtrl.pop();
        }
      });
    });
   }

  ionViewDidLoad() {
    this.fatchBankBranch();
  }

  fatchBankBranch() {
    if (this.BankCode != '') {
      let obj = {
        BankCode: this.BankCode
      };
      this.epDealerProvider.getBankBranchByBankCode(obj)
        .then(res => {
          this.BankBranchList = res.data.bankbranchlist;
          this.BankBranchList_Full = this.BankBranchList;

          this.checkDataEmpty();
        }), err => {
          console.log(err);
        }
    }
  }

  checkDataEmpty() {
    let DataIsEmpty = this.BankBranchList == undefined || this.BankBranchList == null || this.BankBranchList.length == 0;

    if (DataIsEmpty) {
      this.alertStatus = true;

      this.alertConfirm = this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบสาขาธนาคาร',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.alertStatus = false;
              this.navCtrl.pop();
            }
          }]
      });
      this.alertConfirm.present();
    }
  }

  onSelectBankBranch(item){
    this.navCtrl.pop().then(() => this.navParams.get('bankBranchObject')(item))
  }

  getItems(ev: any) {
    let val = ev.target.value;
    
    if (val && val.trim() != '') {
      this.BankBranchList = this.BankBranchList_Full.filter((item =>{
        return item.branchname.toLowerCase().indexOf(val.toLowerCase()) > -1;
      }))
    }else{
      this.BankBranchList = this.BankBranchList_Full;
    }
  }

}

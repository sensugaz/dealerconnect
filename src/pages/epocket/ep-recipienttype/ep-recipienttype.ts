import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-ep-recipienttype',
  templateUrl: 'ep-recipienttype.html',
})
export class EpRecipienttypePage {
  DataList: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.fatchData();
  }

  fatchData(){
    this.DataList.push({value:"03",recipientname:"บุคคลธรรมดา"},{value:"53",recipientname:"นิติบุคคล"});
  }

  onSelectRecipient(item){
    this.navCtrl.pop().then(() => this.navParams.get('RecipientObject')(item))
  }
}

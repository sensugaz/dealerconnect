import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { AlertController,LoadingController,NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPProductProvider } from '../../../providers/epocket/product';
import { CalendarComponentOptions, CalendarModal } from 'ion2-calendar';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { EpDealerHistoryInfoPage } from '../ep-dealer-history-info/ep-dealer-history-info';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'page-ep-dealer-history',
  templateUrl: 'ep-dealer-history.html',
})
export class EpDealerHistoryPage {
 
  loading: any;
  config: any;
  userInfo: any;
  customerInfo: any;
  dealerID: string;

  HistoryHD: any[];
  HistoryInfo: any;

  dateRange: { from: string; to: string; };
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsRange: CalendarComponentOptions = {
    pickMode: 'range'
  };

  startDate: any;
  endDate: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              public epProductProvider: EPProductProvider,
              public epDealerProvider: EPDealerProvider,
              public alertCtrl: AlertController,
              // private toastCtrl: ToastController,
              private modalCtrl: ModalController) {

      this.config = this.storageProvider.getConfig();
      this.userInfo = this.storageProvider.getUserInfo();
      this.customerInfo = this.storageProvider.getCustomerInfo();

      //console.log('owen test ', this.customerInfo);
      //console.log('Owen Test >>' + this.customerInfo);

      this.dealerID = this.customerInfo.customerCode;

      //console.log('DealerID >> ',this.dealerID);

      // Old
      this.startDate = moment().toDate(); 
      this.endDate = moment().toDate();

      // New
      // this.startDate = moment().format('DD-MM-YYYY'); 
      // this.endDate = moment().add('days', 7).format('DD-MM-YYYY');
  }

  ionViewDidLoad() {
    // console.log('Owen');
    this.fetchRedeemHistory();
  }

  onOpenStartDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.startDate = date.dateObj;
        //this.startDate = moment(date.dateObj).format('DD-MM-YYYY');
      }
    });
  }

  onOpenEndDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.endDate).toDate(),
        from: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        // this.endDate = moment(date.dateObj).format('DD-MM-YYYY');
        this.endDate = date.dateObj ;
      }
    });
  }

  fetchRedeemHistory() {

    this.showLoading();

    // this.startDate = moment(this.startDate).format('YYYY-MM-DD');
    // this.endDate = moment(this.endDate).format('YYYY-MM-DD');

    let DataInfo = {
		  BeginDate: moment(this.startDate).format('YYYY-MM-DD') ,
      EndDate: moment(this.endDate).format('YYYY-MM-DD'),
      DealerID: String(this.dealerID)
    };

    this.epDealerProvider.getRedeemHistory(DataInfo)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.HistoryHD = res.data.redeemHistoryHD;
          //console.log(res.data);
        }
        this.hideLoading();
      });
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

  onSelected(refdocno){
    //console.log(refdocno);
    this.navCtrl.push(EpDealerHistoryInfoPage,{ refdocno : refdocno }, {
			animate: true,
			animation: 'ease-in'
		});
  }

}

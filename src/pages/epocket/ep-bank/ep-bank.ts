import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EPSaleProvider } from './../../../providers/epocket/sale';


@Component({
  selector: 'page-ep-bank',
  templateUrl: 'ep-bank.html',
})
export class EpBankPage {
  BankList_Full : Array<any>;
  BankList: Array<any>;
  items: string[];
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private epSaleProvider: EPSaleProvider
  ) {
  }

  ionViewDidLoad() {
    this.fatchBank();
  }
 

  fatchBank() {
    this.epSaleProvider.getBank({})
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.BankList = res.data.banklist;
          this.BankList_Full = this.BankList;
        }
      });
  }

  onSelectBank(item){
   
    this.navCtrl.pop().then(() => this.navParams.get('bankObject')(item))
  }

  getItems(ev: any) {
    let val = ev.target.value;
    
    if (val && val.trim() != '') {
      this.BankList = this.BankList_Full.filter((item =>{
        return item.bankname.toLowerCase().indexOf(val.toLowerCase()) > -1;
      }))
    }else{
      this.BankList = this.BankList_Full;
    }
  }

}

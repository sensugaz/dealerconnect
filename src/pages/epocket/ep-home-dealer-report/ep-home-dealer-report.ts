import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { EpChangeprofileHistoryMainPage } from './../ep-changeprofile-history-main/ep-changeprofile-history-main';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { EpRpt01DealerexchangecoverPage } from '../ep-report/ep-rpt01-dealerexchangecover/ep-rpt01-dealerexchangecover';
import { EpRpt02DealerredeempainterPage } from '../ep-report/ep-rpt02-dealerredeempainter/ep-rpt02-dealerredeempainter';
import { EpRpt03DealerredeembrandtypePage } from '../ep-report/ep-rpt03-dealerredeembrandtype/ep-rpt03-dealerredeembrandtype';
import { EpRpt04DealerredeemhistoryPage } from '../ep-report/ep-rpt04-dealerredeemhistory/ep-rpt04-dealerredeemhistory';
import { EpRpt05CoinstockdealerPage } from '../ep-report/ep-rpt05-coinstockdealer/ep-rpt05-coinstockdealer';
import * as moment from 'moment';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPDealerProvider } from '../../../providers/epocket/dealer';

@Component({
  selector: 'page-ep-home-dealer-report',
  templateUrl: 'ep-home-dealer-report.html',
})
export class EpHomeDealerReportPage {
  startDate: any;
  endDate: any;
  customerInfo: any;
  DEALER_ID: string;
  DataReport: any;
  tpMenu: Array<any> = [
    { // report 01 new 
      icon: './assets/icon/ePocket_icon/EpRpt01Dealerexchangecover.png',
      action: EpRpt01DealerexchangecoverPage
    }, { // *ngFor sub menu
      icon: './assets/icon/ePocket_icon/ChangeProfileHistory.png',
      action: EpChangeprofileHistoryMainPage
    }, {
      icon: './assets/icon/ePocket_icon/EpRpt02DealerRedeemPainter.png',
      action: EpRpt02DealerredeempainterPage
    }, {
      icon: './assets/icon/ePocket_icon/EpRpt03Dealerredeembrandtype.png',
      action: EpRpt03DealerredeembrandtypePage
    }, {
      icon: './assets/icon/ePocket_icon/EpRpt04DealerRedeemHistory.png',
      action: EpRpt04DealerredeemhistoryPage
    }, {
      icon: './assets/icon/ePocket_icon/EpRpt05CoinStock.png',
      action: EpRpt05CoinstockdealerPage
    }
  ];

  alertConfirmPassword : any = this.alertCtrl.create();
  alertPresented: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private platform: Platform,
    private storageProvider: StorageProvider,
    private epDealerProvider: EPDealerProvider,
    private alertCtrl: AlertController) {

    this.startDate = moment().subtract(1, 'months').toDate();
    this.endDate = moment().toDate();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;
  }

  ionViewDidEnter() {
    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (this.alertPresented) {
          this.alertPresented = false;
          this.alertConfirmPassword.dismiss();
        } else {
          this.navCtrl.pop();
        }
      });
    });
   }


  ionViewDidLoad() {
  }


  onSelected(menu) {
    if (menu.action.name == "EpRpt01DealerexchangecoverPage") {
      this.fatchReport01(menu);
    } else if (menu.action.name == "EpRpt02DealerredeempainterPage") {
      this.fatchReport02(menu);
    } else if (menu.action.name == "EpRpt03DealerredeembrandtypePage") {
      this.fatchReport03(menu);
    } else if (menu.action.name == "EpRpt04DealerredeemhistoryPage") {
      this.fatchReport04(menu);
    } else if (menu.action.name == "EpRpt05CoinstockdealerPage") {
      this.fatchReport05(menu);
    } else if(menu.action.name == "EpChangeprofileHistoryMainPage"){
      this.navCtrl.push(menu.action, {
        animate: true,
        animation: 'ease-in'
      });
    }
  }

  fatchReport01(menu) {
    let paraObject = {
      DEALER_ID: this.DEALER_ID,
      FROM_DATE: moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE: moment(this.endDate).format('DD-MM-YYYY')
    };

    this.epDealerProvider.getRPT01DelaerExchangeCover(paraObject)
      .then(res => {
        this.DataReport = res.data.exchangeCover;
        this.checkDataEmpty(menu);
      }), err => {
        console.log(err);
      }
  }

  fatchReport02(menu) {
    let paraObject = {
      DEALER_ID: this.DEALER_ID,
      FROM_DATE: moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE: moment(this.endDate).format('DD-MM-YYYY'),
      PAINTER_ID: "0"
    };

    this.epDealerProvider.getRPT02DealerRedeemByPainter(paraObject)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerRedeemPainter;
          this.checkDataEmpty(menu);
        }
      }), err => {
        console.log(err);
      }
  }

  fatchReport03(menu) {
    let paraObject = {
      DEALER_ID: this.DEALER_ID,
      FROM_DATE: moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE: moment(this.endDate).format('DD-MM-YYYY')
    };

    this.epDealerProvider.getRPT03DealerRedeemBT(paraObject)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerRedeemBT;
          this.checkDataEmpty(menu);
        }
      }), err => {
        console.log(err);
      }
  }

  fatchReport04(menu) {
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE :  moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY')
    };

    this.epDealerProvider.getRPT04DealerRedeemHistory(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerRedeemHistory;
          this.checkDataEmpty(menu); 
        }
      }),err =>{
        console.log(err);
      }
  }

  fatchReport05(menu) {
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE : "",
      TO_DATE : ""
    };

    this.epDealerProvider.getRPT05CoinStock(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.coinStock;
          this.checkDataEmpty(menu); 
        }
      }),err =>{
        console.log(err);
      }
  }

  checkDataEmpty(menu) {
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;

    if (DataReportIsEmpty) {
      this.alertConfirmPassword = this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
            }
          }]
      });

      this.alertConfirmPassword.present();
      this.alertPresented = true;

    } else {
      this.navCtrl.push(menu.action, {
        animate: true,
        animation: 'ease-in'
      });
    }
  }
}


import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EPDealerProvider } from '../../../providers/epocket/dealer';

@Component({
  selector: 'page-ep-vendordistrict',
  templateUrl: 'ep-vendordistrict.html',
})
export class EpVendordistrictPage {

  DisList : Array<any>;
  DisList_Full : Array<any>;
  CityCode : string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public epDealerProvider : EPDealerProvider) {

             this.CityCode =  this.navParams.get('CityCode');
  }

  ionViewDidLoad() {
    this.fatchData();
  }

  fatchData() {
    let obj = {
      CityCode : this.CityCode
    }
    this.epDealerProvider.getVendorDistrict(obj)
      .then(res => {
        this.DisList = res.data.districtlist;
        this.DisList_Full = this.DisList;
      }), err => {
        console.log(err);
      }
  }

  onSelectDistrict(item){
    this.navCtrl.pop().then(() => this.navParams.get('DistricObject')(item));
  }

  getItems(ev: any) {
    let val = ev.target.value;
    
    if (val && val.trim() != '') {
      this.DisList = this.DisList_Full.filter((item =>{
        return item.districtdesc.toLowerCase().indexOf(val.toLowerCase()) > -1;
      }))
    }else{
      this.DisList = this.DisList_Full;
    }
  }

}

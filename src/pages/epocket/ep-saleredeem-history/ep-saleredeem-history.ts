import { Component } from '@angular/core';
import { AlertController,LoadingController,NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPProductProvider } from '../../../providers/epocket/product';
import { CalendarComponentOptions, CalendarModal } from 'ion2-calendar';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { EpDealerHistoryInfoPage } from '../ep-dealer-history-info/ep-dealer-history-info';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPSaleProvider } from '../../../providers/epocket/sale';
import { EpSaleredeemHistoryInfoPage } from './../ep-saleredeem-history-info/ep-saleredeem-history-info';
import { EpHomeCoinstockPage } from '../ep-home-coinstock/ep-home-coinstock';
import { Platform } from 'ionic-angular/platform/platform';

@Component({
  selector: 'page-ep-saleredeem-history',
  templateUrl: 'ep-saleredeem-history.html',
})
export class EpSaleredeemHistoryPage {
  
   loading: any;
   config: any;
   userInfo: any;
   customerInfo: any;
   dealerID: string;
 
   HistoryHD: any[];
   HistoryInfo: any;
 
   dateRange: { from: string; to: string; };
   type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
   optionsRange: CalendarComponentOptions = {
     pickMode: 'range'
   };
 
   startDate: any;
   endDate: any;
 
   constructor(public navCtrl: NavController, 
               public navParams: NavParams,
               public loadingCtrl: LoadingController,
               public configProvider: ConfigProvider,
               public storageProvider: StorageProvider,
               public epProductProvider: EPProductProvider,
               public alertCtrl: AlertController,
               private epSaleProvider : EPSaleProvider,
               private modalCtrl: ModalController,
               private platform: Platform) {
 
       this.config = this.storageProvider.getConfig();
       this.userInfo = this.storageProvider.getUserInfo();
       this.customerInfo = this.storageProvider.getCustomerInfo();
 
       this.startDate = moment().format('YYYY-MM-DD'); 
       this.endDate = moment().add('days', 7).format('YYYY-MM-DD');
       
       this.backButtonAndroid();
   }

   backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }
 
   ionViewDidLoad() {
     this.fetchRedeemHistory();
   }
 
   onOpenStartDate() {
     let calendar =  this.modalCtrl.create(CalendarModal, {
       options: {
         title: 'เลือกวันที่',
         canBackwardsSelected: true,
         defaultDate: moment(this.startDate).toDate(),
         color: 'cal-color',
         doneIcon: true,
         closeIcon: true
       }
     });
 
     calendar.present();
 
     calendar.onDidDismiss((date, type) => {
       if (type == 'done') {
         this.startDate = moment(date.dateObj).format('YYYY-MM-DD');
       }
     });
   }
 
   onOpenEndDate() {
     let calendar =  this.modalCtrl.create(CalendarModal, {
       options: {
         title: 'เลือกวันที่',
         canBackwardsSelected: true,
         defaultDate: moment(this.endDate).toDate(),
         from: moment(this.startDate).toDate(),
         color: 'cal-color',
         doneIcon: true,
         closeIcon: true
       }
     });
 
     calendar.present();
 
     calendar.onDidDismiss((date, type) => {
       if (type == 'done') {
         this.endDate = moment(date.dateObj).format('YYYY-MM-DD');
       }
     });
   }
 
   fetchRedeemHistory() {
 
     this.showLoading();
 
     let DataInfo = {
       BeginDate: this.startDate ,
       EndDate: this.endDate,
       DealerID: String(this.dealerID)
     };
 
     this.epSaleProvider.getSaleRedeemHistory(DataInfo)
       .then(res => {
         console.log("res=>",res);
         if (res.result == 'SUCCESS') {
           this.HistoryHD = res.data.redeemHistoryHD;
         }
         this.hideLoading();
         this.checkDataEmpty()
       });
   }

   checkDataEmpty(){
    if ((this.HistoryHD.length == 0)) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูลรายงานฝาเหรียญ',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.setRoot(EpHomeCoinstockPage);
            }
          }]
      }).present();
    }
  }

 
   showLoading() {
     if (!this.loading) {
       this.loading = this.loadingCtrl.create({
         content: 'กำลังโหลด...'
       });
 
       this.loading.present();
     }
   }
 
   hideLoading() {
     if (this.loading) {
       this.loading.dismiss();
       this.loading = null;
     }
   }
 
   onSelected(refdocno){
     this.navCtrl.push(EpSaleredeemHistoryInfoPage,{ refdocno : refdocno }, {
       animate: true,
       animation: 'ease-in'
     });
   }
 
 }
 
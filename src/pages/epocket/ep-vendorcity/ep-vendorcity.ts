import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  selector: 'page-ep-vendorcity',
  templateUrl: 'ep-vendorcity.html',
})
export class EpVendorcityPage {

  CityList : Array<any>;
  CityList_Full : Array<any>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public epDealerProvider : EPDealerProvider) {
  }

  ionViewDidLoad() {
    this.fatchData();
  }

  fatchData() {
    this.epDealerProvider.getVendorCity({})
      .then(res => {
        this.CityList = res.data.citylist;
        this.CityList_Full = this.CityList;
      }), err => {
        console.log(err);
      }
  }

  onSelectCity(item){
    this.navCtrl.pop().then(() => this.navParams.get('cityObject')(item));
  }

  getItems(ev: any) {
    let val = ev.target.value;
    
    if (val && val.trim() != '') {
      this.CityList = this.CityList_Full.filter((item =>{
        return item.citydesc.toLowerCase().indexOf(val.toLowerCase()) > -1;
      }))
    }else{
      this.CityList = this.CityList_Full;
    }
  }


}

import { EpCartPage } from './../ep-cart/ep-cart';
import { StorageProvider } from './../../../providers/shared/storage';
import { ConfigProvider } from '../../../providers/eordering/config';
import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { EPProductProvider } from '../../../providers/epocket/product';
import { EpProductInfoPage } from '../ep-product-info/ep-product-info';

/**
 * Generated class for the EpProductBrandSizeListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ep-product-brand-size-list',
  templateUrl: 'ep-product-brand-size-list.html',
})
export class EpProductBrandSizeListPage {
  userInfo: any;
  alertCtrl: any;

  //productBrand: Object = {};
  productBrand: any;
  productBrandSizes: any;
  loading: any;
  searchText: string;
  config: any;
  painter: any;
  customerInfo: any;
  dealerID: string;

  datalist = {
    btfs: '',
    qty: ''
  };

  constructor(public navCtrl: NavController, 
              public loadingCtrl: LoadingController,
              public navParams: NavParams,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              public epProductProvider: EPProductProvider) {

              this.productBrand = this.navParams.get('productBrand');
              this.config = this.storageProvider.getConfig();
              this.userInfo = this.storageProvider.getUserInfo();
              this.customerInfo = this.storageProvider.getCustomerInfo();
              this.dealerID = this.customerInfo.customerCode ;
              this.painter = this.navParams.get('painter');
  }

  ionViewDidLoad() {
    this.fetchProductBrandSize(this.productBrand.brandcode);
  }
  
  fetchProductBrandSize(BrandNo : string) {

    this.epProductProvider.getProductBrandSize(BrandNo,this.dealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.productBrandSizes = res.data.productBrandSize;
          this.productBrandSizes.forEach(item => {
            item.qty = null;
          });
        }
      });
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

  onSelectProductInfo(BTFS) {
    this.navCtrl.push(EpProductInfoPage, { BTFS : BTFS , painter : this.painter }, {
			animate: true,
			animation: 'ease-in'
		});
  }

  AddOrderToCart() {

    this.productBrandSizes.forEach(item => {
      if ((item.qty != null) && (item.qty != '') && (item.qty > 0)) {
        this.SaveOrder(item.btfs, item.qty);
      }
    });
    this.navCtrl.push(EpCartPage,{painter : this.painter});
  }

  SaveOrder(BTFSNO : string, Qty : string) {

    let productData = {
		  BTFSNO: BTFSNO ,
      Qty: Qty ,
      PromotionID: '1',
      Username: this.userInfo.userName,
      DealerID: this.dealerID,
      PainterID: this.painter.id
    };
    
    this.showLoading();

    this.epProductProvider.addProductDealerRedeem(productData)
		  .then(res => {
		  	if (res.result == 'SUCCESS') {
		  		// this.navCtrl.setRoot(EpCartPage,{painter : this.painter});
		  		//this.navCtrl.push(EpCartPage,{painter : this.painter});
			  } else {
				  this.alertCtrl.create({
					  title: 'แจ้งเตือน',
					  subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
					  buttons: ['ตกลง']
				  }).present();
        }
      });

    this.hideLoading();
  }

  cancel() {
    this.navCtrl.popToRoot();
  }

}

import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, App } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { EpCoinstockProductInfoPage } from '../ep-coinstock-product-info/ep-coinstock-product-info';
import { StorageProvider } from './../../../providers/shared/storage';
import * as moment from 'moment';
import { DecimalPipe } from '@angular/common';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { EpCoinstockListPage } from './../ep-coinstock-list/ep-coinstock-list';
import { EpHomeCoinstockPage } from '../ep-home-coinstock/ep-home-coinstock';


@Component({
  selector: 'page-ep-coinstock-productbrand-list',
  templateUrl: 'ep-coinstock-productbrand-list.html',
  providers: [DecimalPipe]
})
export class EpCoinstockProductbrandListPage {
  productBrand: Array<any>;
  loading: any;
  dealerID: string;
  CustomerName: string;
  public rootPage: any;
  customerInfo: any;
  userInfo: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public app: App,
    public loadingCtrl: LoadingController,
    public configProvider: ConfigProvider,
    public epSaleProvider: EPSaleProvider,
    public storageProvider: StorageProvider,
    public alertCtrl: AlertController,
    private decimalPipe: DecimalPipe,
    private toastCtrl: ToastController) {

    this.userInfo = this.storageProvider.getUserInfo();

    this.dealerID = this.navParams.get("dealerID");
    this.CustomerName = this.navParams.get("custname");
  }

  fetchProductBrandByDealer() {
    this.showLoading();
    let productData = {
      DEALER_ID: this.dealerID
    };

    this.epSaleProvider.GetProductBrandByDealer(productData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.productBrand = res.data.productBrand;
          this.productBrand.forEach(item => {
            if (item.saleqty == 0)
              item.saleqty = '';
          });
        }
        this.hideLoading();
      });
  }

  Cancel() {
    this.navCtrl.pop();
  }
  //======================= ตกลง =======================
  Confirm() {
    //======================= Message Confirm =======================
    // let alert = this.alertCtrl.create({
    //   title: 'ยืนยัน การแก้ไขข้อมูล ?',
    //   enableBackdropDismiss: false,
    //   mode: 'ios',
    //   cssClass: 'alertCustomCss',
    //   buttons: [
    //     {
    //       text: 'ยกเลิก',
    //       role: 'cancel',
    //       cssClass: 'danger',
    //       handler: () => {
    //       }
    //     },
    //     {
    //       text: 'ยืนยัน',
    //       handler: () => {
    //         let messageError = this.validate();
    //         if (messageError != '') {
    //           this.ShowMessageError(messageError);
    //         }
    //         else{
    //           this.save();

    //           let dealerObj = {
    //             DEALER_ID: this.dealerID
    //           };

    //           this.epSaleProvider.GetSaleRedeemQtyAllMsg(dealerObj).then(res => {
    //             if (res.result == 'SUCCESS') {
    //               this.alertConfirm(res.data.saleRedeemGroup);
    //             }
    //             this.hideLoading();
    //           });
    //         }
    //       }
    //     }
    //   ]
    // });
    // alert.present();


    let messageError = this.validate();
    if (messageError != '') {
      this.ShowMessageError(messageError);
    }
    else {
      this.save();

      let dealerObj = {
        DEALER_ID: this.dealerID
      };

      this.epSaleProvider.GetSaleRedeemQtyAllMsg(dealerObj).then(res => {
        if (res.result == 'SUCCESS') {
          this.alertConfirm(res.data.saleRedeemGroup);
        }
        //this.hideLoading();
      });
    }
  }


  //======================= บันทึก =======================  
  OkOnclick() {

    //======================= Message confrim =======================  
    // let alert = this.alertCtrl.create({
    //   title: 'ยืนยัน การแก้ไขข้อมูล ?',
    //   enableBackdropDismiss: false,
    //   mode: 'ios',
    //   cssClass: 'alertCustomCss',
    //   buttons: [
    //     {
    //       text: 'ยกเลิก',
    //       role: 'cancel',
    //       cssClass: 'danger',
    //       handler: () => {
    //       }
    //     },
    //     {
    //       text: 'ยืนยัน',
    //       handler: () => {
    //         let messageError = this.validate();
    //         if (messageError != '') {
    //           this.ShowMessageError(messageError);
    //         }
    //         else{
    //           this.save();
    //         }
    //       }
    //     }
    //   ]
    // });
    // alert.present();


    // let messageError = this.validate();
    this.save();
    //let messageError = '';
    // if (messageError != '') {
    //   this.ShowMessageError(messageError);
    // }
    // else {
    //   this.save();
    // }
  }

  save() {
    let checkEditData = 0;

    this.productBrand.forEach(item => {
      if(item.saleqty == "")
      item.saleqty = 0;
        let productData = {
          BTFS: item.btfs,
          DealerID: this.dealerID,
          SALE_QTY: item.saleqty,
          Username: this.userInfo.userName
        };

      // กรณีเอาค่าเฉพาะที่แก้
      // if (item.saleqty != "") {
      //   let productData = {
      //     BTFS: item.btfs,
      //     DealerID: this.dealerID,
      //     SALE_QTY: item.saleqty,
      //     Username: this.userInfo.userName
      //   };

        this.epSaleProvider.UpdSaleRedeemQty(productData)
          .then(res => {
            if (res.result == 'SUCCESS') {
            }
            else {
            }
          });

        checkEditData += 1;
      
    });


    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    setTimeout(() => {
    }, 1000);

    //if (checkEditData > 0)
    toast.present();
  }

  ShowMessageError(messageError) {
    let alert = this.alertCtrl.create({
      title: 'ไม่สามารถบันทึกได้ ?',
      enableBackdropDismiss: false,
      mode: 'ios',
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {

          }
        }
      ]
    });
    alert.setMessage(messageError);

    alert.present();

  }



  validate() {
    let check = true;
    let messageError = '';
    this.productBrand.forEach(item => {
      if (item.saleqty > item.redeemqty) {
        messageError += `<span>${item.btfsdesc} เกินจำนวนที่สามารถรับได้</span><br/>`
      }
    });
    return messageError;
  }

  alertConfirm(saleRedeemGroup) {
    let alert = this.alertCtrl.create({
      // title: 'ยืนยันข้อมูล ?',
      title: this.dealerID + '/' + this.CustomerName,
      enableBackdropDismiss: false,
      mode: 'ios',
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
          }
        },
        {
          text: 'ส่งข้อมูล',
          handler: () => {
            this.editStatus();
          }
        }
      ]
    });


    let sumCoin = 0;
    let sumramount = 0;
    let sumVALUE_R = 0;
    let sumVALUE_P = 0;

    saleRedeemGroup.forEach(value => {
      sumCoin += value.qty;
      sumramount += value.ramount;
      sumVALUE_P += value.sumvaluep;
      sumVALUE_R += value.sumvaluer;
      message += '<p>' + value.cointypename + '    ' + value.qty + '    ' + value.ramount + '</p>'
    });

    let body = '';
    saleRedeemGroup.forEach(value => {
      body += `
            <tr>
              <td>${value.cointypename}</td>
              <td align="right">${this.decimalPipe.transform(value.qty, '1.0-0')}</td>
              <td align="right">${this.decimalPipe.transform(value.ramount, '1.0-0')}</td>
            </tr>
          `;
    });

    let message = `
    <p align="left"> 
    ${ moment().format('DD/MM/YYYY')} </p>
    <table class="table__message">
      <thead>
        <tr>
          <th>เหรียญ</th>
          <th>จำนวน</th>
          <th>บาท</th>
        </tr>
      </thead>
      <tbody>
        ${body}
      </tbody>
    </table>
    <p class="TextLeft">
      <b>
        <span>รวมทั้งสิ้น</span><br/>
        <span>จำนวนเหรียญ (เหรียญ) : </span>
        <span class="TextResult">${this.decimalPipe.transform(sumCoin, '1.0-0')}</span>
        <br/>
        <span>รวมค่าแลก (บาท) : </span>
        <span class="TextResult">${this.decimalPipe.transform(sumVALUE_R, '1.0-0')}</span>
        <br/>
        <span>รวมค่ารักษา (บาท) : </span>
        <span class="TextResult">${this.decimalPipe.transform(sumVALUE_P, '1.0-0')}</span>
        <br/>
        <span>รวมจำนวนเงิน (บาท) : </span>
        <span class="TextResult">${this.decimalPipe.transform(sumVALUE_R + sumVALUE_P, '1.0-0')}</span>
      <b>
    </p>

  `;

    alert.setMessage(message);
    alert.present();
  }

  editStatus() {
   
    let dealerObj = {
      DEALER_ID: this.dealerID
    };

     
    this.epSaleProvider.GetSaleRedeemQtyAll(dealerObj).then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomeCoinstockPage);

        let toast = this.toastCtrl.create({
          message: 'บันทึกข้อมูลสำเร็จ',
          duration: 2000,
          position: 'top'
        });

        setTimeout(() => {
        }, 1000);

        toast.present();
      }
    });
  }



  onSelectProductInfo(btfs) {
    this.navCtrl.push(EpCoinstockProductInfoPage, { BTFS: btfs, DEALERID: this.dealerID }, {
      animate: true,
      animation: 'ease-in'
    });
  }


  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ionViewDidLoad() {
    //this.fetchProductBrandByDealer();
  }

  // onPageWillEnter() {
  //   this.fetchProductBrandByDealer();
  // }


  ionViewDidEnter() {//on pop page
    this.fetchProductBrandByDealer();
  }



}

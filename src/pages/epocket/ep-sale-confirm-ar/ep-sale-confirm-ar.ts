import { EpHomeCoinstockPage } from './../ep-home-coinstock/ep-home-coinstock';
import { EpHomePage } from './../ep-home/ep-home';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController,Platform } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPSaleProvider } from './../../../providers/epocket/sale';


@Component({
  selector: 'page-ep-sale-confirm-ar',
  templateUrl: 'ep-sale-confirm-ar.html',
})
export class EpSaleConfirmArPage {

  loading: any;
  DataHD: any = {};
  DataDT: any;
  dealerID: string;
  userId: string;
  customerInfo: any;
  userInfo: any;
  remarkText: string = '';
  SumReturnCoin: number = 0;
  SumReceiveCoin: number = 0;
  SumLossCoin: number = 0;
  UserID : string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public storageProvider: StorageProvider,
    private toastCtrl: ToastController,
    public epSaleProvider: EPSaleProvider,
    private platform: Platform) {

    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.userInfo = this.storageProvider.getUserInfo();
    this.UserID = this.userInfo.userId;

    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    this.fetchDataInfo();
  }

  fetchDataInfo() {

    let obj = {
      SALE_ID: String(this.userInfo.userId)
    };

    this.epSaleProvider.getWarehouseRedeem(obj)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (res.data.warehouseReceive.length > 0 && res.data.warehouseReceive.length != undefined) {
            this.DataHD = res.data.warehouseReceive[0];
            this.DataDT = res.data.warehouseReceive;
            this.sumCoin(this.DataDT);
          }
          this.checkDataEmpty();
        }
        
      }
      );
  }


  checkDataEmpty() {
    let HDIsEmpty = this.DataHD.dealercode == undefined || this.DataHD.dealercode == null;
    let DTIsEmpty = this.DataDT == undefined || this.DataDT == null;

    // if ((HDIsEmpty) && (DTIsEmpty)) {
    if ((this.SumReturnCoin == 0) && (this.SumReceiveCoin == 0) && this.SumLossCoin == 0) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูลรับฝาคืน',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.popToRoot();
            }
          }]
      }).present();
    }
  }

  sumCoin(saleRedeemReceive) {
    saleRedeemReceive.forEach((value, key) => {
      this.SumReturnCoin += value.salereturn;
      this.SumReceiveCoin += value.whreturn;
      this.SumLossCoin += value.lostreturn;
    });
  }

  ReturnCoin()  {
    let alert = this.alertCtrl.create({
      title: 'ยืนยันรับฝาคืน ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.savedata();
          }
        }
      ]
    });
    alert.present();
  }


  
  savedata() {
    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    let obj = {
      SALE_ID: String(this.userInfo.userId)
    };

    this.epSaleProvider.getSentToWarehouse(obj)
    .then(res => {
      if (res.result == 'SUCCESS') {
        // this.navCtrl.setRoot(EpHomeCoinstockPage);
        this.navCtrl.pop();

        toast.onDidDismiss(() => {
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomeCoinstockPage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    }
    );

  }

  Reject(){
    let toast = this.toastCtrl.create({
      message: 'ดำเนินการ Reject ข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    let formData = {
			SALEID: String(this.UserID)
    };
    
    this.epSaleProvider.updSaleReject(formData)
      .then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomeCoinstockPage);

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomeCoinstockPage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถทำรายการได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    });
  }



}

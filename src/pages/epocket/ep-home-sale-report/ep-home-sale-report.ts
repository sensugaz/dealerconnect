import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { EpChangeprofileHistoryMainPage } from './../ep-changeprofile-history-main/ep-changeprofile-history-main';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { EpDealerHistoryPage } from '../ep-dealer-history/ep-dealer-history';
import { EpRpt06SalereceivePage } from '../ep-report/ep-rpt06-salereceive/ep-rpt06-salereceive';
import { EpRpt07SaleredeemPage } from '../ep-report/ep-rpt07-saleredeem/ep-rpt07-saleredeem';
import { EpRpt08SalecoinstockPage } from '../ep-report/ep-rpt08-salecoinstock/ep-rpt08-salecoinstock';
import { EpSaleredeemHistoryPage } from '../ep-saleredeem-history/ep-saleredeem-history';
import { EpRpt09DealerwaitconfirmPage } from '../ep-report/ep-rpt09-dealerwaitconfirm/ep-rpt09-dealerwaitconfirm';
import * as moment from 'moment';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPSaleProvider } from '../../../providers/epocket/sale';

@Component({
  selector: 'page-ep-home-sale-report',
  templateUrl: 'ep-home-sale-report.html',
})
export class EpHomeSaleReportPage {

  startDate: any;
  endDate: any;
  userInfo: any;
  username : string;
  alert : any = this.alertCtrl.create();
  DataReport : any;
  alertConfirmPassword : any = this.alertCtrl.create();
  alertPresented: boolean = false;
  
  tpMenu: Array<any> = [
    {
      icon: './assets/icon/ePocket_icon/EpRpt06SaleReceive.png',
      action: EpRpt06SalereceivePage
    }, { 
      icon: './assets/icon/ePocket_icon/EpRpt07Saleredeem.png',
      action: EpRpt07SaleredeemPage
    } ,{
      icon: './assets/icon/ePocket_icon/EpRpt08Salecoinstock.png',
      action: EpRpt08SalecoinstockPage
    } ,{
      icon : './assets/icon/ePocket_icon/EpRpt09Dealerwaitconfirm.png',
      action: EpRpt09DealerwaitconfirmPage
    }
    ];

 
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private platform: Platform,
    private alertCtrl : AlertController,
    private storageProvider : StorageProvider,
    private epSaleProvider : EPSaleProvider) {

      this.userInfo = this.storageProvider.getUserInfo();
      this.username = this.userInfo.customerdesc;
      this.startDate = moment().subtract(1, 'months').toDate();
      this.endDate = moment().toDate();
  }

  ionViewDidEnter(){
    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (this.alertPresented) {
          this.alertPresented = false;
          this.alertConfirmPassword.dismiss();
        } else {
          this.navCtrl.pop();
        }
      });
    });
   }

   onSelected(menu) {
    if (menu.action.name == "EpRpt06SalereceivePage") {
      this.fatchReport06(menu);
    } else if (menu.action.name == "EpRpt07SaleredeemPage") {
      this.fatchReport07(menu);
    } else if (menu.action.name == "EpRpt08SalecoinstockPage") {
      this.fatchReport08(menu);
    } else if (menu.action.name == "EpRpt09DealerwaitconfirmPage") {
      this.fatchReport09(menu);
    }
  }

  fatchReport06(menu){
    let paraObject = {
      SALE_NAME : this.username,
      FROM_DATE : moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY'),
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.getRPT06SaleReceive(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.saleReceive;
          this.checkDataEmpty(menu)
        }
      }),err =>{
        console.log(err);
      }
  }

  fatchReport07(menu){
    let paraObject = {
      SALE_NAME : this.username,
      FROM_DATE : moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY'),
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.GetRPT07SaleRedeem(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
            this.DataReport = res.data.saleRedeem;
            this.checkDataEmpty(menu);
        }
      }),err =>{
        console.log(err);
      } 
      
  }

  fatchReport08(menu){
    let paraObject = {
      SALE_NAME : this.username,
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.GetRPT08SaleCoinStock(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
            this.DataReport = res.data.saleCoinStock;
            this.checkDataEmpty(menu);
        }
      }),err =>{
        console.log(err);
      } 
  }

  fatchReport09(menu){
    let paraObject = {
      SALE_NAME : this.username,
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.getGetRPT09WaitDealerConfirm(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
            this.DataReport = res.data.dealerWaitConfirm;
            this.checkDataEmpty(menu);
        }
      }),err =>{
        console.log(err);
      } 
  }

  checkDataEmpty(menu) {
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;

    if (DataReportIsEmpty) {
      this.alertPresented = true;

      this.alertConfirmPassword = this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.alertPresented = false;
            }
          }]
      });

      this.alertConfirmPassword.present();

    } else {
      let actionMenu = menu.action;
      this.navCtrl.push(menu.action, {
        animate: true,
        animation: 'ease-in'
      });
    }
  }






  // onSelected(menu) {
  //   let actionMenu = menu.action;
  //   this.navCtrl.push(menu.action, {
  //     animate: true,
  //     animation: 'ease-in'
  //   });
  // }

  ionViewDidLoad() {
    
  }

}

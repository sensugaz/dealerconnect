import { EpHomeSaleReportPage } from './../ep-home-sale-report/ep-home-sale-report';
import { StorageProvider } from './../../../providers/shared/storage';
import { EpDealerNewsPage } from './../ep-dealer-news/ep-dealer-news';
import { EpCoinstockReturncoinPage } from './../ep-coinstock-returncoin/ep-coinstock-returncoin';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { EpCoinstockListPage } from '../../../pages/epocket/ep-coinstock-list/ep-coinstock-list';
import { EpSaleredeemHistoryPage } from './../ep-saleredeem-history/ep-saleredeem-history';
import { EpSaleConfirmArPage } from './../ep-sale-confirm-ar/ep-sale-confirm-ar';
import { EPSaleProvider } from '../../../providers/epocket/sale';
import { DcHomePage } from '../../dealerconnect/dc-home/dc-home';
import { Platform } from 'ionic-angular/platform/platform';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'page-ep-home-coinstock',
  templateUrl: 'ep-home-coinstock.html',
})
export class EpHomeCoinstockPage {
   /* == sale Redeem == */
  userInfo: any;
  saleRedeem : Array<any>;
  DealerCount : number;

  /* == sale Confrim == */
  DataHD: any = {};
  DataDT: any;
  SumReturnCoin: number = 0;
  SumReceiveCoin: number = 0;
  SumLossCoin: number = 0;

  alertCoinReceive: any = this.alertCtrl.create();
  alertPresented: boolean = false;
  
  /* Menu */
  tpMenu: Array<any> = [{ // *ngFor sub menu
    icon: './assets/icon/ePocket_icon/EPKEEPCOIN.png',
    action: 'CoinStock'
  }, {
    icon: './assets/icon/ePocket_icon/EPTEMP.png',
    action: 'SaleConfirmAR'
  }
  , {
    icon: './assets/icon/ePocket_icon/EPNEWS.png',
    action: 'News'
  }, { // *ngFor sub menu
    icon: './assets/icon/ePocket_icon/reportTemp.png',
    action : 'Report'
  }
   // , {
  //   icon: './assets/icon/ePocket_icon/EPRECEIVECOIN.png',
  //   action: 'ReceivCoin'
  // }
  // ,{
  //   icon: './assets/icon/ePocket_icon/EPRETURNCOIN.png',
  //   action: 'ReturnCoin'
  // }
];

  

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private epSaleProvider :EPSaleProvider,
    private storageProvider : StorageProvider,
    private platform: Platform,
    private alertCtrl: AlertController) {
    this.userInfo = this.storageProvider.getUserInfo();
  }

  backButtonHandler() {
    this.platform.registerBackButtonAction(() => {
      if (this.alertPresented) {
        this.alertPresented = false;
        this.alertCoinReceive.dismiss();
      }
      else {
        this.navCtrl.setRoot(DealerConnect, { action: 'NP' });
      }
    });
  }

  ionViewDidEnter(){
    this.fetchSaleRedeem();
    this.fetchSaleConfirm();
    this.backButtonHandler();
  }

  onSelected(action){
    if(action == 'CoinStock')
      this.navCtrl.push(EpCoinstockListPage);
    else if(action == 'ReceivCoin')
      this.navCtrl.push(EpSaleredeemHistoryPage);
    else if(action == 'ReturnCoin')
      this.navCtrl.push(EpCoinstockReturncoinPage);
    else if(action == 'News')
      this.navCtrl.push(EpDealerNewsPage);
    else if(action == 'SaleConfirmAR'){
      if(this.SumReceiveCoin == 0)
        this.notiSaleConfirm()
      else
        this.navCtrl.push(EpSaleConfirmArPage);
    }
    else if(action == 'Report')
      this.navCtrl.push(EpHomeSaleReportPage);
  }

  notiSaleConfirm(){
    this.alertCoinReceive = this.alertCtrl.create({
      title: 'ePocket Alert',
      subTitle: 'ไม่พบข้อมูลรับฝาคืน',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ตกลง',
          handler: () => {
            this.alertPresented = false;
            this.navCtrl.popToRoot();
          }
        }]
    });

    this.alertCoinReceive.present();
    this.alertPresented = true;
  }


  gotoDCHOME(){
    this.navCtrl.setRoot(DcHomePage);
  }

  fetchSaleRedeem() {
    let objuserlogin = {
		  USERID : this.userInfo.userId.toString()
    };

    this.epSaleProvider.GetSaleRedeem(objuserlogin)
      .then(res => {
        this.DealerCount = 0;
        if (res.result == 'SUCCESS') {
          this.saleRedeem = res.data.saleRedeem;
          this.DealerCount = this.saleRedeem.length;
        }
      });
  }

  fetchSaleConfirm(){
    let obj = {
      SALE_ID: String(this.userInfo.userId)
    };

    this.epSaleProvider.getWarehouseRedeem(obj)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (res.data.warehouseReceive.length > 0 && res.data.warehouseReceive.length != undefined) {
            this.DataHD = res.data.warehouseReceive[0];
            this.DataDT = res.data.warehouseReceive;
            this.sumCoin(this.DataDT);
          }
        }
       
      }
      );
  }

  sumCoin(saleRedeemReceive) {
    this.SumReceiveCoin = 0;
    saleRedeemReceive.forEach((value, key) => {
      this.SumReturnCoin += value.salereturn;
      this.SumReceiveCoin += value.whreturn;
      this.SumLossCoin += value.lostreturn;
    });
  }

}

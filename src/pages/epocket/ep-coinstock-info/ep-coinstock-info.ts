import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, App } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { EpCoinstockProductbrandListPage } from '../ep-coinstock-productbrand-list/ep-coinstock-productbrand-list';

@Component({
  selector: 'page-ep-coinstock-info',
  templateUrl: 'ep-coinstock-info.html',
})
export class EpCoinstockInfoPage {
  customerInfo : any = { }
  loading: any;  
  dealerID : string;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public app: App,
              public loadingCtrl: LoadingController,
              public configProvider: ConfigProvider,
              public epSaleProvider : EPSaleProvider) {
      this.dealerID = this.navParams.get('dealerID');
      this.fetchSaleRedeemInfo();
  }

  fetchSaleRedeemInfo(){
    this.showLoading();

    let productData = {
		  DEALER_ID: this.dealerID 
    };

    this.epSaleProvider.GetSaleRedeemInfo(productData)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.customerInfo = res.data.customerInfo[0];
      }
      this.hideLoading();
    });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

  KeepCoin(){
    this.navCtrl.push(EpCoinstockProductbrandListPage, { dealerID : this.dealerID }, {
			animate: true,
			animation: 'ease-in'
		});
  }



  ionViewDidLoad() {
  }

}

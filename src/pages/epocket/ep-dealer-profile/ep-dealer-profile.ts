import { EpBankPage } from './../ep-bank/ep-bank';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { StorageProvider } from './../../../providers/shared/storage';
import { ConfigProvider } from './../../../providers/eordering/config';
import { EpCoinstockInfoPage } from '../ep-coinstock-info/ep-coinstock-info';
import { EpDealerProfileStep2Page } from '../ep-dealer-profile-step2/ep-dealer-profile-step2';


@Component({
  selector: 'page-ep-dealer-profile',
  templateUrl: 'ep-dealer-profile.html',
})
export class EpDealerProfilePage {

  userInfo: any;
  customerInfo: any;
  dealerID: string;

  inputAccountName: string;
  inputBranch: string;  
  inputBankCode: string;
  inputBankName: string;
  inputBankAccount: string;

  inputTaxno : string;
  inputAddresstax : string;
  inputMobile : string;
  inputIswht : string;

  BankInfo: any;
  changeProfile: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public configProvider: ConfigProvider,
    public storageProvider: StorageProvider,
    private epSaleProvider: EPSaleProvider,
    private platform : Platform 
  ) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.dealerID = this.customerInfo.customerCode;

    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    this.fetchDataInfo();
  }

  fetchDataInfo() {
    let obj = {
      DEALER_ID: this.dealerID
    };

    this.epSaleProvider.getProfileInfo(obj)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.changeProfile = res.data.changeProfile[0];
          this.inputAccountName = this.changeProfile.accountname;
          this.inputBranch = this.changeProfile.bankbranch;
          this.inputBankAccount = this.changeProfile.bankaccount;
          this.inputBankCode = this.changeProfile.bankkey;
          this.inputBankName = this.changeProfile.bankname;

          this.inputTaxno = this.changeProfile.taxno;
          this.inputAddresstax = this.changeProfile.addresstax;
          this.inputMobile = this.changeProfile.mobile;
          this.inputIswht = this.changeProfile.iswht;
        }
      });
  }

  next() {
    this.navCtrl.push(EpDealerProfileStep2Page, {
      "accountname" : this.inputAccountName,
      "bankcode" : this.inputBankCode,
      "bankname" : this.inputBankName,
      "branch": this.inputBranch,
      "bankaccount" : this.inputBankAccount,

      "taxno": this.inputTaxno,
      "addresstax": this.inputAddresstax,
      "mobile": this.inputMobile,
      "iswht": this.inputIswht
    });
  }

  BankSelect() {
    new Promise((bankObject, reject) => {
      this.navCtrl.push(EpBankPage, { bankObject: bankObject });
    }).then(data => {
      this.BankInfo = data;
      this.inputBankCode = this.BankInfo.bankcode;
      this.inputBankName = this.BankInfo.bankname;
    });
  }

}


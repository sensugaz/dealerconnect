import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EpChangeprofileStep4Page } from '../ep-changeprofile-step4/ep-changeprofile-step4';
import { EpVendorcityPage } from '../ep-vendorcity/ep-vendorcity';
import { EpVendordistrictPage } from '../ep-vendordistrict/ep-vendordistrict';


@Component({
  selector: 'page-ep-changeprofile-step3',
  templateUrl: 'ep-changeprofile-step3.html',
})
export class EpChangeprofileStep3Page {

  // inputAddresstax: string = "";
  // inputStreet4: string = "";
  // inputCityCode: string = "";
  // inputCityName: string = "";

  // inputDistrictCode: string = "";
  // inputDistrictDesc: string = "";
  // inputDistrictName: string = "";
  // inputPostCode: string = "";

  inputAccountName: string = "";
  inputBankKey: string  = "";
  inputBankName: string  = "";
  inputBankBranchCode = "";
  inputBankBranchName = "";
  inputBankAccount: string  = "";

  inputRecipienttype : string = "";
  inputRecipienttypeName : string = "";
  inputTaxno : string  = "";
  inputMobile : string  = "";
  inputMobileWeb : string  = "";
  inputEmail : string = "";

  inputAddresstax : string  = "";
  inputstreet4 : string= "";
  inputCityCode : string = "";
  inputCityDesc : string = "";
  inputDistrictCode : string = "";
  inputDistrictDesc : string = "";
  inputIswht : string  = "";
  inputstreet5 : string= "";
  inputPostCode : string = "";
  
  inputImgbookbank : string ="";
  inputImgidcard : string ="";

  inputAccountNameOld = "";
  inputBranchOld: string  = "";  
  inputBankCodeOld: string  = "";
  inputBankNameOld: string  = "";
  inputBankAccountOld: string  = "";
  isnew: number = 0;
  isChange: number;


  DistricInfo : any;
  CityInfo: any;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams) {

      this.inputAccountName = this.navParams.get('accountname');
      this.inputBankKey = this.navParams.get('bankkey');
      this.inputBankName = this.navParams.get('bankname');
      this.inputBankBranchCode = this.navParams.get('bankbranchcode');
      this.inputBankBranchName = this.navParams.get('bankbranchname');
      this.inputBankAccount = this.navParams.get('bankaccount');

      this.inputRecipienttype = this.navParams.get('recipienttype');
      this.inputTaxno = this.navParams.get('taxno');
      this.inputMobile = this.navParams.get('mobile');
      this.inputMobileWeb = this.navParams.get('mobileweb');
      this.inputEmail = this.navParams.get('email');
      
      this.inputAddresstax = this.navParams.get('addresstax');

      
      this.inputstreet4 = this.navParams.get('street4');
      this.inputCityCode = this.navParams.get('citycode');
      this.inputCityDesc = this.navParams.get('citydesc');
      this.inputDistrictCode = this.navParams.get('districtcode');
      this.inputDistrictDesc = this.navParams.get('districtdesc');
      this.inputIswht = this.navParams.get('iswht');
      this.inputstreet5 = this.navParams.get('street5');
      this.inputPostCode = this.navParams.get('postcode');
      
      this.inputImgbookbank = this.navParams.get('imgbookbank');
      this.inputImgidcard = this.navParams.get('imgidcard');
      this.isnew = this.navParams.get('isnew');
      this.isChange = this.navParams.get('isChange');
            
      console.log("Page3 ",this.isChange);
      this.inputImgbookbank =  this.navParams.get('imgbookbank');
      this.inputImgidcard =  this.navParams.get('imgidcard');
  }

  ionViewDidLoad() {
  }

  next() {
    this.navCtrl.push(EpChangeprofileStep4Page,{
      "accountname" : this.inputAccountName,
      "bankkey" : this.inputBankKey,
      "bankname" : this.inputBankName,
      "bankbranchcode" : this.inputBankBranchCode,
      "bankbranchname" : this.inputBankBranchName,
      "bankaccount" : this.inputBankAccount,

      "recipienttype" : this.inputRecipienttype,
      "taxno": this.inputTaxno,
      "mobile": this.inputMobile,
      "mobileweb": this.inputMobileWeb,
      "email": this.inputEmail,
      
      "addresstax": this.inputAddresstax,
      "street4": this.inputstreet4,
      "citycode": this.inputCityCode,
      "citydesc": this.inputCityDesc,
      "districtcode": this.inputDistrictCode,
      "districtdesc": this.inputDistrictDesc,
      "iswht": this.inputIswht,
      "street5": this.inputstreet5,
      "postcode" : this.inputPostCode,

      "imgbookbank" : this.inputImgbookbank,
      "imgidcard" : this.inputImgidcard,

      "isnew" : this.isnew,
      "isChange" : this.isChange
    });
  }

  back(){
    this.navCtrl.pop();
  }

  CitySelect(){
    new Promise((cityObject, reject) => {
      this.navCtrl.push(EpVendorcityPage, { cityObject: cityObject });
    }).then(data => {
      this.CityInfo = data;
      this.inputCityCode = this.CityInfo.citycode;
      this.inputCityDesc = this.CityInfo.citydesc;
    });
  }

  //อำเภอ / เขต
  DistricSelect(){
    new Promise((DistricObject, reject) => {
      this.navCtrl.push(EpVendordistrictPage, { "CityCode" : this.inputCityCode,DistricObject: DistricObject });
    }).then(data => {
      this.DistricInfo = data;
      this.inputDistrictCode = this.DistricInfo.districtcode;
      this.inputDistrictDesc = this.DistricInfo.districtdesc;
    });
  }

  //รอ Master 
  StreetSelect(){

  }

}

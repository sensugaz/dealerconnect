import { Component } from '@angular/core';
import { AlertController,LoadingController,NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPProductProvider } from '../../../providers/epocket/product';
import { EpCartPage } from '../ep-cart/ep-cart';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'page-ep-product-info',
  templateUrl: 'ep-product-info.html',
})
export class EpProductInfoPage {

  inputQty: number;

  BTFS: string;
  loading: any;
  config: any;
  productInfo: any = { };
  userInfo: any;
  customerInfo: any;
  dealerID: string;
  promotionID: string;
  painter: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              public epProductProvider: EPProductProvider,
              public alertCtrl: AlertController) {
              
              this.BTFS = this.navParams.get('BTFS');
              this.painter = this.navParams.get('painter');
              console.log('productInfo >> ',this.painter);
              this.config = this.storageProvider.getConfig();
              this.userInfo = this.storageProvider.getUserInfo();
              this.customerInfo = this.storageProvider.getCustomerInfo();
              this.dealerID = this.customerInfo.customerCode ;
              this.promotionID = '1';
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpProductInfoPage');
    this.fetchProductInfo(this.BTFS);
  }

  fetchProductInfo(BTFS : string) {

    // this.showLoading();

    this.epProductProvider.getProductInfo(BTFS,this.dealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.productInfo = res.data.productInfo[0];
          // console.log(res.data.productBrandSize);
        }

        // this.hideLoading();
      });
      
  }

  // showLoading() {
	// 	if (!this.loading) {
	// 		this.loading = this.loadingCtrl.create({
	// 			content: 'กำลังโหลด...'
	// 		});

	// 		this.loading.present();
	// 	}
	// }

	// hideLoading() {
	// 	if (this.loading) {
	// 		this.loading.dismiss();
	// 		this.loading = null;
	// 	}
  // }

  cancel() {
    //console.log('Cancel');
    //this.navCtrl.canGoBack();
    //this.navCtrl.setRoot(ePocket);
    this.navCtrl.popToRoot();
  }

  // confirm() {
  //   this.navCtrl.push('TpConfirmPage');
  //   //this.navCtrl.push(EpProductInfoPage, { BTFS : BTFS });
  // }

  AddProductToCart(){

    let productData = {
		  BTFSNO: this.productInfo.btfs ,
      Qty: this.inputQty ,
      PromotionID:this.promotionID,
      Username: this.userInfo.userName,
      DealerID: this.dealerID,
      PainterID: this.painter.id
    };
    
    console.log(productData);
    
    this.epProductProvider.addProductDealerRedeem(productData)
		  .then(res => {
		  	if (res.result == 'SUCCESS') {
		  		this.navCtrl.setRoot(EpCartPage,{painter : this.painter});
			  } else {
				  this.alertCtrl.create({
					  title: 'แจ้งเตือน',
					  subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
            buttons: ['ตกลง'],
            mode: 'ios',
            enableBackdropDismiss: false,
				  }).present();
        }
      });
  }

}

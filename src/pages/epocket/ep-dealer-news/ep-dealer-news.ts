import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { EPProductProvider } from './../../../providers/epocket/product';
import { StorageProvider } from './../../../providers/shared/storage';
import { ConfigProvider } from './../../../providers/eordering/config';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController ,Platform} from 'ionic-angular';
import { EpDealerNewsInfoPage } from '../ep-dealer-news-info/ep-dealer-news-info';

@Component({
  selector: 'page-ep-dealer-news',
  templateUrl: 'ep-dealer-news.html',
})
export class EpDealerNewsPage {

  loading: any;
  config: any;
  userInfo: any;
  customerInfo: any;
  dealerID: string;
  UserType: string ;

  HistoryHD: any[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              public epProductProvider: EPProductProvider,
              public epDealerProvider: EPDealerProvider,
              public alertCtrl: AlertController,
              private platform: Platform) {

              this.config = this.storageProvider.getConfig();
              this.userInfo = this.storageProvider.getUserInfo();

              this.backButtonAndroid();
  }
  
  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpDealerNewsPage');
    this.fetchDataList();
  }

  fetchDataList() {

    this.showLoading();

    this.UserType = 'Admin';

    this.epDealerProvider.getEPocketNews(this.userInfo.deptStatus)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.HistoryHD = res.data.news;
        }
        this.hideLoading();
      });
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

  onSelected(newsid){
    this.navCtrl.push(EpDealerNewsInfoPage,{ newsid : newsid }, {
			animate: true,
			animation: 'ease-in'
		});
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPSaleProvider } from './../../../providers/epocket/sale';

/**
 * Generated class for the EpChangeprofileHistoryRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ep-changeprofile-history-request',
  templateUrl: 'ep-changeprofile-history-request.html',
})
export class EpChangeprofileHistoryRequestPage {

  userInfo: any;
  customerInfo: any;
  DEALER_ID: string
  DataChangeProfileRequest: Array<any>

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private epSaleProvider: EPSaleProvider,
    private storageProvider: StorageProvider) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;

    this.fetchChangeProfileData();
  }

  ionViewDidLoad() {
  }

  fetchChangeProfileData() {
    let paraObject = {
      DEALER_ID: this.DEALER_ID
    }
    this.epSaleProvider.getProfileHistoryRequest(paraObject)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataChangeProfileRequest = res.data.changeProfile;
        }
      });
  }




}

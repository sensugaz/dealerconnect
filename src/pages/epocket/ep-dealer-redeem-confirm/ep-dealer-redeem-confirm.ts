import { EpHomePage } from './../ep-home/ep-home';
import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController,Platform } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'page-ep-dealer-redeem-confirm',
  templateUrl: 'ep-dealer-redeem-confirm.html',
})
export class EpDealerRedeemConfirmPage {

  loading: any;
  DataHD: any = {};
  DataDT: any;
  dealerID: string;
  customerInfo: any;
  userInfo: any;
  remarkText: string = '';
 
  SumDealerQty : number ;
  SumSaleQty : number ;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              public storageProvider: StorageProvider,
              private toastCtrl: ToastController,
              public epDealerProvider: EPDealerProvider,
              private platform : Platform) {

    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.dealerID = this.customerInfo.customerCode ;
    this.userInfo = this.storageProvider.getUserInfo();
    
    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpDealerRedeemConfirmPage');
    this.fetchDataInfo();
    
  }

  checkDataEmpty(){
    //let HDIsEmpty = this.DataHD == undefined || this.DataHD == null;
    let HDIsEmpty = this.DataHD.dealercode == undefined || this.DataHD.dealercode == null ;
    let DTIsEmpty = this.DataDT == undefined || this.DataDT == null;

    // console.log('DataHD > ',this.DataHD);
    // console.log('DataDT > ',this.DataDT);
    // console.log('HD > ',HDIsEmpty);
    // console.log('DT > ',DTIsEmpty);

    if ((HDIsEmpty) && (DTIsEmpty)){
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูลการรับฝา',
        enableBackdropDismiss: false,
        buttons: [
          { text: 'ตกลง',
            handler: () => {
              this.navCtrl.popToRoot();
              }
          }]
      }).present();
    }
  }

  fetchDataInfo() {

    this.showLoading();

    this.epDealerProvider.getDealerReceive(this.dealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataHD = res.data.dealerreceivehd[0];
          this.DataDT = res.data.dealerreceivedt;
          this.sumDataDT();

          this.checkDataEmpty();
        }
        this.hideLoading();
        this.checkDataEmpty();
      }
    );
}
  sumDataDT(){
    this.SumDealerQty = 0 ;
    this.SumSaleQty = 0;
    this.DataDT.forEach(element => {
      this.SumDealerQty += parseInt(element.dealerqty);
      this.SumSaleQty += parseInt(element.saleqty);
    });
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

  ConfirmOrder(){
    let alert = this.alertCtrl.create({
      title: 'ยืนยันการบันทึกข้อมูล ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.savedata();
          }
        }
      ]
    });

    alert.present();

  }

  savedata(){

    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    let formData = {
			DealerID: this.dealerID,
			Remark: this.remarkText,
			Username: this.userInfo.userName
    };
    
    this.epDealerProvider.updateDealerReceive(formData)
      .then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomePage);

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomePage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    });
  }

  Reject(){
    let alert = this.alertCtrl.create({
      title: 'ยกเลิกการแลกฝา ?',
      mode: 'ios',
      cssClass:"AlertTitleRed",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.rejectdata();
          }
        }
      ]
    });

    alert.present();

  }

  rejectdata(){

    let toast = this.toastCtrl.create({
      message: 'ดำเนินการ Reject ข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    let formData = {
			DealerID: this.dealerID,
			Username: this.userInfo.userName
    };
    
    this.epDealerProvider.updateDealerRejectReceive(formData)
      .then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomePage);

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomePage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถทำรายการได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    });
  }

}

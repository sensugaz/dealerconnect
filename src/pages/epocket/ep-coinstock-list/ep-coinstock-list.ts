import { EpHomeCoinstockPage } from './../ep-home-coinstock/ep-home-coinstock';
import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, App, AlertController, Platform } from 'ionic-angular';
import { EPProductProvider } from '../../../providers/epocket/product';
import { EPSaleProvider } from '../../../providers/epocket/sale';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EpCoinstockInfoPage } from './../../../pages/epocket/ep-coinstock-info/ep-coinstock-info';
import { StorageProvider } from './../../../providers/shared/storage';
import { convertToView } from 'ionic-angular/navigation/nav-util';
import { EpCoinstockProductbrandListPage } from '../ep-coinstock-productbrand-list/ep-coinstock-productbrand-list';
import { EoMenuPage } from './../../eordering/eo-menu/eo-menu';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@Component({
  selector: 'page-ep-coinstock-list',
  templateUrl: 'ep-coinstock-list.html',
})
export class EpCoinstockListPage {
  saleRedeem: Array<any>;
  loading: any;
  config: any;
  userInfo: any;
  rootPage: any;
  alertPresented: boolean = false;
  alert: any = this.alertCtrl.create();

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public app: App,
    public loadingCtrl: LoadingController,
    public configProvider: ConfigProvider,
    public epProductProvider: EPProductProvider,
    public epSaleProvider: EPSaleProvider,
    public storageProvider: StorageProvider,
    private alertCtrl: AlertController,
    private platform: Platform,
    public toastCtrl: ToastController,
  ) {


    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();

     
  }

  ionViewDidEnter() {
    this.backButtonAndroid();
    this.fetchSaleRedeem();
  }

  backButtonAndroid() {
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (this.alertPresented) {
          this.alert.dismiss();
          this.alertPresented = false;
        } else {
          this.navCtrl.pop();
        }

      });
    });
  }

  // myHandlerFunction() {
  //   let toast = this.toastCtrl.create({
  //     message: "Press Again to Confirm Exit1",
  //     duration: 3000
  //   });
  //   toast.present();
  //   this.navCtrl.pop();
  // }

  fetchSaleRedeem() {
    this.showLoading();

    let objuserlogin = {
      USERID: this.userInfo.userId.toString()
    };

    this.epSaleProvider.GetSaleRedeem(objuserlogin)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.saleRedeem = res.data.saleRedeem;
          console.log("this sale",this.saleRedeem);
        }
        this.hideLoading();
        this.checkDataEmpty();
      });
  }


  checkDataEmpty() {
    if ((this.saleRedeem.length == 0)) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูลฝาเหรียญ',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              // this.navCtrl.popToRoot();
              this.navCtrl.setRoot(EpHomeCoinstockPage);
              // this.rootPage = EpHomeCoinstockPage;
            }
          }]
      }).present();
    }
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpCoinstockListPage');
  }

  onSelectCustomer(item) {
    this.navCtrl.push(EpCoinstockInfoPage, { dealerID: item }, {
      animate: true,
      animation: 'ease-in'
    });
  }

  GotoBTFSPage(dealerID, custname,countproductconfirm) {
    console.log(countproductconfirm);
    if(countproductconfirm == 0 || countproductconfirm == '0'){
      this.navCtrl.push(EpCoinstockProductbrandListPage, { "dealerID": dealerID, "custname": custname });
    }else{
       this.alert = this.alertCtrl.create({
        title: 'ไม่สามารถทำรับฝาได้',
        subTitle: 'เนื่องจากมีรายการรอร้านค้ายืนยันในรอบที่แล้ว',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
            }
          }
        ]
      });

      this.alert.present();
      this.alertPresented = true;
    }
  }

  gotoEPHome() {
    this.navCtrl.setRoot(EpHomeCoinstockPage);
  }

 


}

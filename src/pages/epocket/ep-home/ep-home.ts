import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { EpDealerRedeemConfirmPage } from '../ep-dealer-redeem-confirm/ep-dealer-redeem-confirm';
import { EpDealerNewsPage } from '../ep-dealer-news/ep-dealer-news';
import { EpDealerPaymentHistoryPage } from '../ep-dealer-payment-history/ep-dealer-payment-history';
// import { HomePage } from '../../dealerconnect/home/home';
import { EpConfirmPhoneNoPage } from '../ep-confirm-phone-no/ep-confirm-phone-no';
import { StorageProvider } from '../../../providers/shared/storage';
import { EpChangeprofilePage } from './../ep-changeprofile/ep-changeprofile';
import { EpHomeDealerReportPage } from '../ep-home-dealer-report/ep-home-dealer-report';
import { EpDealerProfilePage } from '../ep-dealer-profile/ep-dealer-profile';
import { DcHomePage } from '../../dealerconnect/dc-home/dc-home';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

/**
 * Generated class for the EpHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ep-home',
  templateUrl: 'ep-home.html',
})
export class EpHomePage {

  DataRedeemHD: any = {};
  DataRedeemDT: any;

  dealerID: string;
  customerInfo: any;
  userInfo: any;

  DataHD: any;
  CountSumReceiveQty: number;
  alertCoinReceive: any = this.alertCtrl.create();
  alertPresented: boolean = false;

  tpMenu: Array<any> = [{ // *ngFor sub menu
    icon: './assets/icon/ePocket_icon/EPCHANGE.png',
    action: EpConfirmPhoneNoPage
  }, { // *ngFor sub menu
    icon: './assets/icon/ePocket_icon/reportTemp.png',
    action: EpHomeDealerReportPage
  }, {
    icon: './assets/icon/ePocket_icon/EPCONFIRM.png',
    action: EpDealerRedeemConfirmPage
  }, {
    icon: './assets/icon/ePocket_icon/EPNEWS.png',
    action: EpDealerNewsPage
  }, {
    icon: './assets/icon/ePocket_icon/EPACCOUNT.png',
    action: EpDealerPaymentHistoryPage
  }, {
    icon: './assets/icon/ePocket_icon/EPPROFILE.png',
    action: EpChangeprofilePage
  }
    // ,{
    //   icon: './assets/icon/ePocket_icon/reportTemp.png',
    //   action: EpHomeReportPage
    // }
    // , {
    //   icon: './assets/icon/ePocket_icon/History.png',
    //   action: EpHomeCoinstockPage
    // }
  ];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storageProvider: StorageProvider,
    private alertCtrl: AlertController,
    public epDealerProvider: EPDealerProvider,
    private platform: Platform
  ) {
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.dealerID = this.customerInfo.customerCode;
    this.userInfo = this.storageProvider.getUserInfo();
    //this.backButtonAndroid();
  }

  ionViewDidEnter() {
    this.backButtonHandler();
    this.fetchDataInfo();
  }

  backButtonHandler() {
    this.platform.registerBackButtonAction(() => {
      if (this.alertPresented) {
        this.alertPresented = false;
        this.alertCoinReceive.dismiss();
      }
      else {
        this.navCtrl.setRoot(DealerConnect, { action: 'NP' });
      }
    });
  }


  backButtonAndroid() {
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
  }

  fetchDataInfo() {
    this.epDealerProvider.getDealerReceive(this.dealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataHD = res.data.dealerreceivehd[0];
          this.CountSumReceiveQty = this.DataHD.sumreceiveqty;
        }
        else {
          this.CountSumReceiveQty = 0;
        }
      }
      );

    //if (this.userInfo.deptStatus === 'D')
    this.IsNewDealer();
  }

  IsNewDealer() {
    let CustomerCode = this.customerInfo.customerCode;

    let ParaObject = {
      DEALER_ID: CustomerCode
    }

    this.epDealerProvider.getVendorByDealerId(ParaObject)
      .then(res => {
        if (res.result == 'SUCCESS') {

        }
        else {
          this.navCtrl.push(EpChangeprofilePage);
        }
      });

  }

  async changeCoin() {
    await this.CheckRedeemData();

    let HDIsEmpty = this.DataRedeemHD.dealercode == undefined || this.DataRedeemHD.dealercode == null;
    let DTIsEmpty = this.DataRedeemDT == undefined || this.DataRedeemDT == null;

    if ((!HDIsEmpty) && (!DTIsEmpty)) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'กรุณายืนยันการรับฝาก่อนการรับฝาใหม่ทุกครั้งด้วยครับ.',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.alertPresented = false;
              this.navCtrl.popToRoot();
            }
          }]
      }).present();

      this.alertPresented = true;
    } else {
      this.navCtrl.setRoot(EpConfirmPhoneNoPage, {
        animate: true,
        animation: 'ease-in'
      });
    }
  }

  onSelected(menu) {

    if (menu.action === 'EpConfirmPhoneNoPage') {
      this.changeCoin();
    } else if (menu.action.name === 'EpDealerRedeemConfirmPage') {
      if (this.CountSumReceiveQty == 0) {
        this.notiConfirm();
      } else {
        this.pushPage(menu);
      }
    }
    else {
      this.pushPage(menu);
    }
  }

  pushPage(menu) {
    this.navCtrl.push(menu.action, {
      animate: true,
      animation: 'ease-in'
    });
  }

  notiConfirm() {
    this.alertCoinReceive = this.alertCtrl.create({
      title: 'ePocket Alert',
      subTitle: 'ไม่พบข้อมูลการรับฝา',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ตกลง',
          handler: () => {
            this.alertPresented = false;
            this.navCtrl.popToRoot();
          }
        }]
    });

    this.alertCoinReceive.present();
    this.alertPresented = true;
  }


  gotoDCHOME() {
    this.navCtrl.setRoot(DcHomePage);
  }

  async CheckRedeemData() {

    await this.epDealerProvider.getDealerReceive(this.dealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataRedeemHD = res.data.dealerreceivehd[0];
          this.DataRedeemDT = res.data.dealerreceivedt;
        }
      }
      );

  }

  userSetting() {
    this.navCtrl.push(EpDealerProfilePage);

  }

}

import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { EpHomePage } from './../ep-home/ep-home';
import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPProductProvider } from '../../../providers/epocket/product';
import { EpProductBrandListPage } from '../ep-product-brand-list/ep-product-brand-list';
import { StorageProvider } from '../../../providers/shared/storage';
import { DecimalPipe } from '@angular/common';
@Component({
  selector: 'page-ep-cart',
  templateUrl: 'ep-cart.html',
  providers: [DecimalPipe]
})
export class EpCartPage {

  loading: any;
  config: any;
  userInfo: any;
  customerInfo: any;

  cartHD: any = {};
  cartDT: Array<any>;
  cartGroupDT: Array<any>;
  dealerID: string;
  painter: any;
  numberText: string;
  painterID: string;

  ImagePath: string;

  alert: any = this.alertCtrl.create();
  alertConfirmPassword: any = this.alertCtrl.create();
  alertPresented: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public configProvider: ConfigProvider,
    public storageProvider: StorageProvider,
    public epProductProvider: EPProductProvider,
    private epDealerProvider: EPDealerProvider,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private decimalPipe: DecimalPipe,
    private platform: Platform) {

    this.painter = this.navParams.get('painter');

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.dealerID = this.customerInfo.customerCode;
    this.numberText = this.painter.mobileno;
    this.painterID = this.painter.id;
    this.ImagePath = 'http://qasorder2.toagroup.com:8040/Images/Product/ePocket/30.jpg';

  }

  // ionViewDidLoad() {
  //   this.fetchCartInfo(this.dealerID);
  // }

  ionViewDidEnter() {
    this.backButtonAndroid();
    this.fetchCartInfo(this.dealerID);
  }

  backButtonAndroid() {
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (this.alertPresented) {
          this.alert.dismiss();
          this.alertPresented = false;
        } else {
          this.navCtrl.pop();
        }

      });
    });
  }


  fetchCartInfo(DealerID: string) {

    // this.showLoading();

    this.ImagePath = "http://qasorder2.toagroup.com:8040/Images/Product/ePocket/30.jpg";

    this.cartHD = { "totalQty": "0", "totalRamont": "0" };
    this.cartDT = [];
    this.cartGroupDT = [];

    this.epProductProvider.getCartInfo(DealerID, this.painterID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.cartHD = res.data.dealerRedeemHD[0];
          this.cartDT = res.data.dealerRedeemDT;
          this.cartGroupDT = res.data.dealerRedeemGroupDT;
        }
      });
  }

  cancel() {
    this.navCtrl.setRoot(EpProductBrandListPage, { painter: this.painter });
  }

  confirm() {
    this.onSave();
    // this.navCtrl.push(EpConfirmPhoneNoPage,{cartHD:this.cartHD,cartDT:this.cartDT,cartGroupDT:this.cartGroupDT});
  }

  remove(RID) {

    let toast = this.toastCtrl.create({
      message: 'ลบข้อมูลสำเร็จ',
      duration: 2000,
      position: 'top'
    });

    this.epProductProvider.RemoveOrder(RID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          toast.present();
          this.fetchCartInfo(this.dealerID);
        }
      });
  }

  removeAll() {
    this.alert = this.alertCtrl.create({
      title: 'คุณต้องการที่จะลบข้อมูลทั้งหมดหรือไม่ ?',
      mode: 'ios',
      cssClass: 'alertCustomCss',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
            this.alertPresented = false;
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.deleteAll();
          }
        }
      ]
    });

    this.alert.present();
    this.alertPresented = true;
  }

  deleteAll() {
    let toast = this.toastCtrl.create({
      message: 'ลบข้อมูลทั้งหมดสำเร็จ',
      duration: 2000,
      position: 'top'
    });

    this.epProductProvider.RemoveOrderAll(this.dealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          toast.present();
          this.fetchCartInfo(this.dealerID);
        }
      });
  }

  doRefresh(refresher) {
    setTimeout(() => {
      this.fetchCartInfo(this.dealerID);
      refresher.complete();
    }, 2000);
  }

  onSave() {
    this.alert = this.alertCtrl.create({
      title: 'ยืนยันข้อมูล ?',
      mode: 'ios',
      cssClass: 'alertCustomCss',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
            this.alertPresented = false;
          }
        }
      ]
    });

    let btnSave = {
      text: 'ยืนยัน',
      handler: () => {
        this.save();
      }
    };

    if (this.cartGroupDT.length > 0)
      this.alert.data.buttons.push(btnSave);


    let count = this.cartHD.totalQty;
    let amount = this.cartHD.totalRamont;

    let body = '';

    this.cartGroupDT.forEach(value => {
      body += `
        <tr>
          <td>${value.cointypename}</td>
          <td align="right">${this.decimalPipe.transform(value.qty, '1.0-0')}</td>
          <td align="right">${this.decimalPipe.transform(value.ramount, '1.2-2')}</td>
        </tr>
      `;
    });

    // let mobileDisplay = this.numberText ;

    let message = `
      <p>เบอร์โทร: ${(this.numberText == '9999999999') ? ' ไม่ระบุ ' : this.numberText} </p>
      <table class="table__message">
        <thead>
          <tr>
            <th>เหรียญ</th>
            <th>จำนวน</th>
            <th>บาท</th>
          </tr>
        </thead>
        <tbody>
          ${body}
        </tbody>
      </table>

      <p class="TextLeft">
        <b>
          <span>รวมทั้งสิ้น</span><br/>
          <span>จำนวนฝา (ฝา) : </span>
          <span class="TextResult">${this.decimalPipe.transform(count, '1.0-0')}</span>
          <br/>
          <span>จำนวนเงิน (บาท) : </span>
          <span class="TextResult">${this.decimalPipe.transform(amount, '1.2-2')} </span>
        <b>
      </p>

    `;

    this.alert.setMessage(message);

    this.alertConfirmPassword = this.alertCtrl.create({
      title: 'ePocket Alert',
      subTitle: 'ไม่สามารถยืนยันข้อมูลได้ <br/>เนื่องจากไม่มีรายการแลกฝา',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ตกลง',
          handler: () => {
          }
        }]
    });


    if (this.cartGroupDT.length > 0)
      this.alert.present();
    else
      this.alertConfirmPassword.present();

    this.alertPresented = true;

  }

  save() {
    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    let formData = {
      DealerID: this.dealerID,
      MobileNo: this.numberText,
      Username: this.userInfo.userName
    };

    this.epProductProvider.Checkout(formData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.SendSMS();
          this.navCtrl.setRoot(EpHomePage);

          toast.onDidDismiss(() => {
          });

          setTimeout(() => {
            this.navCtrl.setRoot(EpHomePage);
          }, 1000);

          toast.present();

        } else {
          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();

          this.alertPresented = true;
        }
      });

  }

  SendSMS() {
    if (this.painter.isregister != 'Y' && this.painter.painterno != "") {
      let paraObj = {
        To: this.painter.painterno,
        Content: 'กรุณาลงทะเบียนเข้าใช้ระบบที่ url= ' + this.painter.passcode + ' ขอบคุณค่ะ'
      }

      this.epDealerProvider.sendSMSPainter(paraObj)
        .then(res => {
          if (res.result == 'SUCCESS') {
          } else {
          }
        });

    }
  }

}

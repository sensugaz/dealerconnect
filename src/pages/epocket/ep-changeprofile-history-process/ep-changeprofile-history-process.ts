import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPSaleProvider } from './../../../providers/epocket/sale';

/**
 * Generated class for the EpChangeprofileHistoryProcessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ep-changeprofile-history-process',
  templateUrl: 'ep-changeprofile-history-process.html',
})
export class EpChangeprofileHistoryProcessPage {

  userInfo: any;
  customerInfo: any;
  DEALER_ID: string
  DataChangeProfileProcess: Array<any>

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private epSaleProvider: EPSaleProvider,
    private storageProvider: StorageProvider) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;

    this.fetchChangeProfileProcess();
  }


  fetchChangeProfileProcess() {
    let object_ = {
      DEALER_ID : this.DEALER_ID
    }
    this.epSaleProvider.getProfileHistoryProcess(object_)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.DataChangeProfileProcess = res.data.changeProfile;       
      }
    });
  }

  ionViewDidLoad() {
  }

}

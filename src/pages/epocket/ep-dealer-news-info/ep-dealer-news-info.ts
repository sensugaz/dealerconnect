import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { EPProductProvider } from './../../../providers/epocket/product';
import { StorageProvider } from './../../../providers/shared/storage';
import { ConfigProvider } from './../../../providers/eordering/config';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-ep-dealer-news-info',
  templateUrl: 'ep-dealer-news-info.html',
})
export class EpDealerNewsInfoPage {

  loading: any;
  config: any;
  userInfo: any;
  customerInfo: any;
  dealerID: string;
  UserType: string;

  HistoryInfo: any;
  NewsId: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              public epProductProvider: EPProductProvider,
              public epDealerProvider: EPDealerProvider,
              public alertCtrl: AlertController) {

    this.NewsId = this.navParams.get('newsid');
    this.HistoryInfo =  {};
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpDealerNewsInfoPage');
    this.fetchData();
  }

  fetchData() {

    this.showLoading();

    this.UserType = 'Admin';

    this.epDealerProvider.getEPocketNewsInfo(this.NewsId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.HistoryInfo = res.data.newsinfo[0];
        }

        this.hideLoading();
      });
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

}

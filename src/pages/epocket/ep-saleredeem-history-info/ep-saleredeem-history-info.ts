import { EPSaleProvider } from './../../../providers/epocket/sale';
import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
//import { EPDealerProvider } from '../../../providers/epocket/dealer';
/**
 * Generated class for the EpSaleredeemHistoryInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ep-saleredeem-history-info',
  templateUrl: 'ep-saleredeem-history-info.html',
})
export class EpSaleredeemHistoryInfoPage {
  
    loading: any;
    HistoryHD: any;
    HistoryInfo: any;
    RefDocNo: string;
  
    constructor(public navCtrl: NavController, 
                public navParams: NavParams,
                public loadingCtrl: LoadingController,
                public epSaleProvider: EPSaleProvider) {
  
                // this.RefDocNo = this.navParams.get('refdocno');
                this.HistoryHD = this.navParams.get('refdocno'); ;
  
    }
  
    ionViewDidLoad() {
      this.fetchDataInfo(this.RefDocNo);
    }
  
    fetchDataInfo(DataHD) {
  
      //console.log(DataHD);
      // console.log('HistoryHD >>> ');
      // console.log(this.HistoryHD);
  
      this.showLoading();
      console.log("sale INFO");
      console.log(this.HistoryHD);
      this.epSaleProvider.getSaleRedeemHistoryInfo(this.HistoryHD.refdocno)
        .then(res => {
          if (res.result == 'SUCCESS') {
            this.HistoryInfo = res.data.redeemHistoryInfo;
          }
          this.hideLoading();
        });

    }
  
    showLoading() {
      if (!this.loading) {
        this.loading = this.loadingCtrl.create({
          content: 'กำลังโหลด...'
        });
  
        this.loading.present();
      }
    }
  
    hideLoading() {
      if (this.loading) {
        this.loading.dismiss();
        this.loading = null;
      }
    }
  
  }
  
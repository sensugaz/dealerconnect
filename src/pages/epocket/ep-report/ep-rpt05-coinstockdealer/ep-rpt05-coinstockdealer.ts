import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Component } from '@angular/core';
import { NavController, NavParams,Platform } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPDealerProvider } from './../../../../providers/epocket/dealer';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

@Component({
  selector: 'page-ep-rpt05-coinstockdealer',
  templateUrl: 'ep-rpt05-coinstockdealer.html',
})
export class EpRpt05CoinstockdealerPage {

  DataReport : any;
  DataReport_Excel : any;

  customerInfo : any;
  csvData : any[] = [];
  headerRow : any[]= [];
  DEALER_ID : string;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epDealerProvider:EPDealerProvider
    ,private alertCtrl : AlertController
    ,private file: File
    ,private fileOpener: FileOpener
    ,private platform : Platform
  ) {
      this.customerInfo = this.storageProvider.getCustomerInfo();
      this.DEALER_ID = this.customerInfo.customerCode;

      this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID
    };

    this.epDealerProvider.getRPT05DealerCoinStock(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.coinStockInfo;
        }
      }),err =>{
        console.log(err);
      } 

      
    this.epDealerProvider.getRPT05DealerCoinStock_Excel(paraObject)
    .then(res =>{
      if (res.result == 'SUCCESS') {
        this.DataReport_Excel = res.data.coinStockInfo;
        let csv = papa.unparse(this.DataReport_Excel);
        this.extractData(csv);
      }
    }),err =>{
      console.log(err);
    }
  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;
    
    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        subTitle: 'ไม่พบฝาค้างส่ง ',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["มูลค่าฝา","แบรนด์", "ยอดที่ร้านรับ"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายละเอียดฝาค้างส่ง.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv);
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายละเอียดฝาค้างส่ง.csv';

    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
   // ===== End Export ===== //

  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt05ConstockInfoPage');
  }



}


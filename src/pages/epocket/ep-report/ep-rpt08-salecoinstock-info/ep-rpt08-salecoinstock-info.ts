import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPSaleProvider } from '../../../../providers/epocket/sale';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-ep-rpt08-salecoinstock-info',
  templateUrl: 'ep-rpt08-salecoinstock-info.html',
})
export class EpRpt08SalecoinstockInfoPage {

  userInfo: any;
  username : string;
  DataReport : any;
  DataReport_Excel : any;

  DOC_NO : string;
  ARRETURN_DATE : string;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epSaleProvider:EPSaleProvider
    ,private platform : Platform
    ,private file: File
    ,private document: DocumentViewer
    ,private fileOpener: FileOpener) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.username = this.userInfo.customerdesc;

    this.DOC_NO = this.navParams.get("DOC_NO");
    this.ARRETURN_DATE = this.navParams.get("ARRETURN_DATE");

    this.fatchData();

  }

  private fatchData(){
    let paraObject = {
      SALE_NAME : this.username,
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.getGetRPT08SaleCoinStockDocNo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleCoinStockDocNo.length > 0)
          {
            this.DataReport = res.data.saleCoinStockDocNo;
          }
        }
      }),err =>{
        console.log(err);
      } 

      this.epSaleProvider.getGetRPT08SaleCoinStockDocNo_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleCoinStockDocNo.length > 0)
          {
            this.DataReport_Excel = res.data.saleCoinStockDocNo;
            let csv = papa.unparse(this.DataReport_Excel);
            this.extractData(csv);
          }
        }
      }),err =>{
        console.log(err);
      }
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["รหัสร้านค้า","ชื่อร้านค้า","มูลค่าฝา","จำนวนฝา","ค่าแลก","ค่ารักษาฝาหลังหัก WHT","รวม"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายละเอียดฝาที่ส่งคืน TOA.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายละเอียดฝาที่ส่งคืน TOA.csv';
  

    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //

  DetailOnClick(){
    this.navCtrl.push(EpRpt08SalecoinstockInfoPage);
  }

  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt07SaleredeemPage');
  }

}

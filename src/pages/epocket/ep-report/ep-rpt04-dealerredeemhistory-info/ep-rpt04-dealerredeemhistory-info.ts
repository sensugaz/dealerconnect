import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPDealerProvider } from './../../../../providers/epocket/dealer';
import * as papa from 'papaparse';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-ep-rpt04-dealerredeemhistory-info',
  templateUrl: 'ep-rpt04-dealerredeemhistory-info.html',
})
export class EpRpt04DealerredeemhistoryInfoPage {

  DataReport : any;
  DataReport_Excel : any;

  customerInfo : any;
  DEALER_ID : string;
  SALE_DATE : string;
  COINTYPE_NAME : string

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epDealerProvider:EPDealerProvider
    ,private platform : Platform
    ,private file: File
    ,private fileOpener: FileOpener) {

      this.customerInfo = this.storageProvider.getCustomerInfo();
      this.DEALER_ID = this.customerInfo.customerCode;
      this.SALE_DATE = this.navParams.get("SALE_DATE");
      this.COINTYPE_NAME = this.navParams.get("COINTYPENAME");
       
      this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      SALE_DATE : this.SALE_DATE,
      COINTYPE_NAME : this.COINTYPE_NAME
    };

    this.epDealerProvider.getRPT04DealerRedeemHistoryInfo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerRedeemHistoryInfo;
        }

      }),err =>{
        console.log(err);
      } 

      this.epDealerProvider.getRPT04DealerRedeemHistoryInfo_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport_Excel = res.data.dealerRedeemHistoryInfo;
          let csv = papa.unparse(this.DataReport_Excel);
          this.extractData(csv);
        }
      }),err =>{
        console.log(err);
      } 
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["แบรนด์","รหัสร้านค้า","ชื่อร้านค้า","มูลค่าฝา","ยอดที่ร้านรับ"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานรายละเอียดฝาที่ส่งคืน TOA.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานรายละเอียดฝาที่ส่งคืน TOA.csv';

    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //
  
  cancel(){
    this.navCtrl.pop();
  }

    // ======= On select Detail =====
  toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
  
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt04DealerredeemhistoryInfoPage');
  }

}

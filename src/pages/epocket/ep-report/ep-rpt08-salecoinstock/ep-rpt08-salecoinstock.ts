import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPSaleProvider } from '../../../../providers/epocket/sale';
import { EpRpt08SalecoinstockInfoPage } from '../ep-rpt08-salecoinstock-info/ep-rpt08-salecoinstock-info';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-ep-rpt08-salecoinstock',
  templateUrl: 'ep-rpt08-salecoinstock.html',
})
export class EpRpt08SalecoinstockPage {

  userInfo: any;
  username : string;
  DataReport : any;
  DataReport_Excel : any;

  DataReportHD : any;
  DEALER_NAME : string;
  SaleCreateDate : string;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epSaleProvider:EPSaleProvider
    ,private alertCtrl: AlertController
    ,private platform : Platform
    ,private file: File
    ,private document: DocumentViewer
    ,private fileOpener: FileOpener) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.username = this.userInfo.customerdesc;
    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      SALE_NAME : this.username,
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.GetRPT08SaleCoinStock(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleCoinStock.length > 0)
          {
            this.DataReportHD = res.data.saleCoinStock[0];
            this.DataReport = res.data.saleCoinStock;
            this.DEALER_NAME = res.data.saleCoinStock[0].dealername;
            this.SaleCreateDate = res.data.saleCoinStock[0].salecreatedate;
          }
        }
      }),err =>{
        console.log(err);
      } 

      this.epSaleProvider.GetRPT08SaleCoinStock_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleCoinStock.length > 0)
          {
            this.DataReport_Excel = res.data.saleCoinStock;
            let csv = papa.unparse(this.DataReport_Excel);
            this.extractData(csv);
          }
        }
      }),err =>{
        console.log(err);
      }
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["ประเภทฝา","จำนวน"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานฝาค้างส่ง.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานฝาค้างส่ง.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //

  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null ;

    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

  DetailOnClick(docno,arreturndate){
    this.navCtrl.push(EpRpt08SalecoinstockInfoPage,{"DOC_NO":docno,"ARRETURN_DATE":arreturndate});
  }

  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt07SaleredeemPage');
  }

}

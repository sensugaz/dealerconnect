import { Component } from '@angular/core';
import { NavController, NavParams ,AlertController, Platform} from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPDealerProvider } from './../../../../providers/epocket/dealer';
import { EpRpt04DealerredeemhistoryInfoPage } from './../ep-rpt04-dealerredeemhistory-info/ep-rpt04-dealerredeemhistory-info';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CalendarModal } from 'ion2-calendar';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePicker } from '@ionic-native/date-picker';

@Component({
  selector: 'page-ep-rpt04-dealerredeemhistory',
  templateUrl: 'ep-rpt04-dealerredeemhistory.html',
})
export class EpRpt04DealerredeemhistoryPage {

  DataReport : any;
  DataReport_Excel : any;

  customerInfo : any;
  csvData : any[] = [];
  headerRow : any[]= [];
  DEALER_ID : string;

  startDate: any;
  endDate: any;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epDealerProvider:EPDealerProvider
    ,private modalCtrl: ModalController
    ,private alertCtrl : AlertController
    ,private file: File
    ,private fileOpener: FileOpener
    ,private platform : Platform
    ,private datePicker: DatePicker
  ) 
  {
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;

    this.startDate = moment().subtract(1, 'months').toDate(); 
    this.endDate = moment().toDate();

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE :  moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY')
    };

    this.epDealerProvider.getRPT04DealerRedeemHistory(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerRedeemHistory;
        }
      }),err =>{
        console.log(err);
      }

      this.epDealerProvider.getRPT04DealerRedeemHistory_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport_Excel = res.data.dealerRedeemHistory;
          let csv = papa.unparse(this.DataReport_Excel);
          this.extractData(csv);
        }
      }),err =>{
        console.log(err);
      }
  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;
 
    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

   // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    //this.headerRow = ["SALE_DATE","DEALER_CODE", "DEALER_NAME", "COIN_TYPE_NAME","DEALER_QTY","SALE_QTY","DIFF_QTY","SALE_AMOUNT","VALUE_P_AMOUNT","TOTAL_AMOUNT"]
    this.headerRow = ["วันที่ sale รับ","มูลค่าฝา", "ยอดที่ร้านรับ", "ยอดที่ sale รับ"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานฝาที่ส่งคืน TOA.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv)
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานฝาที่ส่งคืน TOA.csv';

    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
   // ===== End Export ===== //

  DetailOnClick(SALE_DATE,COINTYPENAME){
    this.navCtrl.push(EpRpt04DealerredeemhistoryInfoPage,{"SALE_DATE" : SALE_DATE,"COINTYPENAME":COINTYPENAME});
  }

  ionViewDidLoad() {
  }

  cancel(){
    this.navCtrl.pop();
  }

    // ======= On select Detail =====
  toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
  
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }


   // ===== Filter ===== //
   onOpenStartDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.startDate = date.dateObj ;
      }
    });
  }

  onOpenEndDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.endDate).toDate(),
        from: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.endDate = date.dateObj;
      }
    });
  }

  openStartDate() {
    this.datePicker.show({
      date: this.startDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.startDate = date;
      }
    })
    .catch(() => {});
  }

  openEndDate() {
    this.datePicker.show({
      date: this.endDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.endDate = date;
      }
    })
    .catch(() => {});
  }
  // ===== End Filter ===== //
  
    
}

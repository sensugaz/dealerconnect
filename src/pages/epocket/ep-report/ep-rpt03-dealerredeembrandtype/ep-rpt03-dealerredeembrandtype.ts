import { EPDealerProvider } from './../../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams ,AlertController, Platform} from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CalendarModal } from 'ion2-calendar';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePicker } from '@ionic-native/date-picker';

@Component({
  selector: 'page-ep-rpt03-dealerredeembrandtype',
  templateUrl: 'ep-rpt03-dealerredeembrandtype.html',
})
export class EpRpt03DealerredeembrandtypePage {

  DataReport : any;
  DataReport_Excel : any;
  customerInfo : any;

  DEALER_ID : string;

  startDate: any;
  endDate: any;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController, public navParams: NavParams
  ,private storageProvider:StorageProvider
  ,private epDealerProvider:EPDealerProvider
  ,private modalCtrl: ModalController
  ,private alertCtrl : AlertController
  ,private file: File
  ,private fileOpener: FileOpener
  ,private platform : Platform
  ,private datePicker: DatePicker
) {

    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;

    this.startDate = moment().subtract(1, 'months').toDate(); 
    this.endDate = moment().toDate();

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE :  moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY')
    };

    this.epDealerProvider.getRPT03DealerRedeemBT(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerRedeemBT;
          //this.checkDataEmpty();
        }

      }),err =>{
        console.log(err);
      } 


      this.epDealerProvider.getRPT03DealerRedeemBT_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport_Excel = res.data.dealerRedeemBT;
          let csv = papa.unparse(this.DataReport_Excel);
          this.extractData(csv);
          
          //this.checkDataEmpty();
        }

      }),err =>{
        console.log(err);
      }



  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;
    
    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["แบรนด์","มูลค่าฝา", "จำนวนฝา", "จำนวนเงิน"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานแบรนด์ที่แลกฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv)
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานช่างสีที่แลกฝา.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //


  // ===== Filter ===== //
  onOpenStartDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.startDate = date.dateObj ;
      }
    });
  }

  onOpenEndDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.endDate).toDate(),
        from: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.endDate = date.dateObj;
      }
    });
  }

  openStartDate() {
    this.datePicker.show({
      date: this.startDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.startDate = date;
      }
    })
    .catch(() => {});
  }

  openEndDate() {
    this.datePicker.show({
      date: this.endDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.endDate = date;
      }
    })
    .catch(() => {});
  }
  // ===== End Filter ===== //

  cancel(){
    this.navCtrl.pop();
  }

    // ======= On select Detail =====
  toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
   
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }
  

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpRpt03DealerredeembrandtypePage');
  }

}

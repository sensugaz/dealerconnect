import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EPDealerProvider } from '../../../../providers/epocket/dealer';
import { StorageProvider } from '../../../../providers/shared/storage';
import * as papa from 'papaparse';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-ep-rpt01-dealerexchangecover-info',
  templateUrl: 'ep-rpt01-dealerexchangecover-info.html',
})
export class EpRpt01DealerexchangecoverInfoPage {

  customerInfo : any;
  Dealer_ID : string;
  dealerExchangeInfo : any;
  DealerHD : any;
  DealerName : string;
  pSaleDate : string;
  pSaleQty : string;
  pTotalgrandAmount : string;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController 
    ,public navParams: NavParams
    ,private epDealerProvider:EPDealerProvider
    ,private storageProvider : StorageProvider
    ,private platform : Platform
    ,private file: File
    ,private fileOpener: FileOpener) {

      this.customerInfo = this.storageProvider.getCustomerInfo();
      this.Dealer_ID = this.customerInfo.customerCode;
      this.pSaleDate = this.navParams.get("SaleDate");
      this.pSaleQty = this.navParams.get("SaleQty");
      this.pTotalgrandAmount = this.navParams.get("TotalgrandAmount");
      this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.Dealer_ID,
      SALE_DATE : this.pSaleDate
    };

    this.epDealerProvider.getRPT01DelaerExchangeCoverInfo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.dealerExchangeInfo = res.data.exchangeCoverInfo;
          this.DealerHD = res.data.exchangeCoverInfo[0]; 

          this.Dealer_ID = this.DealerHD.dealercode;
          this.DealerName = this.DealerHD.dealername;
        }

      }),err =>{
        console.log(err);
      } 

      this.epDealerProvider.getRPT01DelaerExchangeCoverInfo_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.dealerExchangeInfo = res.data.exchangeCoverInfo;
          let csv = papa.unparse(this.dealerExchangeInfo);
          this.extractData(csv);
        }

      }),err =>{
        console.log(err);
      } 
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["รหัสร้านค้า","ชื่อร้านค้า","แบรนด์","มูลค่าฝา","จำนวนฝาที่ร้านค้ารับ","จำนวนฝาที่ร้านค้ารับ(บาท)","จำนวนฝาที่ Sale รับ","จำนวนฝาที่ Sale รับ(บาท)","ผลต่าง","เงินรวม"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานรายละเอียดการแลกฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานรายละเอียดการแลกฝา.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //

  cancel(){
    this.navCtrl.pop();
  }
  
  ionViewDidLoad() {
  }

}

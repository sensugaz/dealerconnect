import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPSaleProvider } from '../../../../providers/epocket/sale';
import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import * as papa from 'papaparse';

@Component({
  selector: 'page-ep-rpt07-saleredeem-info',
  templateUrl: 'ep-rpt07-saleredeem-info.html',
})
export class EpRpt07SaleredeemInfoPage {

  userInfo: any;
  username : string;
  DataReport : any;
  DataReport_Excel : any;

  DOC_NO : string;
  AR_RETUEN_DATE : string;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epSaleProvider:EPSaleProvider
    ,private platform : Platform
    ,private file: File
    ,private document: DocumentViewer
    ,private fileOpener: FileOpener) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.username = this.userInfo.customerdesc;

    this.DOC_NO = this.navParams.get("DOC_NO");
    this.AR_RETUEN_DATE = this.navParams.get("AR_RETUEN_DATE");
    

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DOC_NO : this.DOC_NO,
      SALE_NAME : this.username,
      RETUEN_DATE : this.AR_RETUEN_DATE
    };

    this.epSaleProvider.getGetRPT07SaleRedeemInfo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleRedeemInfo.length > 0)
          {
            this.DataReport = res.data.saleRedeemInfo;
          }
        }
      }),err =>{
        console.log(err);
      } 

      this.epSaleProvider.getGetRPT07SaleRedeemInfo_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleRedeemInfo.length > 0)
          {
            this.DataReport_Excel = res.data.saleRedeemInfo;
            let csv = papa.unparse(this.DataReport_Excel);
            this.extractData(csv);
          }
        }
      }),err =>{
        console.log(err);
      } 
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["ประเภทฝา","จำนวน"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายละเอียดฝาที่ส่งคืน TOA.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายละเอียดฝาที่ส่งคืน TOA.csv';
  
    
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //


  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt07SaleredeemPage');
  }

}

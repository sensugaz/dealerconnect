import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPSaleProvider } from '../../../../providers/epocket/sale';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as papa from 'papaparse';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-ep-rpt09-dealerwaitconfirm',
  templateUrl: 'ep-rpt09-dealerwaitconfirm.html',
})
export class EpRpt09DealerwaitconfirmPage {

  userInfo: any;
  username : string;
  DataReport : any;
  DataReport_Excel : any;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storageProvider : StorageProvider,
    private epSaleProvider : EPSaleProvider,
    private modalCtrl: ModalController,
    private alertCtrl : AlertController
    ,private platform : Platform
    ,private file: File
    ,private document: DocumentViewer
    ,private fileOpener: FileOpener) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.username = this.userInfo.customerdesc;

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      SALE_NAME : this.username,
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.getGetRPT09WaitDealerConfirm(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.dealerWaitConfirm;
        }
      }),err =>{
        console.log(err);
      } 

      this.epSaleProvider.getGetRPT09WaitDealerConfirm_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport_Excel = res.data.dealerWaitConfirm;
          let csv = papa.unparse(this.DataReport_Excel);
          this.extractData(csv);
        }
      }),err =>{
        console.log(err);
      } 
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["รหัสร้านค้า","ชื่อร้านค้า","มูลค่าฝา","ร้านค้ารับ","Sale รับ"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานรอร้านค้ายืนยัน.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานรอร้านค้ายืนยัน.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //



  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;

    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

  
  


  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
  }

}

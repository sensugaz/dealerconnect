import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPSaleProvider } from '../../../../providers/epocket/sale';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';

@Component({
  selector: 'page-ep-rpt06-salereceive-info',
  templateUrl: 'ep-rpt06-salereceive-info.html',
})
export class EpRpt06SalereceiveInfoPage {

  userInfo: any;
  username : string;
  DataReport : any;
  DataReport_Excel : any;

  csvData : any[] = [];
  headerRow : any[]= [];

  CREATE_DATE : string;
  DEALER_ID : string;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epSaleProvider:EPSaleProvider
    ,private platform : Platform
    ,private file: File
    ,private fileOpener: FileOpener) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.username = this.userInfo.customerdesc;

     
    this.CREATE_DATE = this.navParams.get("CREATE_DATE");
    this.DEALER_ID = this.navParams.get("DEALER_ID");
 
    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      SALE_NAME : this.username,
      CREATE_DATE : this.CREATE_DATE,
      DEALER_ID : this.DEALER_ID,
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.getRPT06SaleReceiveInfo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleReceiveInfo.length > 0)
          {
            this.DataReport = res.data.saleReceiveInfo;
          }

        }
      }),err =>{
        console.log(err);
      } 


      this.epSaleProvider.getRPT06SaleReceiveInfo_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.saleReceiveInfo.length > 0)
          {
            this.DataReport_Excel = res.data.saleReceiveInfo;
            let csv = papa.unparse(this.DataReport_Excel);
            this.extractData(csv);
          }

        }
      }),err =>{
        console.log(err);
      } 
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["แบรนด์","จำนวน", "มูลค่า"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;

  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายละเอียดการรับฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv);    
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายละเอียดการรับฝา.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //

  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpRpt06SalereceiveInfoPage');
  }

}

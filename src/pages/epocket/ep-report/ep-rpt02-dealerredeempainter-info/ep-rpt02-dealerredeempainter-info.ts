import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EPDealerProvider } from '../../../../providers/epocket/dealer';
import { StorageProvider } from '../../../../providers/shared/storage';
import * as papa from 'papaparse';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-ep-rpt02-dealerredeempainter-info',
  templateUrl: 'ep-rpt02-dealerredeempainter-info.html',
})
export class EpRpt02DealerredeempainterInfoPage {

  PainterNo : string;
  PainterName : string;
  DealerCode : string;
  DealerCreateDate : string
  customerInfo : any;
  dealerRedeemPainterInfo : any;
  dataReport_Excel : any;

  csvData : any[] = [];
  headerRow : any[]= [];
  constructor(public navCtrl: NavController
    ,public navParams: NavParams
    ,private epDealerProvider : EPDealerProvider
    ,private storageProvider : StorageProvider
    ,private platform : Platform
    ,private file: File
    ,private fileOpener: FileOpener
  ) {
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.PainterNo = this.navParams.get("PainterNo");
    this.PainterName = this.navParams.get("PainterName");
    //this.DealerCode = this.navParams.get("DealerCode");
    this.DealerCode = this.customerInfo.customerCode;

    this.DealerCreateDate = this.navParams.get("DealerCreateDate");

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DealerCode,
      PAINTER_NO : this.PainterNo,
      DEALER_CREATEDATE : this.DealerCreateDate
    };

    this.epDealerProvider.getRPT02DealerRedeemPainterInfo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.dealerRedeemPainterInfo = res.data.dealerRedeemPainterInfo;
        }

      }),err =>{
        console.log(err);
      } 

      this.epDealerProvider.getRPT02DealerRedeemPainterInfo_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.dataReport_Excel = res.data.dealerRedeemPainterInfo;
          let csv = papa.unparse(this.dataReport_Excel);
          this.extractData(csv);
        }
      }),err =>{
        console.log(err);
      } 
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["เบอร์ช่าง","ชื่อ","รหัสร้านค้า","ชื่อร้านค้า","แบรนด์","มูลค่าฝา","จำนวนฝา","จำนวนเงิน"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานรายละเอียดช่างสีที่แลกฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
    this.ReadWriteFile(csv);    
  }

  
  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานรายละเอียดช่างสีที่แลกฝา.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => {console.log('File is opened')}
          ).catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //

  cancel(){
    this.navCtrl.pop();
  }
  

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt02DealerredeempainterInfoPage');
  }

}

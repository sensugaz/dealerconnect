import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EPDealerProvider } from './../../../../providers/epocket/dealer';

@Component({
  selector: 'page-ep-rpt05-constock-info',
  templateUrl: 'ep-rpt05-constock-info.html',
})
export class EpRpt05ConstockInfoPage {

  DataReport : any;
  customerInfo : any;
  DEALER_ID : string;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epDealerProvider:EPDealerProvider
  ) {
      this.customerInfo = this.storageProvider.getCustomerInfo();
      this.DEALER_ID = this.customerInfo.customerCode;

      this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID
    };

    this.epDealerProvider.getRPT05CoinStockInfo(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.coinStockInfo;
        }

      }),err =>{
        console.log(err);
      } 
  }

  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt05ConstockInfoPage');
  }



}

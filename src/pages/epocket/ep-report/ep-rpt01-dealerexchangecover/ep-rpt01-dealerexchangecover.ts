import { Component } from '@angular/core';
import { NavController,AlertController, Platform, LoadingController } from 'ionic-angular';
import * as papa from 'papaparse';
import { EPDealerProvider } from '../../../../providers/epocket/dealer';
import { EpRpt01DealerexchangecoverInfoPage } from '../ep-rpt01-dealerexchangecover-info/ep-rpt01-dealerexchangecover-info';
import { StorageProvider } from '../../../../providers/shared/storage';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CalendarModal } from 'ion2-calendar';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePicker } from '@ionic-native/date-picker';


@Component({
  selector: 'page-ep-rpt01-dealerexchangecover',
  templateUrl: 'ep-rpt01-dealerexchangecover.html',
})
export class EpRpt01DealerexchangecoverPage {

  customerInfo: any;

  csvData : any[] = [];
  headerRow : any[]= [];
  DEALER_ID : string ;
  dealerExchange : any;
  dealerExchange_Excel : any;

  startDate: any;
  endDate: any;
  loading: any;

  constructor(public navCtrl: NavController, 
    private epDealerProvider : EPDealerProvider,
    private storageProvider: StorageProvider,
    private modalCtrl: ModalController,
    private file: File,
    private platform: Platform,
    private alertCtrl : AlertController,
    private fileOpener: FileOpener,
    private datePicker: DatePicker,
    private loadingCtrl: LoadingController
  ) 
  {
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;
    this.startDate = moment().subtract(1, 'months').toDate(); 
    this.endDate = moment().toDate();
     
    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE : moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY')
    };

    this.epDealerProvider.getRPT01DelaerExchangeCover(paraObject)
      .then(res =>{
        this.dealerExchange = res.data.exchangeCover;
      }),err =>{
        console.log(err);
      }

      this.epDealerProvider.GetRPT01DealerExchangeCover_Excel(paraObject)
      .then(res =>{
        this.dealerExchange_Excel = res.data.exchangeCover;
        let csv = papa.unparse(this.dealerExchange_Excel);
        this.extractData(csv);
      }),err =>{
        console.log(err);
      }
  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.dealerExchange == undefined || this.dealerExchange == null || this.dealerExchange.length == 0;
    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }
 
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["วันที่รับ","Sale Name", "ยอดที่ร้านรับ", "ยอดที่ Sale รับ", "ผลต่าง",
                      "ค่าแลก","ค่ารักษาฝาหลังหัก WHT","วันที่สั่งจ่าย","จำนวนรวม"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }

   
 
  downloadCSV() {
    this.showLoading();
    
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานการแลกฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'DealerExchange.csv';
  
    // console.log('applicationDirectory ==> ', this.file.applicationDirectory);
    // console.log('applicationStorageDirectory ==> ', this.file.applicationStorageDirectory);
    // console.log('dataDirectory ==> ', this.file.dataDirectory);
    // console.log('cacheDirectory ==> ', this.file.cacheDirectory);
    // console.log('externalApplicationStorageDirectory ==> ', this.file.externalApplicationStorageDirectory);
    // console.log('externalDataDirectory ==> ', this.file.externalDataDirectory);
    // console.log('externalCacheDirectory ==> ', this.file.externalCacheDirectory);
    // console.log('externalRootDirectory ==> ', this.file.externalRootDirectory);

    this.file.checkFile(path, name)
      .then(() => {
        this.file.writeExistingFile(path, name, csv)
          .then(res => {
            this.fileOpener.open(`${path}/${name}`, 'text/csv')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error openening file', JSON.stringify(e)));
          });
      })
      .catch(err => {
        this.file.writeFile(path, name, csv)
          .then(res => {
            console.log('new file');
            console.log(res);
          });
      });


      this.hideLoading();
  }
 
  ionViewDidLoad() {
  }

  cancel(){
    this.navCtrl.pop();
  }

  // ======= On select Detail =====
  toggleSection(i) {
    this.dealerExchange[i].open = !this.dealerExchange[i].open;
  }
 
  toggleItem(i, j) {
    this.dealerExchange[i].details[j].open = !this.dealerExchange[i].details[j].open;
  }

  onClickDetail(SaleDate,SaleQty,TotalgrandAmount){
       this.navCtrl.push(EpRpt01DealerexchangecoverInfoPage,{
       "SaleDate":SaleDate
      ,"SaleQty":SaleQty
      ,"TotalgrandAmount":TotalgrandAmount
    });
  }

  // ===== Filter ===== //
  onOpenStartDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.startDate = date.dateObj ;
      }
    });
  }

  onOpenEndDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.endDate).toDate(),
        from: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.endDate = date.dateObj;
      }
    });
    
  }
  // ===== End Filter ===== //

  openStartDate() {
    this.datePicker.show({
      date: this.startDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.startDate = date;
      }
    })
    .catch(() => {});
  }

  openEndDate() {
    this.datePicker.show({
      date: this.endDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.endDate = date;
      }
    })
    .catch(() => {});
  }

  //====== Load ====
  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
  }
  
  hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }


}

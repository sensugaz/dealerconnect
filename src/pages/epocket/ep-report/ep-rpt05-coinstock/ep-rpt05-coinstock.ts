import { EPDealerProvider } from './../../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EpRpt05ConstockInfoPage } from './../ep-rpt05-constock-info/ep-rpt05-constock-info';


@Component({
  selector: 'page-ep-rpt05-coinstock',
  templateUrl: 'ep-rpt05-coinstock.html',
})
export class EpRpt05CoinstockPage {

  DataReport : any;
  DataReport_Excel : any;
  DataReportHD : any;
  customerInfo : any;
  DEALER_ID : string;
  DEALER_NAME : string;
  DATE_TO : string;

  SumRAomunt : number;
  SumPAomunt : number;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private storageProvider:StorageProvider
    ,private epDealerProvider:EPDealerProvider
    ,private alertCtrl : AlertController) {

    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE : "",
      TO_DATE : ""
    };

    this.epDealerProvider.getRPT05CoinStock(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.coinStock.length > 0)
          {
            this.DataReportHD = res.data.coinStock[0];
            this.DataReport = res.data.coinStock;
            this.DEALER_NAME = res.data.coinStock[0].dealername;
            this.DATE_TO = res.data.coinStock[0].dealercreatedate;

            this.SumPAomunt = 0 ;
            this.SumRAomunt = 0 ;

            this.DataReport.forEach(element => {
              this.SumRAomunt += parseInt(element.ramount);
              this.SumPAomunt += parseInt(element.pamount);
            });
          }
        }
        this.checkDataEmpty();
        
      }),err =>{
        console.log(err);
      } 


      this.epDealerProvider.getRPT05DealerCoinStock_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          if(res.data.coinStock.length > 0)
          {
            // this.DataReportHD = res.data.coinStock[0];
            this.DataReport_Excel = res.data.coinStock;
            // this.DEALER_NAME = res.data.coinStock[0].dealername;
            // this.DATE_TO = res.data.coinStock[0].dealercreatedate;

            // this.SumPAomunt = 0 ;
            // this.SumRAomunt = 0 ;

            // this.DataReport.forEach(element => {
            //   this.SumRAomunt += parseInt(element.ramount);
            //   this.SumPAomunt += parseInt(element.pamount);
            // });
          }
        }
        this.checkDataEmpty();
        
      }),err =>{
        console.log(err);
      } 
      
  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null ;

    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        // title: 'ePocket Alert',
        subTitle: 'ไม่พบฝาค้างส่ง ',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

  

  DetailOnClick(){
    this.navCtrl.push(EpRpt05ConstockInfoPage);
    
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpRpt05CoinstockPage');
  }

}

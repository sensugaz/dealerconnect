import { Platform } from 'ionic-angular/platform/platform';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { StorageProvider } from '../../../../providers/shared/storage';
import { EpRpt06SalereceiveInfoPage } from '../ep-rpt06-salereceive-info/ep-rpt06-salereceive-info';
import { EPSaleProvider } from '../../../../providers/epocket/sale';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CalendarModal } from 'ion2-calendar';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePicker } from '@ionic-native/date-picker';

@Component({
  selector: 'page-ep-rpt06-salereceive',
  templateUrl: 'ep-rpt06-salereceive.html',
})
export class EpRpt06SalereceivePage {

  userInfo: any;
  username : string;
  DataReport : any;
  DataReport_Excel : any;

  startDate: any;
  endDate: any;

  csvData : any[] = [];
  headerRow : any[]= [];

  alert : any = this.alertCtrl.create();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storageProvider : StorageProvider,
    private epSaleProvider : EPSaleProvider,
    private modalCtrl: ModalController,
    private alertCtrl : AlertController,
    private platform : Platform
    ,private file: File
    ,private fileOpener: FileOpener
    ,private datePicker: DatePicker
    ) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.username = this.userInfo.customerdesc;
    console.log(this.userInfo.userId);

    this.startDate = moment().subtract(1, 'months').toDate(); 
    this.endDate = moment().toDate();
  }

  ionViewDidEnter() {
    this.fatchData();
    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        //this.alert.dismiss();
        this.navCtrl.pop();
      });
    });
   }

  private fatchData(){
    let paraObject = {
      SALE_NAME : this.username,
      FROM_DATE : moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY'),
      SALE_ID : this.userInfo.userId
    };

    this.epSaleProvider.getRPT06SaleReceive(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.saleReceive;
        }
      }),err =>{
        console.log(err);
      } 

       
      this.epSaleProvider.getRPT06SaleReceive_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DataReport_Excel = res.data.saleReceive;
          let csv = papa.unparse(this.DataReport_Excel);
          this.extractData(csv);
        }
      }),err =>{
        console.log(err);
      }
  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.DataReport == undefined || this.DataReport == null || this.DataReport.length == 0;

    if (DataReportIsEmpty) {
      this.alert = this.alertCtrl.create({
          title: 'ePocket Alert',
          subTitle: 'ไม่พบข้อมูล',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'ตกลง',
              handler: () => {
                this.navCtrl.pop();
              }
            }]
        }).present();
    }
  }

  DetailOnClick(DEALER_ID,CREATE_DATE){

    this.navCtrl.push(EpRpt06SalereceiveInfoPage,{"CREATE_DATE":CREATE_DATE,"DEALER_ID":DEALER_ID});
  }

  
  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    this.headerRow = ["วันที่ sale รับ","รหัสร้านค้า","ร้านค้า", "จำนวนฝา"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานการรับฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv);    
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานการรับฝา.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //


  // ===== Filter ===== //
  openStartDate() {
    this.datePicker.show({
      date: this.startDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.startDate = date;
      }
    })
    .catch(() => {});
  }

  openEndDate() {
    this.datePicker.show({
      date: this.endDate,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      locale: 'TH',
      okText: 'ตกลง',
      cancelText: 'ยกเลิก',
    })
    .then((date) => {
      if (date != undefined || date != null) {
        this.endDate = date;
      }
    })
    .catch(() => {});
  }


  // ===== End Filter ===== //

  cancel(){
    this.navCtrl.pop();
  }
  
   // ======= On select Detail =====
   toggleSection(i) {
    this.DataReport[i].open = !this.DataReport[i].open;
  }
 
  toggleItem(i, j) {
    this.DataReport[i].details[j].open = !this.DataReport[i].details[j].open;
  }

  ionViewDidLoad() {
  }

}

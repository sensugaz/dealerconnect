import { Component } from '@angular/core';
import { NavController,AlertController, Platform } from 'ionic-angular';
import * as papa from 'papaparse';
import { StorageProvider } from './../../../../providers/shared/storage';
import { EPDealerProvider } from '../../../../providers/epocket/dealer';
import { EpRpt02DealerredeempainterInfoPage } from './../ep-rpt02-dealerredeempainter-info/ep-rpt02-dealerredeempainter-info';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CalendarModal } from 'ion2-calendar';
import { EpPainterPage } from '../../ep-painter/ep-painter';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

@Component({
  selector: 'page-ep-rpt02-dealerredeempainter',
  templateUrl: 'ep-rpt02-dealerredeempainter.html',
})

export class EpRpt02DealerredeempainterPage {

  DealerRedeemPainter : any;
  DealerRedeemPainter_Excel : any;
  customerInfo: any;
  DEALER_ID : string;
  csvData : any[] = [];
  headerRow : any[]= [];
  startDate: any;
  endDate: any;
  PainiterInfo : any;
  PainterID : number = 0;

  PainterNo : string;
  TitleName : string;
  FirstName : string;
  LastName : string;
  FullName : string;


  constructor(public navCtrl: NavController, 
    private epDealerProvider : EPDealerProvider,
    private storageProvider: StorageProvider,
    private modalCtrl: ModalController,
    private alertCtrl : AlertController,
    private platform : Platform,
    private file: File,
    private fileOpener: FileOpener
  ) 
  {
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.DEALER_ID = this.customerInfo.customerCode;
    this.startDate = moment().subtract(1, 'months').toDate(); 
    this.endDate = moment().toDate();

    this.fatchData();
  }

  private fatchData(){
    let paraObject = {
      DEALER_ID : this.DEALER_ID,
      FROM_DATE : moment(this.startDate).format('DD-MM-YYYY'),
      TO_DATE : moment(this.endDate).format('DD-MM-YYYY'),
      PAINTER_ID : this.PainterID.toString()
    };

    this.epDealerProvider.getRPT02DealerRedeemByPainter(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DealerRedeemPainter = res.data.dealerRedeemPainter;
          //let csv = papa.unparse(this.DealerRedeemPainter);
          // this.extractData(csv);
          // this.checkDataEmpty();
        }
      }),err =>{
        console.log(err);
      }

      this.epDealerProvider.getRPT02DealerRedeemByPainter_Excel(paraObject)
      .then(res =>{
        if (res.result == 'SUCCESS') {
          this.DealerRedeemPainter_Excel = res.data.dealerRedeemPainter;
          console.log(this.DealerRedeemPainter_Excel);
          let csv = papa.unparse(this.DealerRedeemPainter_Excel);
          this.extractData(csv);
          // this.checkDataEmpty();
        }
      }),err =>{
        console.log(err);
      }
 
  }

  checkDataEmpty(){
    let DataReportIsEmpty = this.DealerRedeemPainter == undefined || this.DealerRedeemPainter == null || this.DealerRedeemPainter.length == 0;
    if (DataReportIsEmpty) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูล',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
      }).present();
    }
  }

 // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    //this.headerRow = parsedData[1];
    this.headerRow = ["เบอร์ช่าง","ชื่อช่าง", "วันที่ช่างแลก", "จำนวนฝา", "จำนวนเงิน"]
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "รายงานช่างสีที่แลกฝา.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv)
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'รายงานช่างสีที่แลกฝา.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //
 
  ionViewDidLoad() {
  }

  cancel(){
    this.navCtrl.pop();
  }

  // ======= On select Detail =====
  toggleSection(i) {
    this.DealerRedeemPainter[i].open = !this.DealerRedeemPainter[i].open;
  }
 
  toggleItem(i, j) {
    this.DealerRedeemPainter[i].details[j].open = !this.DealerRedeemPainter[i].details[j].open;
  }

  onClickDetail(PainterNo,PainterName,DealerCode,DealerCreateDate){
    this.navCtrl.push(EpRpt02DealerredeempainterInfoPage,{"PainterNo":PainterNo
    ,"PainterName":PainterName,"DealerCode":DealerCode,"DealerCreateDate":DealerCreateDate});
  }

    

  // ===== Filter ===== //
  onOpenStartDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.startDate = date.dateObj ;
      }
    });
  }

  onOpenEndDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.endDate).toDate(),
        from: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.endDate = date.dateObj;
      }
    });
  }
    // ===== End Filter ===== //

    PainterSelect(){
      new Promise((painterObject, reject) => {
        this.navCtrl.push(EpPainterPage, { painterObject: painterObject });
      }).then(data => {
        this.PainiterInfo = data;
        this.PainterNo = this.PainiterInfo.painterno;
        this.TitleName = this.PainiterInfo.titlename;
        this.FirstName = this.PainiterInfo.firstname;
        this.LastName = this.PainiterInfo.lastname;
        this.PainterID = parseInt(this.PainiterInfo.id);

        this.FullName = this.PainterNo;
        if(this.TitleName.length > 0 || this.FirstName.length > 0 || this.LastName.length > 0)
        this.FullName += ' (';

        this.FullName += this.TitleName +' '+ this.FirstName + ' '+ this.LastName;

        if(this.TitleName.length > 0 || this.FirstName.length > 0 || this.LastName.length > 0)
        this.FullName += ')';

      });
    }

}

import { EpProductBrandSizeListPage } from './../ep-product-brand-size-list/ep-product-brand-size-list';
import { Component } from '@angular/core';
import { LoadingController , NavController, NavParams, App } from 'ionic-angular';
import { EPProductProvider } from '../../../providers/epocket/product';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EpHomePage } from '../ep-home/ep-home';
import { EpCartPage } from '../ep-cart/ep-cart';
import { StorageProvider } from '../../../providers/shared/storage';

/**
 * Generated class for the EpProductBrandListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ep-product-brand-list',
  templateUrl: 'ep-product-brand-list.html',
})
export class EpProductBrandListPage {

  products: Array<any>;
  loading: any;
  config: any;
  painter: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public app: App ,
              public loadingCtrl: LoadingController,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              public epProductProvider: EPProductProvider) {
              
              this.config = this.storageProvider.getConfig();
              this.painter = this.navParams.get('painter');
              //console.log('brand >> ',this.painter);
  }

  ionViewDidLoad() {
    this.fetchProductBrand();
  }

  fetchProductBrand() {

    // console.log('Owen In Loop ==> ');

    // this.showLoading();

    this.epProductProvider.getProductBrand()
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.products = res.data.productBrand;
        }
        //console.log(this.products);
        // this.hideLoading();
      });
  }

  // showLoading() {
	// 	if (!this.loading) {
	// 		this.loading = this.loadingCtrl.create({
	// 			content: 'กำลังโหลด...'
	// 		});

	// 		this.loading.present();
	// 	}
	// }

	// hideLoading() {
	// 	if (this.loading) {
	// 		this.loading.dismiss();
	// 		this.loading = null;
	// 	}
  // }

  onSelectProductBrand(item) {
    //this.navCtrl.push, EpProductBrandSizeListPage, { productBrand: item}
    this.navCtrl.push(EpProductBrandSizeListPage, { productBrand : item , painter : this.painter}, {
			animate: true,
			animation: 'ease-in'
		});
  }

  gotoEPHome(){
    this.navCtrl.setRoot(EpHomePage);
  }

  gotoCart(){
    this.navCtrl.setRoot(EpCartPage , {painter : this.painter});
  }

}

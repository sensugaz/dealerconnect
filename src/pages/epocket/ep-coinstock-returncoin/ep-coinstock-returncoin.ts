import { EpHomeCoinstockPage } from './../ep-home-coinstock/ep-home-coinstock';
import { EpHomePage } from './../ep-home/ep-home';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { StorageProvider } from '../../../providers/shared/storage';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { Platform } from 'ionic-angular/platform/platform';

@Component({
  selector: 'page-ep-coinstock-returncoin',
  templateUrl: 'ep-coinstock-returncoin.html',
})
export class EpCoinstockReturncoinPage {

  loading: any;
  DataHD: any = {};
  DataDT: any;
  dealerID: string;
  customerInfo: any;
  userInfo: any;
  remarkText: string = '';
  SumCoin_: number = 0;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public storageProvider: StorageProvider,
    private toastCtrl: ToastController,
    public epSaleProvider: EPSaleProvider,
    private platform: Platform
  ) {
    
    
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.dealerID = this.customerInfo.customerCode;
    this.userInfo = this.storageProvider.getUserInfo();

    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    this.fetchDataInfo();
  }

  checkDataEmpty() {
    let HDIsEmpty = this.DataHD.dealercode == undefined || this.DataHD.dealercode == null;
    let DTIsEmpty = this.DataDT == undefined || this.DataDT == null;

    if ((HDIsEmpty) && (DTIsEmpty)) {
      this.alertCtrl.create({
        title: 'ePocket Alert',
        subTitle: 'ไม่พบข้อมูลการส่งฝาคืน',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ตกลง',
            handler: () => {
              this.navCtrl.popToRoot();
            }
          }]
      }).present();
    }
  }

  fetchDataInfo() {

    this.showLoading();
    this.epSaleProvider.getSaleRedeemReceive(this.userInfo.userId)
      .then(res => {
        if (res.result == 'SUCCESS') {
          if (res.data.saleRedeemReceive.length > 0 && res.data.saleRedeemReceive.length != undefined) {
            this.DataHD = res.data.saleRedeemReceive[0];
            this.DataDT = res.data.saleRedeemReceive;
            this.sumCoin(this.DataDT);
          }
            this.checkDataEmpty();
        }
        this.hideLoading();
      }
      );
  }

  sumCoin(saleRedeemReceive) {
    let t = 0;
    saleRedeemReceive.forEach((value, key) => {
      this.SumCoin_ += value.saleqty;
    });
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });
      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ReturnCoin() {
    let alert = this.alertCtrl.create({
      title: 'ยืนยันการส่งฝาคืน ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.savedata();
          }
        }
      ]
    });
    alert.present();
  }

  savedata() {
    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    this.epSaleProvider.updSaleRedeemReceive(this.userInfo.userId)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomeCoinstockPage);

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomeCoinstockPage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }

      this.hideLoading();
    }
    );

  }

}
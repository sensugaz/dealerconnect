import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EpChangeprofileHistoryProcessPage } from '../ep-changeprofile-history-process/ep-changeprofile-history-process';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'page-ep-changeprofile-history-main',
  templateUrl: 'ep-changeprofile-history-main.html',
})
export class EpChangeprofileHistoryMainPage {
  page : number
  userInfo: any;
  customerInfo: any;
  DEALER_ID : string
  DataChangeProfileRequest : Array<any>
  DataChangeProfileProcess : Array<any>

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private epSaleProvider :EPSaleProvider,
    private storageProvider: StorageProvider) {
    
      this.page = 0;
      this.userInfo = this.storageProvider.getUserInfo();
      this.customerInfo = this.storageProvider.getCustomerInfo();
      this.DEALER_ID = this.customerInfo.customerCode;
    
  }

  ionViewDidLoad() {

    this.fetchChangeProfileRequest();
    this.fetchChangeProfileProcess();
  }

  fetchChangeProfileRequest() {
    let object_ = {
      DEALER_ID : this.DEALER_ID
    }
    this.epSaleProvider.getProfileHistoryRequest(object_)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.DataChangeProfileRequest = res.data.changeProfile;       
      }
    });
  }

  fetchChangeProfileProcess() {
    let object_ = {
      DEALER_ID : this.DEALER_ID
    }
    this.epSaleProvider.getProfileHistoryProcess(object_)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.DataChangeProfileProcess = res.data.changeProfile;       
      }
    });
  }

  onSelectedPage(index) {
    this.page = index;
  }

}

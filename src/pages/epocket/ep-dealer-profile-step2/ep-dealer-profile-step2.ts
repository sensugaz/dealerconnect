import { EpHomePage } from './../ep-home/ep-home';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { StorageProvider } from './../../../providers/shared/storage';

@Component({
  selector: 'page-ep-dealer-profile-step2',
  templateUrl: 'ep-dealer-profile-step2.html',
})
export class EpDealerProfileStep2Page {

  userInfo: any;
  customerInfo: any;

  inputAccountName: string;
  inputBranch: string;
  inputBankCode: string;
  inputBankName: string;
  inputBankAccount: string;

  inputTaxno: string;
  inputAddresstax: string;
  inputMobile: string;
  inputIswht: string;

  DEALER_ID: string;
  ActionBy: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storageProvider: StorageProvider,
    private epSaleProvider: EPSaleProvider,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {

    this.inputAccountName = this.navParams.get('accountname');
    this.inputBranch = this.navParams.get('branch');
    this.inputBankCode = this.navParams.get('bankcode');
    this.inputBankName = this.navParams.get('bankname');
    this.inputBankAccount = this.navParams.get('bankaccount');

    this.inputTaxno = this.navParams.get('taxno');
    this.inputAddresstax = this.navParams.get('addresstax');
    this.inputMobile = this.navParams.get('mobile');
    this.inputIswht = this.navParams.get('iswht');

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.DEALER_ID = this.customerInfo.customerCode;
    this.ActionBy = this.userInfo.userId;

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EpChangeprofileStep2Page');
  }

  back() {
    this.navCtrl.pop();
  }

  saveOnClick() {
    let alert = this.alertCtrl.create({
      title: 'ยืนยันการแก้ไขข้อมูล ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.savedata();
          }
        }
      ]
    });
    alert.present();
  }

  savedata() {
    let objChangeProfile = {
      ACCOUNT_NAME: this.inputAccountName,
      BANK_BRANCH: this.inputBranch,
      BANK_KEY: this.inputBankCode,
      BANK_NAME: this.inputBankName,
      BANK_ACCOUNT: this.inputBankAccount,
      TAX_NO: this.inputTaxno,
      ADDRESS_TAX: this.inputAddresstax,
      MOBILE_WEB: this.inputMobile,
      IS_WHT: Number(this.inputIswht),
      DEALER_ID: String(this.DEALER_ID),
      LAST_UPDATE_BY: String(this.ActionBy)
    }

    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    this.epSaleProvider.updProfile(objChangeProfile)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomePage);

        toast.onDidDismiss(() => {
          //console.log('Dismissed toast');
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomePage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
       
    }
    );

  }

}


import { EPSaleProvider } from './../../../providers/epocket/sale';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from './../../../providers/shared/storage';
import { Camera } from '@ionic-native/camera';
import { EpRecipienttypePage } from '../ep-recipienttype/ep-recipienttype';
import { EpChangeprofileStep3Page } from '../ep-changeprofile-step3/ep-changeprofile-step3';

@Component({
  selector: 'page-ep-changeprofile-step2',
  templateUrl: 'ep-changeprofile-step2.html',
})
export class EpChangeprofileStep2Page {

  userInfo: any;
  customerInfo: any;


  // inputGalleryDoc1: any = "";
  // inputGalleryDoc2: any = "";

  inputAccountName: string = "";
  inputBankKey: string  = "";
  inputBankName: string  = "";
  inputBankBranchCode = "";
  inputBankBranchName = "";
  inputBankAccount: string  = "";

  inputRecipienttype : string = "";
  inputRecipienttypeName : string = "";
  inputTaxno : string  = "";
  inputMobile : string  = "";
  inputMobileWeb : string  = "";
  inputEmail : string = "";

  inputAddresstax : string  = "";
  inputstreet4 : string= "";
  inputCityCode : string = "";
  inputCityDesc : string = "";
  inputDistrictCode : string = "";
  inputDistrictDesc : string = "";
  inputIswht : string  = "";
  inputstreet5 : string= "";
  inputPostCode : string = "";
  
  inputImgbookbank : string ="";
  inputImgidcard : string ="";

  inputAccountNameOld = "";
  inputBranchOld: string  = "";  
  inputBankCodeOld: string  = "";
  inputBankNameOld: string  = "";
  inputBankAccountOld: string  = "";
  isnew: number = 0;
  isChange: number;

  DEALER_ID: string;
  ActionBy: string;

  Doc1base64: any = "";
  Doc2base64: any = "";

  RecipientTypeInfo: any;
  // inputRecipientValue: string = "";
  // inputRecipientName: string = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storageProvider: StorageProvider,
    public camera: Camera
  ) {

    this.inputAccountName = this.navParams.get('accountname');
    this.inputBankKey = this.navParams.get('bankkey');
    this.inputBankName = this.navParams.get('bankname');
    this.inputBankBranchCode = this.navParams.get('bankbranchcode');
    this.inputBankBranchName = this.navParams.get('bankbranchname');
    this.inputBankAccount = this.navParams.get('bankaccount');

    this.inputRecipienttype = this.navParams.get('recipienttype');
    this.inputTaxno = this.navParams.get('taxno');
    this.inputMobile = this.navParams.get('mobile');
    this.inputMobileWeb = this.navParams.get('mobileweb');
    this.inputEmail = this.navParams.get('email');
    
    this.inputAddresstax = this.navParams.get('addresstax');
    this.inputstreet4 = this.navParams.get('street4');
    this.inputCityCode = this.navParams.get('citycode');
    this.inputCityDesc = this.navParams.get('citydesc');
    this.inputDistrictCode = this.navParams.get('districtcode');
    this.inputDistrictDesc = this.navParams.get('districtdesc');
    this.inputIswht = this.navParams.get('iswht');
    this.inputstreet5 = this.navParams.get('street5');
    this.inputPostCode = this.navParams.get('postcode');
    
    this.inputImgbookbank = this.navParams.get('imgbookbank');
    this.inputImgidcard = this.navParams.get('imgidcard');
    this.isnew = this.navParams.get('isnew');
    this.isChange = this.navParams.get('isChange');
    console.log("Page2 ",this.isChange);
    this.inputImgbookbank =  this.navParams.get('imgbookbank');
    this.inputImgidcard =  this.navParams.get('imgidcard');

    if (this.inputRecipienttype == '03')
      this.inputRecipienttypeName = 'บุคคลธรรมดา';
    else if (this.inputRecipienttype == '53')
      this.inputRecipienttypeName = 'นิติบุคคล';

    //this.Doc1base64 = this.navParams.get('imgbookbank') != '' ? 'data:image/jpeg;base64,' + this.navParams.get('imgbookbank') : '';
    //this.Doc2base64 = this.navParams.get('imgidcard') != '' ? 'data:image/jpeg;base64,' + this.navParams.get('imgidcard') : '';

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.DEALER_ID = this.customerInfo.customerCode;
    this.ActionBy = this.userInfo.userId;
  }

  ionViewDidLoad() {
  }

  back() {
    this.navCtrl.pop();
  }

  saveOnClick() {
    //this.savedata();

    //this.ConfirmPassword();
    // let alert = this.alertCtrl.create({
    //   title: 'ยืนยันการแก้ไขข้อมูล ?',
    //   mode: 'ios',
    //   enableBackdropDismiss: false,
    //   buttons: [
    //     {
    //       text: 'ยกเลิก',
    //       role: 'cancel',
    //       cssClass: 'danger',
    //       handler: () => {
    //         console.log('Cancel clicked');
    //       }
    //     },
    //     {
    //       text: 'ยืนยัน',
    //       handler: () => {
    //         this.savedata();
    //       }
    //     }
    //   ]
    // });
    // alert.present();
  }

  // ConfirmPassword() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Login',
  //     enableBackdropDismiss: false,
  //     inputs: [
  //       {
  //         name: 'password',
  //         placeholder: 'Password',
  //         type: 'password'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: data => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Login',
  //         handler: data => {
  //           this.savedata();
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  // }

  // savedata() {
  //   let objChangeProfile = {
  //     ACCOUNT_NAME: this.inputAccountName,
  //     BANK_BRANCH: this.inputBranch,
  //     BANK_KEY: this.inputBankCode,
  //     BANK_NAME: this.inputBankName,
  //     BANK_ACCOUNT: this.inputBankAccount,
  //     TAX_NO: this.inputTaxno,
  //     ADDRESS_TAX: this.inputAddresstax,
  //     MOBILE_WEB: this.inputMobile,
  //     IS_WHT: 1,
  //     DEALER_ID: String(this.DEALER_ID),
  //     LAST_UPDATE_BY: String(this.ActionBy),
  //     IMG1: this.Doc1base64.replace('data:image/jpeg;base64,', ''),
  //     IMG2: this.Doc2base64.replace('data:image/jpeg;base64,', ''),
  //     RECIPIENT_TYPE: this.inputRecipientValue
  //   }

  //   let toast = this.toastCtrl.create({
  //     message: 'บันทึกข้อมูลสำเร็จ',
  //     duration: 2000,
  //     position: 'bottom'
  //   });


  //   let check = this.validate(objChangeProfile);
  //   if (check) {
  //     this.epSaleProvider.updProfile(objChangeProfile)
  //       .then(res => {
  //         if (res.result == 'SUCCESS') {
  //           this.sendMail();
  //           this.navCtrl.setRoot(EpHomePage);

  //           toast.onDidDismiss(() => {
  //             console.log('Dismissed toast');
  //           });

  //           setTimeout(() => {
  //             this.navCtrl.setRoot(EpHomePage);
  //           }, 1000);

  //           toast.present();

  //         } else {
  //           this.alertCtrl.create({
  //             title: 'แจ้งเตือน',
  //             subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
  //             mode: 'ios',
  //             enableBackdropDismiss: false,
  //             buttons: ['ตกลง']
  //           }).present();
  //         }
  //       }
  //       ), err => {
  //         console.log(err);
  //       }
  //   } else {
  //     this.alertCtrl.create({
  //       title: 'แจ้งเตือน',
  //       subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบไฟล์อัพโหลดรูปภาพ',
  //       mode: 'ios',
  //       enableBackdropDismiss: false,
  //       buttons: ['ตกลง']
  //     }).present();
  //   }
  // }

  // sendMail() {
  //   let UserName = this.userInfo.userName;
  //   let email = this.userInfo.email;

  //   let ParaObject = {
  //     userName: UserName,
  //     Email: email
  //   }

  //   this.epSaleProvider.updProfileSendMail(ParaObject)
  //     .then(res => {
  //       if (res.result == 'SUCCESS') {
  //       } else {
  //       }
  //     }
  //     ), err => {
  //       console.log(err);
  //     }

  // }

  // validate(objChangeProfile) {
  //   let check = true;

  //   if (this.ischange == 1) {
  //     if (objChangeProfile.IMG1.length < 50 || objChangeProfile.IMG2.length < 50) {
  //       check = false;
  //     }
  //     else
  //       check = true
  //   } else
  //     check = true;

  //   console.log(check);
  //   return check;
  // }

  // accessGalleryDoc1() {
  //   this.camera.getPicture({
  //     sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
  //     destinationType: this.camera.DestinationType.DATA_URL
  //   }).then((imageData) => {
  //     this.Doc1base64 = 'data:image/jpeg;base64,' + imageData;
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  // clearGalleryDoc1() {
  //   this.Doc1base64 = '';
  // }


  // accessGalleryDoc2() {
  //   this.camera.getPicture({
  //     sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
  //     destinationType: this.camera.DestinationType.DATA_URL
  //   }).then((imageData) => {
  //     this.Doc2base64 = 'data:image/jpeg;base64,' + imageData;
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  // clearGalleryDoc2() {
  //   this.Doc2base64 = '';
  // }

  BankSelect() {
    new Promise((RecipientObject, reject) => {
      this.navCtrl.push(EpRecipienttypePage, { RecipientObject: RecipientObject });
    }).then(data => {
      this.RecipientTypeInfo = data;
      this.inputRecipienttype = this.RecipientTypeInfo.value;
      this.inputRecipienttypeName = this.RecipientTypeInfo.recipientname;
    });
  }


  next() {
    this.navCtrl.push(EpChangeprofileStep3Page,{
      "accountname" : this.inputAccountName,
      "bankkey" : this.inputBankKey,
      "bankname" : this.inputBankName,
      "bankbranchcode" : this.inputBankBranchCode,
      "bankbranchname" : this.inputBankBranchName,
      "bankaccount" : this.inputBankAccount,

      "recipienttype" : this.inputRecipienttype,
      "taxno": this.inputTaxno,
      "mobile": this.inputMobile,
      "mobileweb": this.inputMobileWeb,
      "email": this.inputEmail,
      
      "addresstax": this.inputAddresstax,
      "street4": this.inputstreet4,
      "citycode": this.inputCityCode,
      "citydesc": this.inputCityDesc,
      "districtcode": this.inputDistrictCode,
      "districtdesc": this.inputDistrictDesc,
      "iswht": this.inputIswht,
      "street5": this.inputstreet5,
      "postcode" : this.inputPostCode,

      "imgbookbank" : this.inputImgbookbank,
      "imgidcard" : this.inputImgidcard,

      "isnew" : this.isnew,
      "isChange" : this.isChange
    });
  }

}

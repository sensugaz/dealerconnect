import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { EPDealerProvider } from '../../../providers/epocket/dealer';

@Component({
  selector: 'page-ep-dealer-history-info',
  templateUrl: 'ep-dealer-history-info.html',
})
export class EpDealerHistoryInfoPage {

  loading: any;
  HistoryHD: any;
  HistoryInfo: any;
  RefDocNo: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public epDealerProvider: EPDealerProvider) {

              // this.RefDocNo = this.navParams.get('refdocno');
              this.HistoryHD = this.navParams.get('refdocno');

              //console.log(this.RefDocNo);
  }

  ionViewDidLoad() {
    this.fetchDataInfo(this.RefDocNo);
  }

  fetchDataInfo(DataHD) {

    //console.log(DataHD);
    // console.log('HistoryHD >>> ');
    // console.log(this.HistoryHD);

    this.showLoading();

    this.epDealerProvider.getRedeemHistoryInfo(this.HistoryHD.refdocno)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.HistoryInfo = res.data.redeemHistoryInfo;
          console.log(res.data);
        }
        this.hideLoading();
      });
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

}

import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/eordering/config';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { StorageProvider } from '../../../providers/shared/storage';

@Component({
  selector: 'page-ep-coinstock-product-info',
  templateUrl: 'ep-coinstock-product-info.html',
})
export class EpCoinstockProductInfoPage {
  inputQty: number;
  BTFS: string;
  loading: any;
  config: any;
  productInfo: any = {};
  userInfo: any;
  DEALERID: string;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private loadingCtrl: LoadingController,
              private configProvider: ConfigProvider,
              private epSaleProvider: EPSaleProvider,
              private storageProvider: StorageProvider,
              private alertCtrl: AlertController) {

    this.BTFS = this.navParams.get('BTFS');
    this.DEALERID = this.navParams.get('DEALERID');

    this.config = this.storageProvider.getConfig();
    this.userInfo = this.storageProvider.getUserInfo();
  }

  ionViewDidLoad() {
    this.fetchProductInfo(this.BTFS);
  }

  cancel() {
    this.navCtrl.pop();
  }


  fetchProductInfo(BTFS: string) {
    this.showLoading();
    //epSaleProvider

    this.epSaleProvider.getSaleProductInfo(BTFS,this.DEALERID)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.productInfo = res.data.productInfo[0];

        if(this.productInfo.saleqty != 0)
        this.inputQty = this.productInfo.saleqty;
        
        
      }
      this.hideLoading();
    });
  }

  editQty() {
    if (this.inputQty > this.productInfo.redeemqty) {
      let alert = this.alertCtrl.create({
        title: 'แจ้งเตือน',
        subTitle: 'ไม่สามารถรับเกินจำนวนได้',
        mode: 'ios',
        cssClass: 'alertCustomCss',
        buttons: [
          {
            text: 'ตกลง'
          }
        ]
      }).present();
    } else {
      this.funcEditQty();
    }

    // this.alertCtrl.create({
    //   title: 'ยืนยันข้อมูล ?',
    //   subTitle: 'ต้องการแก้ไข จำนวนรับเหรียญ',
    //   buttons: [{ text: 'ยกเลิก' },
    //   {
    //     text: 'ตกลง',
    //     handler: () => {
    //       this.funcEditQty(); //edit qty
    //     }
    //   }
    //   ]
    // }).present();
  }

  funcEditQty() {
    let productData = {
      BTFS: this.productInfo.btfs,
      DealerID: this.DEALERID,
      SALE_QTY: this.inputQty,
      Username: this.userInfo.userName
    };

    if (productData.SALE_QTY == undefined)
      productData.SALE_QTY = 0;
    this.epSaleProvider.UpdSaleRedeemQty(productData)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.navCtrl.pop();
        }
        this.hideLoading();
      });
  }



  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'กำลังโหลด...'
      });

      this.loading.present();
    }
  }

  hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }


}

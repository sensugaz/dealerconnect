import { EPProductProvider } from './../../../providers/epocket/product';
import { Component } from '@angular/core';
import { LoadingController,NavController, NavParams, AlertController, ToastController, Platform } from 'ionic-angular';
import { EpHomePage } from '../ep-home/ep-home';
import { ConfigProvider } from '../../../providers/eordering/config';
import { StorageProvider } from '../../../providers/shared/storage';
import { EpProductBrandListPage } from '../ep-product-brand-list/ep-product-brand-list';

@Component({
  selector: 'page-ep-confirm-phone-no',
  templateUrl: 'ep-confirm-phone-no.html',
})
export class EpConfirmPhoneNoPage {
  numberText: string = '';
  loading: any;
  config: any;
  userInfo: any;
  customerInfo: any;

  cartHD: any = { };
  cartDT: Array<any>;
  cartGroupDT: Array<any>;
  dealerID: string;
  painter: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public epProductProvider: EPProductProvider,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private platform: Platform) {
      
      this.config = this.storageProvider.getConfig();
      this.userInfo = this.storageProvider.getUserInfo();
      // this.cartHD = this.navParams.get('cartHD');
      // this.cartDT = this.navParams.get('cartDT');
      // this.cartGroupDT = this.navParams.get('cartGroupDT');
      this.customerInfo = this.storageProvider.getCustomerInfo();
      this.dealerID = this.customerInfo.customerCode ;
      this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpConfirmPhoneNoPage');
    
  }

  selectedNumber(number) {

    if (number === 'skip'){
      this.numberText = '' ; // this is dummy
      this.save('9999999999');
    }else{
      if (number === 'delete') {
        this.numberText = this.numberText.substr(0, this.numberText.length - 1);
      } else if (this.numberText.length < 12) {
        this.numberText += (this.numberText.length !== 3 && this.numberText.length !== 7) ? number : '-' + number;
      }
    }

    if ((this.numberText.length === 1) && (this.numberText !== '0')) {
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        subTitle: 'กรุณาระบุ เบอร์โทรศัพท์ ให้ถูกต้อง',
        mode: 'ios',
        enableBackdropDismiss: false,
        buttons: ['ตกลง']
      }).present();
      this.numberText = '';
    }

  }

  cancel() {
    this.navCtrl.setRoot(EpHomePage);
  }

  onSave() {

    if (this.numberText.length !== 12) {
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        subTitle: 'กรุณาระบุ เบอร์โทรศัพท์ 10 หลักให้ครบถ้วน',
        enableBackdropDismiss: false,
        buttons: ['ตกลง']
      }).present();
    }else{
      let alert = this.alertCtrl.create({
        title: 'ยืนยันข้อมูล ?',
        mode: 'ios',
        cssClass: 'alertCustomCss',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'ยกเลิก',
            role: 'cancel',
            cssClass: 'danger',
            handler: () => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              this.save(this.numberText);
            }
          }
        ]
      });
  
      let message = '<p>เบอร์โทร: ' + this.numberText + '</p>'
      alert.setMessage(message);
      alert.present();
    }

  }

  save(phonenumber){

    // let MobileNo = this.numberText ;

    // if (this.numberText === ''){
    //   let MobileNo = '9999999999' ;
    // }

    let MobileNo = phonenumber ;
    let Username = this.userInfo.userName ;

    this.epProductProvider.getPainter(MobileNo,Username)
      .then(res => {
      if (res.result == 'SUCCESS') {
        this.painter = res.data.painter[0];
        this.navCtrl.push(EpProductBrandListPage,{ painter : this.painter});
      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    });
    
  }

  saveOld() {
    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });

    let formData = {
			DealerID: this.dealerID,
			MobileNo: this.numberText,
			Username: this.userInfo.userName
    };
    
    this.epProductProvider.Checkout(formData)
      .then(res => {
      if (res.result == 'SUCCESS') {
        this.navCtrl.setRoot(EpHomePage);

        toast.onDidDismiss(() => {
          //console.log('Dismissed toast');
        });

        setTimeout(() => {
          this.navCtrl.setRoot(EpHomePage);
        }, 1000);

        toast.present();

      } else {
        this.alertCtrl.create({
          title: 'แจ้งเตือน',
          subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
          mode: 'ios',
          enableBackdropDismiss: false,
          buttons: ['ตกลง']
        }).present();
      }
    });
    
  }

}

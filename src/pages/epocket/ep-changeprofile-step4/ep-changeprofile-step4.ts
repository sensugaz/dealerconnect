import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController,Platform } from 'ionic-angular';
import { StorageProvider } from './../../../providers/shared/storage';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { Camera} from '@ionic-native/camera';
import { EpHomePage } from '../ep-home/ep-home';

@Component({
  selector: 'page-ep-changeprofile-step4',
  templateUrl: 'ep-changeprofile-step4.html',
})
export class EpChangeprofileStep4Page {


  userInfo: any;
  customerInfo: any;

  //page 1  
  inputAccountName: string = "";
  inputBankKey: string = "";
  inputBankName: string = "";
  inputBankBranchCode = "";
  inputBankBranchName = "";
  inputBankAccount: string = "";

  //page 2  
  inputRecipienttype: string = "";
  inputRecipienttypeName: string = "";
  inputTaxno: string = "";
  inputMobile: string = "";
  inputMobileWeb: string = "";
  inputEmail: string = "";

  //page 3  
  inputAddresstax: string = "";
  inputstreet4: string = "";
  inputCityCode: string = "";
  inputCityDesc: string = "";
  inputDistrictCode: string = "";
  inputDistrictDesc: string = "";
  inputIswht: string = "";
  inputstreet5: string = "";
  inputPostCode: string = "";

  //page 4
  inputImgbookbank: string = "";
  inputImgidcard: string = "";

  //check edit bank
  inputAccountNameOld = "";
  inputBranchOld: string = "";
  inputBankCodeOld: string = "";
  inputBankNameOld: string = "";
  inputBankAccountOld: string = "";
  isnew: number = 0;
  isChange: number;

  Doc1base64: any = "";
  Doc2base64: any = "";

  DEALER_ID: string;
  ActionBy: string;

  alertErrorEmpty : any = this.alertCtrl.create();
  alertPresented: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storageProvider: StorageProvider,
              private epSaleProvider: EPSaleProvider,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private platform: Platform,
              public camera: Camera) {

    this.inputAccountName = this.navParams.get('accountname');
    this.inputBankKey = this.navParams.get('bankkey');
    this.inputBankName = this.navParams.get('bankname');
    this.inputBankBranchCode = this.navParams.get('bankbranchcode');
    this.inputBankBranchName = this.navParams.get('bankbranchname');
    this.inputBankAccount = this.navParams.get('bankaccount');

    this.inputRecipienttype = this.navParams.get('recipienttype');
    this.inputTaxno = this.navParams.get('taxno');
    this.inputMobile = this.navParams.get('mobile');
    this.inputMobileWeb = this.navParams.get('mobileweb');
    this.inputEmail = this.navParams.get('email');

    this.inputAddresstax = this.navParams.get('addresstax');
    this.inputstreet4 = this.navParams.get('street4');
    this.inputCityCode = this.navParams.get('citycode');
    this.inputCityDesc = this.navParams.get('citydesc');
    this.inputDistrictCode = this.navParams.get('districtcode');
    this.inputDistrictDesc = this.navParams.get('districtdesc');
    this.inputIswht = this.navParams.get('iswht');
    this.inputstreet5 = this.navParams.get('street5');
    this.inputPostCode = this.navParams.get('postcode');

    this.inputImgbookbank = this.navParams.get('imgbookbank');
    this.inputImgidcard = this.navParams.get('imgidcard');
    this.isnew = this.navParams.get('isnew');
    this.isChange = this.navParams.get('isChange');
    console.log("Page4 ",this.isChange);          
    this.inputImgbookbank = this.navParams.get('imgbookbank');
    this.inputImgidcard = this.navParams.get('imgidcard');

    this.Doc1base64 = this.navParams.get('imgbookbank') != '' ? 'data:image/jpeg;base64,' + this.navParams.get('imgbookbank') : '';
    this.Doc2base64 = this.navParams.get('imgidcard') != '' ? 'data:image/jpeg;base64,' + this.navParams.get('imgidcard') : '';

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();

    this.DEALER_ID = this.customerInfo.customerCode;
    this.ActionBy = this.userInfo.userId;
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter(){
    this.backButtonAndroid();
  }
  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (this.alertPresented) {
          this.alertPresented = false;
          this.alertErrorEmpty.dismiss();
        } else {
          this.navCtrl.pop();
        }
      });
    });
   }

  saveOnClick() {

    let alert = this.alertCtrl.create({
      title: 'ยืนยันการแก้ไขข้อมูล ?',
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'danger',
          handler: () => {
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.savedata();
          }
        }
      ]
    });
    alert.present();
  }

  savedata() {

    let objChangeProfile = {
      ACCOUNT_NAME: this.inputAccountName,
      BANK_KEY: this.inputBankKey,
      BANK_NAME: this.inputBankName,
      BANK_BRANCH_CODE: this.inputBankBranchCode,
      BANK_BRANCH: this.inputBankBranchName,
      BANK_ACCOUNT: this.inputBankAccount,

      RECIPIENT_TYPE: this.inputRecipienttype,
      TAX_NO: this.inputTaxno,
      MOBILE: this.inputMobile,
      MOBILE_WEB: this.inputMobileWeb,
      EMAIL: this.inputEmail,

      ADDRESS_TAX: this.inputAddresstax,
      STREET4: this.inputstreet4,
      CITY_CODE: this.inputCityCode,
      CITY_DESC: this.inputCityDesc,
      DISTRICT_CODE: this.inputDistrictCode,
      DISTRICT_DESC: this.inputDistrictDesc,
      STREET5: this.inputstreet5,
      POST_CODE: this.inputPostCode,
      IS_WHT: 1,
      DEALER_ID: String(this.DEALER_ID),
      LAST_UPDATE_BY: String(this.ActionBy),

      IMG1: this.Doc1base64.replace('data:image/jpeg;base64,', ''),
      IMG2: this.Doc2base64.replace('data:image/jpeg;base64,', ''),
    }

    let toast = this.toastCtrl.create({
      message: 'บันทึกข้อมูลสำเร็จ',
      duration: 2000,
      position: 'bottom'
    });
    
    let checkIsnull = this.valiIsNull(objChangeProfile); //check Empty
    let check = this.validate(objChangeProfile); //check รูป
    if(checkIsnull){
        if (check) {
          this.epSaleProvider.updProfile(objChangeProfile)
            .then(res => {
              if (res.result == 'SUCCESS') {
                this.sendMail();
                this.navCtrl.setRoot(EpHomePage);

                toast.onDidDismiss(() => {
                });

                setTimeout(() => {
                  this.navCtrl.setRoot(EpHomePage);
                }, 1000);

                toast.present();

              } else {
                this.alertCtrl.create({
                  title: 'แจ้งเตือน',
                  subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลอีกครั้ง',
                  mode: 'ios',
                  enableBackdropDismiss: false,
                  buttons: ['ตกลง']
                }).present();
              }
            }
            ), err => {
            }
        } else {
          this.alertCtrl.create({
            title: 'แจ้งเตือน',
            subTitle: 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบไฟล์อัพโหลดรูปภาพ',
            mode: 'ios',
            enableBackdropDismiss: false,
            buttons: ['ตกลง']
          }).present();
        }
    }




  }


    sendMail() {
    let UserName = this.userInfo.userName;
    let email = this.userInfo.email;

    let ParaObject = {
      userName: UserName,
      Email: email
    }

    this.epSaleProvider.updProfileSendMail(ParaObject)
      .then(res => {
        if (res.result == 'SUCCESS') {
        }  
      }
      ), err => {
         console.log("error==>",err);
      }

  }

  
  valiIsNull(objChangeProfile) {
    let check = true;
    let msgError = '';

    if(objChangeProfile.ACCOUNT_NAME == "" || objChangeProfile.ACCOUNT_NAME == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบชื่อบัญชี'
    }
    else if(objChangeProfile.BANK_KEY == "" || objChangeProfile.BANK_KEY == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบธนาคาร'
    }
    else if(objChangeProfile.BANK_BRANCH_CODE == "" || objChangeProfile.BANK_BRANCH_CODE == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบสาขา'
    }
    else if(objChangeProfile.BANK_ACCOUNT == "" || objChangeProfile.BANK_ACCOUNT == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบเลขที่บัญชี'
    }
    else if(objChangeProfile.RECIPIENT_TYPE == "" || objChangeProfile.RECIPIENT_TYPE == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบประเภทผู้เสียภาษี'
    }
    else if(objChangeProfile.TAX_NO == "" || objChangeProfile.TAX_NO == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบเลขประจำตัวผู้เสียภาษี'
    }
    else if(objChangeProfile.MOBILE_WEB == "" || objChangeProfile.MOBILE_WEB == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบเบอร์โทรศัพท์มือถือ'
    }
    else if(objChangeProfile.EMAIL == "" || objChangeProfile.EMAIL == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบอีเมล'
    }else if(objChangeProfile.ADDRESS_TAX == "" || objChangeProfile.ADDRESS_TAX == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจบ้านเลขที่ / อาคาร / ซอย'
    }
    // else if(objChangeProfile.STREET4 == "" || objChangeProfile.STREET4 == undefined){
    //   check = false;
    //   msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบถนน'
    // }
    else if(objChangeProfile.CITY_CODE == "" || objChangeProfile.CITY_CODE == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบจังหวัด'
    }else if(objChangeProfile.DISTRICT_CODE == "" || objChangeProfile.DISTRICT_CODE == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบอำเภอ / เขต'
    }else if(objChangeProfile.STREET5 == "" || objChangeProfile.STREET5 == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบตำบล / แขวง'
    }else if(objChangeProfile.POST_CODE == "" || objChangeProfile.POST_CODE == undefined){
      check = false;
      msgError = 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบรหัสไปรษณีย์'
    }

    

    this.alertErrorEmpty = this.alertCtrl.create({
      title: 'ePocket Alert',
      subTitle: msgError,
      mode: 'ios',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'ตกลง',
          handler: () => {
            this.alertPresented = false;
          }
        }]
    });

    if(!check){
      this.alertErrorEmpty.present();
      this.alertPresented = true;
    }
    return check;
  }

  validate(objChangeProfile) {
    let check = true;
    if (this.isChange == 1) {
      if (objChangeProfile.IMG1.length < 50 || objChangeProfile.IMG2.length < 50) {
        check = false;
      }
      else
        check = true
    } else
      check = true;

    return check;
  }

  clearGalleryDoc1() {
    this.Doc1base64 = '';
  }


  accessGalleryDoc1() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1024,
      targetHeight: 768,
      correctOrientation: true
    }).then((imageData) => {
      this.Doc1base64 = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }


  clearGalleryDoc2() {
    this.Doc2base64 = '';
  }



  accessGalleryDoc2() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1024,
      targetHeight: 768,
      correctOrientation: true
    }).then((imageData) => {
      this.Doc2base64 = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }




  back() {
    this.navCtrl.pop();
  }

}

import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-ep-painter',
  templateUrl: 'ep-painter.html',
})
export class EpPainterPage {
  PainterList : Array<any>;
  PainterList_Full : Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private epDealerProvider: EPDealerProvider) {
  }

  ionViewDidLoad() {
    this.fatchData();
  }

  fatchData() {
    this.epDealerProvider.getPainter({})
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.PainterList = res.data.painterList;
          this.PainterList_Full = this.PainterList;
        }
      });
  }

  
  SelectPainter(item){
    this.navCtrl.pop().then(() => this.navParams.get('painterObject')(item))
  }

  getItems(ev: any) {
    let val = ev.target.value;
    
    if (val && val.trim() != '') {
      this.PainterList = this.PainterList_Full.filter((item =>{
        return item.painterno.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.titlename.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        item.firstname.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.lastname.toLowerCase().indexOf(val.toLowerCase()) > -1;
      }))
    }else{
      this.PainterList = this.PainterList_Full;
    }
  }

}

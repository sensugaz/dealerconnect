import { ConfigProvider } from './../../../providers/eordering/config';
import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-ep-dealer-payment-history-info',
  templateUrl: 'ep-dealer-payment-history-info.html',
})
export class EpDealerPaymentHistoryInfoPage {
  PayDoc : string;
  PayDate : string;
  PayableDeatil : any;
  SALEQTY : number;
  SUMVALUER : string;
  SUMVALUEP : string;
  AMOUNT : string;
  REFDOC_R : string;
  REFDOC_P : string;
  PRODUCT_HIERARCHY : string;
  
  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public epDealerProvider: EPDealerProvider) {
      
      this.PayDoc = this.navParams.get("PayDoc");
      this.PayDate = this.navParams.get("PayDate");
  }

  fatchData(){
    let pObject = {
      PayDoc : this.PayDoc
    }

    this.epDealerProvider.getPayableInfo(pObject)
    .then(res => {
      if (res.result == 'SUCCESS') {
        this.PayableDeatil = res.data.payableInfo[0];
        this.SALEQTY = this.PayableDeatil.saleqty;
        this.SUMVALUER = this.PayableDeatil.sumvaluer;
        this.SUMVALUEP = this.PayableDeatil.sumvaluep;
        this.AMOUNT = this.PayableDeatil.amount;
        this.REFDOC_R = this.PayableDeatil.refdocr;
        this.REFDOC_P = this.PayableDeatil.refdocp;
        this.PRODUCT_HIERARCHY = this.PayableDeatil.producthierarchy;
      }
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EpDealerPaymentHistoryInfoPage');
    this.fatchData();  
  }

  

}

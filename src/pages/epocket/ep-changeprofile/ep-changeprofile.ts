import { EpBankPage } from './../ep-bank/ep-bank';
import { EPSaleProvider } from './../../../providers/epocket/sale';
import { EpChangeprofileStep2Page } from './../ep-changeprofile-step2/ep-changeprofile-step2';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ConfigProvider } from './../../../providers/eordering/config';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { StorageProvider } from './../../../providers/shared/storage';
import { UserProvider } from '../../../providers/dealerconnect/user';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { EpBankBranchPage } from '../ep-bank-branch/ep-bank-branch';
import { DealerConnect } from '../../../app/modules/dealerconnect/dealerconnect.component';

@Component({
  selector: 'page-ep-changeprofile',
  templateUrl: 'ep-changeprofile.html',
})
export class EpChangeprofilePage {

  userInfo: any;
  customerInfo: any;
  dealerID: string;
  Username : string;
  
  inputAccountName: string = "";
  inputBankKey: string  = "";
  inputBankName: string  = "";
  inputBankBranchCode = "";
  inputBankBranchName = "";
  inputBankAccount: string  = "";

  inputRecipienttype : string = "";
  inputTaxno : string  = "";
  inputMobile : string  = "";
  inputMobileWeb : string  = "";
  inputEmail : string = "";

  inputAddresstax : string  = "";
  inputstreet4 : string= "";
  inputCityCode : string = "";
  inputCityDesc : string = "";
  inputDistrictCode : string = "";
  inputDistrictDesc : string = "";
  inputIswht : string  = "";
  inputstreet5 : string= "";
  inputPostCode : string = "";
  
  inputImgbookbank : string ="";
  inputImgidcard : string ="";

  inputAccountNameOld = "";
  inputBranchOld: string  = "";  
  inputBankCodeOld: string  = "";
  inputBankNameOld: string  = "";
  inputBankAccountOld: string  = "";

  

  isnew : number = 0;
  BankInfo: any;
  BankBranchInfo : any;
  changeProfile: any;

  alertConfirmPassword : any = this.alertCtrl.create();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public configProvider: ConfigProvider,
    public storageProvider: StorageProvider,
    private epSaleProvider: EPSaleProvider,
    private alertCtrl: AlertController,
    private userProviderider : UserProvider,
    private toastCtrl: ToastController,
    private platform : Platform
  ) {

    this.userInfo = this.storageProvider.getUserInfo();
    this.customerInfo = this.storageProvider.getCustomerInfo();
    this.dealerID = this.customerInfo.customerCode;
    this.Username =  this.userInfo.userName
    this.ConfirmPassword();
  }

  ionViewDidEnter() {
    this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.alertConfirmPassword.dismiss();
        this.navCtrl.pop();
      });
    });
   }

  ConfirmPassword(){
    this.alertConfirmPassword = this.alertCtrl.create({
        title: 'ยืนยันรหัสผ่าน',
        mode: 'ios',
        enableBackdropDismiss: false,
        inputs: [
          {
            name: 'password',
            placeholder: 'Password',
            type: 'password'
          }
        ],
        buttons: [
          {
            text: 'ยกเลิก',
            role: 'cancel',
            handler: data => {
              //this.navCtrl.pop();  
              this.navCtrl.setRoot(DealerConnect, { action: 'NP' });
            }
          },
          {
            text: 'ตกลง',
            handler: data => {
              this.ComparePassword(data.password);
            }
          }
        ]
      });
    
      this.alertConfirmPassword.present();
  }


  //=========== Confirm Login ===========
  ComparePassword(password){
    let objectUser = {
      Username : this.Username,
      Password : password
    }
     
    this.userProviderider.getCheckLoginByUser(objectUser)
    .then(res => {
      if(res.result == 'ERROR')
        this.LoginFalse(res.remarkText);
      else 
        this.LoginSuccess(res.remarkText);
    }),err =>{
      console.log(err);
    }
  }

  LoginSuccess(resultMsg){
    let toast = this.toastCtrl.create({
      message: resultMsg,
      duration: 2000,
      position: 'top',
      cssClass : 'text-center'
    });

    toast.present();
  }

  LoginFalse(resultMsg){
    let toast = this.toastCtrl.create({
      message: resultMsg,
      duration: 2000,
      position: 'top'
    });

    this.navCtrl.pop();

    toast.present();
  }


  // =========== Page Initial ===========
  ionViewDidLoad() {
    this.fetchDataInfo();
  }

  fetchDataInfo() {
    let obj = {
      DEALER_ID: this.dealerID
    };

    this.epSaleProvider.getChangeProfileInfo(obj)
      .then(res => {
        let DataIsEmpty = res.data.changeProfile[0] == undefined || res.data.changeProfile[0] == null || res.data.changeProfile[0] == 0;
        if (res.result == 'SUCCESS' && DataIsEmpty == false) {
          this.changeProfile = res.data.changeProfile[0];

          this.inputAccountName = this.changeProfile.accountname;
          this.inputBankKey = this.changeProfile.bankkey;
          this.inputBankName = this.changeProfile.bankname;
          this.inputBankBranchCode = this.changeProfile.bankbranchcode
          this.inputBankBranchName = this.changeProfile.bankbranch;
          
          this.inputRecipienttype = this.changeProfile.recipienttype
          this.inputTaxno = this.changeProfile.taxno;
          this.inputBankAccount = this.changeProfile.bankaccount;
          this.inputMobile = this.changeProfile.mobile;
          this.inputMobileWeb = this.changeProfile.mobileweb;
          this.inputEmail = this.changeProfile.email;

          this.inputAddresstax = this.changeProfile.addresstax;
          this.inputstreet4 = this.changeProfile.street4;
          this.inputCityCode = this.changeProfile.citycode;
          this.inputCityDesc = this.changeProfile.citydesc;

          this.inputDistrictCode = this.changeProfile.districtcode;
          this.inputDistrictDesc = this.changeProfile.districtdesc;
          this.inputIswht = this.changeProfile.iswht;
          this.inputstreet5 = this.changeProfile.street5;
          this.inputPostCode = this.changeProfile.postcode;

          this.inputImgbookbank = this.changeProfile.imgbookbank;
          this.inputImgidcard = this.changeProfile.imgidcard;

          this.inputAccountNameOld = this.inputAccountName;
          this.inputBranchOld = this.inputBankBranchCode;
          this.inputBankCodeOld = this.inputBankKey;
          this.inputBankNameOld = this.inputBankName;
          this.inputBankAccountOld = this.inputBankAccount;

          this.isnew = 1;

        }else{
          this.inputAccountName = "";
          this.inputBankBranchName = "";
          this.inputBankAccount = "";
          this.inputBankKey = "";
          this.inputBankName = "";
          this.inputTaxno = "";
          this.inputAddresstax = "";
          this.inputMobile = "";
          this.inputIswht = "";
          this.inputImgbookbank = "";          
          this.inputImgidcard = "";
          this.isnew = 0;
        }
      });
  }



  // =========== Action Button ===========
  next() {

    let isChange : number = 0;
    if(this.inputAccountNameOld != this.inputAccountName || 
      this.inputBranchOld != this.inputBankBranchCode  ||
      this.inputBankCodeOld != this.inputBankKey  ||
      this.inputBankNameOld != this.inputBankName  || 
      this.inputBankAccountOld != this.inputBankAccount   )
      {
        isChange = 1;
      }

    this.navCtrl.push(EpChangeprofileStep2Page, {
      "accountname" : this.inputAccountName,
      "bankkey" : this.inputBankKey,
      "bankname" : this.inputBankName,
      "bankbranchcode" : this.inputBankBranchCode,
      "bankbranchname" : this.inputBankBranchName,
      "bankaccount" : this.inputBankAccount,

      "recipienttype" : this.inputRecipienttype,
      "taxno": this.inputTaxno,
      "mobile": this.inputMobile,
      "mobileweb": this.inputMobileWeb,
      "email": this.inputEmail,
      
      "addresstax": this.inputAddresstax,
      "street4": this.inputstreet4,
      "citycode": this.inputCityCode,
      "citydesc": this.inputCityDesc,
      "districtcode": this.inputDistrictCode,
      "districtdesc": this.inputDistrictDesc,
      "iswht": this.inputIswht,
      "street5": this.inputstreet5,
      "postcode" : this.inputPostCode,

      "imgbookbank" : this.inputImgbookbank,
      "imgidcard" : this.inputImgidcard,

      "isnew" : this.isnew,
      "isChange" : isChange,
    });
  }

  BankSelect() {
    new Promise((bankObject, reject) => {
      this.navCtrl.push(EpBankPage, { bankObject: bankObject });
    }).then(data => {
      this.BankInfo = data;
      this.inputBankKey = this.BankInfo.bankcode;
      this.inputBankName = this.BankInfo.bankname;
    });
  }

  BankBranchSelect() {
    new Promise((bankBranchObject, reject) => {
      this.navCtrl.push(EpBankBranchPage, { "BankCode" : this.inputBankKey,bankBranchObject: bankBranchObject });
    }).then(data => {
      this.BankBranchInfo = data;
      this.inputBankBranchCode = this.BankBranchInfo.branchcode;
      this.inputBankBranchName = this.BankBranchInfo.branchname;
    });
  }
}

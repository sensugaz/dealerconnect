import { ConfigProvider } from './../../../providers/eordering/config';
import { EPDealerProvider } from './../../../providers/epocket/dealer';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController,Platform } from 'ionic-angular';
import * as moment from 'moment';
import { CalendarModal } from 'ion2-calendar';
import { StorageProvider } from '../../../providers/shared/storage';
import { EpDealerPaymentHistoryInfoPage } from '../ep-dealer-payment-history-info/ep-dealer-payment-history-info';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewer } from '@ionic-native/document-viewer';

@Component({
  selector: 'page-ep-dealer-payment-history',
  templateUrl: 'ep-dealer-payment-history.html',
})
export class EpDealerPaymentHistoryPage {
  
  loading: any;
  config: any;
  userInfo: any;
  customerInfo: any;
  dealerID: string;

  startDate: any;
  endDate: any;

  DataHD : any;
  DataReport : any;

  csvData : any[] = [];
  headerRow : any[]= [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams , 
              public loadingCtrl: LoadingController,
              public epDealerProvider: EPDealerProvider,
              public configProvider: ConfigProvider,
              public storageProvider: StorageProvider,
              private modalCtrl: ModalController,
              private platform : Platform,
              private file: File,
               
              private fileOpener: FileOpener) {
              
              this.config = this.storageProvider.getConfig();
              this.userInfo = this.storageProvider.getUserInfo();
              this.customerInfo = this.storageProvider.getCustomerInfo();
              this.dealerID = this.customerInfo.customerCode;
              
              this.startDate = moment().subtract(1, 'months').toDate(); 
              this.endDate = moment().toDate();
              this.backButtonAndroid();
  }

  backButtonAndroid(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
    });
   }

  ionViewDidLoad() {
    this.fetchPaymentHistory();
  }

  fetchPaymentHistory() {
    this.showLoading();

    let Begindate = moment(this.startDate).format('YYYY-MM-DD') ;
    let Enddate = moment(this.endDate).format('YYYY-MM-DD');
    let DealerID = String(this.dealerID);

    let ParaObject ={
      Begindate : moment(this.startDate).format('YYYY-MM-DD'),
      Enddate : moment(this.endDate).format('YYYY-MM-DD'),
      Dealerid : String(this.dealerID)
     }

    this.epDealerProvider.getDealerPayment(Begindate,Enddate,DealerID)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataHD = res.data.paymentinfo;
        }
        this.hideLoading();
      });

      this.epDealerProvider.getRPTDealerPayment(ParaObject)
      .then(res => {
        if (res.result == 'SUCCESS') {
          this.DataReport = res.data.paymentinfo;
          let csv = papa.unparse(this.DataReport);
          this.extractData(csv);
        }
      });

      
  }

  showLoading() {
		if (!this.loading) {
			this.loading = this.loadingCtrl.create({
				content: 'กำลังโหลด...'
			});

			this.loading.present();
		}
	}

	hideLoading() {
		if (this.loading) {
			this.loading.dismiss();
			this.loading = null;
		}
  }

  onOpenStartDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.startDate = date.dateObj ;
      }
    });
  }

  onOpenEndDate() {
    let calendar =  this.modalCtrl.create(CalendarModal, {
      options: {
        title: 'เลือกวันที่',
        canBackwardsSelected: true,
        defaultDate: moment(this.endDate).toDate(),
        from: moment(this.startDate).toDate(),
        color: 'cal-color',
        doneIcon: true,
        closeIcon: true
      }
    });

    calendar.present();

    calendar.onDidDismiss((date, type) => {
      if (type == 'done') {
        this.endDate = date.dateObj;
      }
    });
  }


  payDocOnclick(payDoc,paydate){
    this.navCtrl.push(EpDealerPaymentHistoryInfoPage,{"PayDoc": payDoc,"PayDate":paydate});
  }

  // ===== Export ===== //
  private extractData(res) {
    let parsedData = papa.parse(res).data;
    //this.headerRow = parsedData[1];
    parsedData.splice(0, 1);
    this.csvData = parsedData;
  }
 
  downloadCSV() {
    let csv = papa.unparse({
      //fields: this.headerRow,
      data: this.csvData
    });
 
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "PaymentHistory.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    this.ReadWriteFile(csv)
  }

  ReadWriteFile(csv){
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.externalDataDirectory;
    }

    let name = 'DealerExchange.csv';
  
    this.file.checkFile(path, name)
    .then(() => {
      this.file.writeExistingFile(path, name, csv)
        .then(res => {
          this.fileOpener.open(`${path}/${name}`, 'text/csv')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error openening file', e));
        });
    })
    .catch(err => {
      this.file.writeFile(path, name, csv)
        .then(res => {
        });
    });
  }
  // ===== End Export ===== //

  cancel(){
    this.navCtrl.pop();
  }

}
